<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{
    use SoftDeletes;
        
    protected $guarded = [];
    
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
