<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Size extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function food()
    {
        return $this->belongsTo('App\Food');
    }
    public function order_food()
    {
        return $this->belongsTo('App\OrderFood');
    }
}
