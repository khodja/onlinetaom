<?php

namespace App\Exports;

use App\Order;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class UsersExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $data = Order::withTrashed()->where('status', '!=', 0)->with('user','restaurant')->orderBy('updated_at','DESC')->get();
        return view('exports.orders', compact('data'));
    }
}
