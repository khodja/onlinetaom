<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Param extends Model
{
    use SoftDeletes;

    protected $guarded = [];



    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image'];

    public function food()
    {
        return $this->belongsTo('App\Food');
    }
    /**
     * Get an image.
     *
     * @return string
     */
    public function getImageAttribute(){
        return env('APP_URL').'/uploads/params/'.$this->id.'.jpg';
    }
}
