<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarrierOrder extends Model
{
    protected $connection='carrier';
    protected $table='orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'driver_id','restaurant_id',
        'order_id', 'street_name',
        'distance', 'delivery_time',
        'cook_time', 'status', 'delivery_price', 'full_price',
        'delivery_delta','ot_delta'
    ];

    protected $casts = [
        'created_at'  => 'date:U',
    ];

    public function user()
    {
        return $this->belongsTo('App\Carrier','driver_id');
    }
    public function client_order()
    {
        return $this->belongsTo('App\Order','order_id');
    }
    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant','restaurant_id');
    }
}
