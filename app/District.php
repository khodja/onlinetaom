<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class District extends Model
{
    use SoftDeletes;    
    protected $guarded = [];

    /**
     * Get district's restaurants .
     *
     * @return HasMany
     */    
    public function restaurants()
    {
        return $this->hasMany('App\Restaurant');
    }    
}
