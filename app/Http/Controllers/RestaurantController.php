<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

use App\Restaurant;
use App\Category;
use App\District;
use App\User;
use Intervention\Image\ImageManagerStatic as Image;
use Carbon\Carbon;
use Auth;

class RestaurantController extends Controller
{

    /**
     * View all Restaurants
     * @return View
     */
    public function index()
    {
        if(Auth::user()->hasRole('manager')){
            $data = Auth::user()->restaurant()->withTrashed()->get();
        }
        else{
            $data = Restaurant::withTrashed()->get();
        }
        return view('backend.restaurant.index', compact('data'));
    }
    public function show($alias)
    {
        $data = Restaurant::where('alias', '=', $alias)->with(['categories','foods','schedules'])->firstOrFail();
//        $user = Auth::user();
//        dd($user);
//        if($favorite->isNotEmpty()) {
//            $data = (object) $favorite->where(['type','restaurant'],['foreign_id', $data->id ]);
//        }

        return response()->json($data);
    }
    public function showAll($start=0,$limit=30)
    {
        if(request()->header('location') !== null){
            $myString = request()->header('location');
            $myArray = explode(',', $myString);
            $longitude = preg_replace('/\s+/', '', $myArray[1]);
            $latitude = preg_replace('/\s+/', '', $myArray[0]);
            $data= Restaurant::select(\DB::raw('*,ST_Distance(POINT(`long`,`lat`),POINT( '.$longitude.', '.$latitude.')) AS dist'))->with('categories')->orderBy('dist','ASC')
                ->offset($start)->take($limit)->get()->where('open',true)->toArray();
            $data = array_values($data);
        }
        else{
//            $data = Restaurant::with('categories')->latest()->take(15)->where('open',true);
//            $data = Restaurant::with('categories')->latest()->take(15)->get();
            $data = Restaurant::with('categories')->latest()->offset($start)->take($limit)->get()->where('open',true)->toArray();
            $data = array_values($data);
        }
        return response()->json($data);
    }
    public function test()
    {
//        $data = Restaurant::with('categories')->get();
//            $data = Restaurant::with('categories')->latest()->take(15)->get()->where('open',true)->toArray();
//            $data = array_values($data);
            $data = Restaurant::find(24)->user()->get();
//            foreach($data as $datas){
//                $this->sendTorest($datas->token)
//            }
//            $users = User::where('restaurant_id',24)->get();
//            $data = collect($data);
//            $data = $data->sortBy('id');
//            $data->values()->all();
        dd($data);
    }
    public function showFinalAll()
    {
        $data = Restaurant::with('categories')->get();
        return response()->json($data);
    }

    /**
     * Store Restaurant
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'desc_uz' => 'required',
            'desc_ru' => 'required',
            'phone' => 'required',
            'long' => 'required',
            'lat' => 'required',
            'payments' => 'required',
            'address' => 'required',
            'image' => 'required|max:5120',
            'cover' => 'required|max:5120',
        ]);

        $restaurant = Restaurant::create([
            'name' => request('name'),
            'desc_uz' => request('desc_uz'),
            'desc_ru' => request('desc_ru'),
            'phone' => request('phone'),
            'phone1' => request('phone1'),
            'phone2' => request('phone2'),
            'percentage' => request('percentage'),
            'long' => request('long'),
            'lat' => request('lat'),
            'delivery_delta' => request('delivery_delta'),
            'ot_delta' => request('ot_delta'),
            'payments' => request('payments'),
            'address' => request('address'),
            'district_id' => request('district_id'),
            'shipping_free' => request('shipping_free') || 0,
        ]);
        $restaurant->schedules()->createMany([
            [
                'restaurant_id'=>$request->id,
                'weekday'=>1,
                'open_hour'=>'9:00',
                'close_hour'=>'20:00',
            ],
            [
                'restaurant_id'=>$request->id,
                'weekday'=>2,
                'open_hour'=>'9:00',
                'close_hour'=>'20:00',
            ],
            [
                'restaurant_id'=>$request->id,
                'weekday'=>3,
                'open_hour'=>'9:00',
                'close_hour'=>'20:00',
            ],
            [
                'restaurant_id'=>$request->id,
                'weekday'=>4,
                'open_hour'=>'9:00',
                'close_hour'=>'20:00',
            ],
            [
                'restaurant_id'=>$request->id,
                'weekday'=>5,
                'open_hour'=>'9:00',
                'close_hour'=>'20:00',
            ],
            [
                'restaurant_id'=>$request->id,
                'weekday'=>6,
                'open_hour'=>'9:00',
                'close_hour'=>'20:00',
            ],
            [
                'restaurant_id'=>$request->id,
                'weekday'=>7,
                'open_hour'=>'9:00',
                'close_hour'=>'20:00',
            ],
        ]);
        $restaurant->categories()->attach($request->category_id);

        $image = $request->file('image');
        $filename = $restaurant->id .'.jpg';
		$path = public_path('uploads/restaurant/'. $filename);
        Image::make($image->getRealPath())->encode('jpg', 90)
        ->crop(200, 200)
        ->save($path);

        $cover = $request->file('cover');
        $covername = $restaurant->id .'.jpg';
		$coverpath = public_path('uploads/restcover/'. $covername);
        Image::make($cover->getRealPath())->encode('jpg', 90)
        ->crop(840, 520)
        ->save($coverpath);


        return redirect()->action('RestaurantController@index')->with('success','Успешно добавлено');
    }

    /**
     * Update Restaurant
     * @param Request $request
     * @return Response
     */
    public function update(Request $request , $id)
    {
            request()->validate([
                'name' => 'required',
                'desc_ru' => 'required',
                'image' => 'max:5120',
                                'cover' => 'max:5120',
            ]);
        $restaurant = Restaurant::withTrashed()->findOrFail($id);
            if ($request->file('image')) {
                $old = Image::make('uploads/restaurant/' . $id . '.jpg');
                $old->destroy();
                $image = $request->file('image');
                $filename = $restaurant->id . '.jpg';
                $path = public_path('uploads/restaurant/' . $filename);
                Image::make($image->getRealPath())->encode('jpg', 90)
                    ->crop(200, 200)
                    ->save($path);
            }
            if ($request->file('cover')) {
                $cover = $request->file('cover');
                $covername = $restaurant->id . '.jpg';
                $coverpath = public_path('uploads/restcover/' . $covername);
                Image::make($cover->getRealPath())->encode('jpg', 90)
                    ->crop(640, 392)
                    ->save($coverpath);
            }
            $restaurant->update([
                'name' => request('name'),
                'desc_uz' => request('desc_uz'),
                'desc_ru' => request('desc_ru'),
                'phone' => request('phone'),
                'phone1' => request('phone1'),
                'phone2' => request('phone2'),
                'delivery_delta' => request('delivery_delta'),
                'ot_delta' => request('ot_delta'),
                'percentage' => request('percentage'),
                'long' => request('long'),
                'lat' => request('lat'),
                'payments' => request('payments'),
                'address' => request('address'),
                'district_id' => request('district_id'),
                'in_main' => request('in_main') || 0,
                'shipping_free' => request('shipping_free') || 0,
            ]);
            if($request->category_id){
                $restaurant->categories()->sync($request->category_id);
            }
            return redirect()->action('RestaurantController@index')->with('success','Успешно изменен');
    }

    /**
     * Show Restaurant create form
     * @return View
     */
    public function create()
    {
        $data = Category::all();
        $dist = District::all();
        return view('backend.restaurant.create',compact('data','dist'));
    }

    /**
     * Show Restaurant edit form
     * @param $id
     * @return View
     */
    public function edit($id)
    {
        $rest = Restaurant::withTrashed()->findOrFail($id);
        $categories = $rest->categories->pluck('id')->toArray();
        $data = Category::all();
        $dist = District::all();
        return view('backend.restaurant.edit', compact('rest','data','dist','categories'));
    }

    public function delete($id)
    {
        $data = Restaurant::withTrashed()->find($id);
        $data->promos()->delete();
            $data->foods()->delete();
        $data->delete();

        return redirect()->back()->with('success','Успешно удален');
    }
    public function restore($id)
    {
        Restaurant::withTrashed()->find($id)->restore();
        return redirect()->back()->with('success','Успешно восстановлен');
    }
    public function switchRestaurant()
    {
        $user = Auth::user();
        $data = $user->restaurant();
        if($data->first()->deleted_at != null) {
            $user->restaurant()->restore();
        } else{
            $data->first()->promos()->delete();
            $data->delete();
        }
        return response()->json('Ok');
    }
}
