<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Size;
use App\Food;

class SizeController extends Controller
{
    public function create($id)
    {
        return view('backend.size.create',compact('id'));
    }
    public function edit($id)
    {
        $data = Size::find($id);
        return view('backend.size.edit',compact('data','id'));
    }
    public function show($id)
    {
        $data = Food::findOrFail($id)->size;
        return $data;
    }     
    public function store(Request $request, $id)
    {
        request()->validate([
            'name' => 'required',
            'price' => 'required',
        ]);            
        $param = Size::create([
            'name'=>request('name'),
            'price'=>request('price'),
            'food_id'=>request('id')
        ]);
        return redirect()->action('FoodController@size',Size::findOrFail($id)->food_id)->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        $param = Size::findOrFail($id)->update(
            $request->all()
        );
        return redirect()->action('FoodController@size',Size::findOrFail($id)->food_id)->with('success','Успешно изменен');
    }
    public function delete($id)
    {
        $param = Size::findOrFail($id);
        $food_id = $param->food_id;
        $param->delete();
        return redirect()->action('FoodController@size',$food_id)->with('success','Успешно удален');
    }    
}
