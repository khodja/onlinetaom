<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Carrier;
use App\CarrierOrder;

class CarrierController extends Controller
{
    public function index()
    {
        $data = Carrier::all();
        $count = Carrier::where('online',1)->count();
        return view('backend.carrier.index',compact('data','count'));
    }
    public function orders($id)
    {
        $data = CarrierOrder::where('driver_id',$id)->with('restaurant','user','client_order')->get();
        return view('backend.carrier.orders',compact('data'));
    }
    public function edit($id)
    {
        $data = Carrier::findOrFail($id);
        return view('backend.carrier.edit',compact('data'));
    }
    public function editOrder($id)
    {
        $data = CarrierOrder::findOrFail($id);
        $user = Carrier::all();

        return view('backend.carrier.change',compact('data','user'));
    }
    public function create()
    {
        return view('backend.carrier.create');
    }
    public function store()
    {
        request()->validate([
            'name' => 'required',
            'phone_number' => 'required',
            'car_number' => 'required',
        ]);
        Carrier::create([
            'name' => request('name'),
            'phone_number' => request('phone_number'),
            'car_number' => request('car_number'),
            'password' => bcrypt(request('password'))
        ]);
        return redirect()->action('CarrierController@index')->with('success', 'Вы добавили курьера');
    }
    public function update(Request $request , $id)
    {
        $data = Carrier::find($id);
        if($request->password != null){
            Carrier::find($id)->update(['password' => bcrypt($request->password)]);
        }
        $data->update($request->except('password'));

        return redirect()->action('CarrierController@index')->with('success', 'Вы изменили курьера');
    }
    public function updateOrder(Request $request , $id)
    {
        $data = CarrierOrder::find($id);
//        $user_id = $data->user->id;
        if(request('driver_id') != 0 ){
            $data->update($request->all());
            return redirect()->action('CarrierController@orders',$data->user->id)->with('success', 'Вы изменили курьера');

        }
        else{
            $data->update([
                'driver_id' => request('driver_id') ,
                'status' => 0
            ]);
            return redirect()->action('OrderController@index')->with('success', 'Вы изменили курьера');
        }

    }
    public function delete($id)
    {
        $data = Carrier::find($id);
        $data->delete();

        return redirect()->action('CarrierController@index')->with('success', 'Вы удалили курьера');
    }
}
