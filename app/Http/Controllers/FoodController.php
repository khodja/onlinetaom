<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Food;
use App\OrderFood;
use App\Restaurant;
use Intervention\Image\ImageManagerStatic as Image;

class FoodController extends Controller
{
    public function create()
    {
        if(request('restaurant_id') == null ){
            return redirect()->action('RestaurantController@index');
        }
        $data = Restaurant::withTrashed()->find(request('restaurant_id'));
        return view('backend.food.create',compact('data'));
    }
    public function index()
    {
        if(Auth::user()->hasRole('manager')){
            $restId = Auth::user()->restaurant->id;
            $data = Food::withTrashed()->where('restaurant_id',$restId)->get();
        }
        else{
            $data = Food::withTrashed()->get();
        }
        return view('backend.food.index', compact('data'));
    }
    public function menu($id)
    {
        $rest_id = Restaurant::withTrashed()->where('alias',$id)->first()->id;
        $data = Food::withTrashed()->where('restaurant_id',$rest_id)->get();
        return view('backend.food.index', compact('data','rest_id'));
    }
    public function foodsManager()
    {
        $restId = Auth::user()->restaurant->id;
        $data = Food::withTrashed()->where('restaurant_id',$restId)->orderBy('name_ru','ASC')->get();
        return response()->json($data);
    }
    public function get()
    {
        $data = Food::all();
        return response()->json($data);
    }
    public function showFood($alias , $id)
    {
        $data = Food::withTrashed()->where('id', '=', $id)->with(['restaurant','params','size'])->firstOrFail();
        return response()->json($data);
    }
    public function show($id)
    {
        $data = Food::withTrashed()->where('id',$id)->with(['restaurant','params','size'])->firstOrFail();
        return response()->json($data);
    }
    public function edit($id)
    {
        $data = Food::find($id);
        $rest = Restaurant::withTrashed()->find($data->restaurant_id);
        return view('backend.food.edit', compact('data','rest'));
    }
    public function store(Request $request)
    {
        request()->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'desc_uz' => 'required',
            'desc_ru' => 'required',
            'price' => 'required',
            'cook_time' => 'required',
        ]);
        $food = Food::create([
            'name_uz'=>request('name_uz'),
            'name_ru'=>request('name_ru'),
            'desc_uz'=>request('desc_uz'),
            'desc_ru'=>request('desc_ru'),
            'price'=>request('price'),
            'cook_time'=>request('cook_time'),
            'restaurant_id'=>request('restaurant_id'),
            'category_id'=>request('category_id'),
        ]);

        $image = $request->file('image');
        $filename = $food->id .'.jpg';
		$path = public_path('uploads/food/'. $filename);
        Image::make($image->getRealPath())->encode('jpg', 90)
        ->crop(840, 520)
        ->save($path);
        $alias = Restaurant::withTrashed()->where('id',request('restaurant_id'))->first()->alias;

        return redirect()->action('FoodController@menu', $alias)->with('success','Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $food = Food::withTrashed()->findOrFail($id);
        $alias = Restaurant::withTrashed()->where('id',$food->restaurant_id)->first()->alias;
        if( $request->file('image') ){
            $old = Image::make('uploads/food/'.$id.'.jpg');
            $old->destroy();
            $image = $request->file('image');
            $filename = $food->id .'.jpg' ;
            $path = public_path('uploads/food/'. $filename);
            Image::make($image->getRealPath())->encode('jpg', 90)
            ->crop(840, 520)
            ->save($path);
        }
        $food->update([
            'name_uz'=>request('name_uz'),
            'name_ru'=>request('name_ru'),
            'desc_uz'=>request('desc_uz'),
            'desc_ru'=>request('desc_ru'),
            'price'=>request('price'),
            'cook_time'=>request('cook_time'),
            'category_id'=>request('category_id'),
        ]);


        return redirect()->action('FoodController@menu', $alias)->with('success','Успешно изменен');
    }

    public function delete($id)
    {
        $food = Food::withTrashed()->where('id',$id)->first();
        $alias = Restaurant::withTrashed()->where('id',$food->restaurant_id)->first();
        $order_food = OrderFood::where('food_id',$id);
        if($order_food){
            $order_food->delete();
        }
        $food->delete();
        return redirect()->action('FoodController@menu', $alias)->with('success','Успешно удален');
    }
    public function deleteManager($id)
    {
        $food = Food::withTrashed()->where('id',$id);
        $alias = $food->first()->restaurant->alias;
        $order_food = OrderFood::where('food_id',$id);
        if($order_food){
            $order_food->delete();
        }
        $food->delete();
        return response()->json('Ok');
    }

    public function restore($id)
    {
        Food::withTrashed()->where('id',$id)->restore();
        return redirect()->back()->with('success','Успешно восстановлен');
    }
    public function switchFood($id)
    {
        $food = Food::withTrashed()->where('id',$id)->first();
        if($food->deleted_at !== null){
            Food::withTrashed()->where('id',$id)->restore();
        }
        else{
            Food::withTrashed()->where('id',$id)->delete();
        }
        return response()->json('Ok');
    }

    public function params($id)
    {
        $food = Food::find($id);
        $data = $food->params()->get();
        $name = $food->name_ru;
        return view('backend.params.index', compact('data','name','id'));
    }

    public function size($id)
    {
        $data = Food::find($id)->size()->get();
        return view('backend.size.index', compact('data','id'));
    }
}
