<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(){
        return view('layouts.react');
    }
    public function filter(){
        return view('frontend.filter');
    }    
    public function categories(){
        return view('frontend.categories');
    }
    public function history(){
        return view('frontend.history');
    }    
    public function basket(){
        return view('frontend.basket');
    }        
    public function events(){
        return view('frontend.event');
    }
    public function profile(){
        return view('frontend.profile');
    }
    public function info(){
        return view('frontend.info');
    }
    public function inner(){
        return view('frontend.inner');
    }
    public function food(){
        return view('frontend.food');
    }
    public function rest(){
        return view('frontend.rest');
    }
    public function method(){
        return view('frontend.method');
    }
    public function description(){
        return view('frontend.description');
    }
    public function search(){
        return view('frontend.search');
    }
    public function verify(){
        return view('frontend.verify');
    }    
    public function mark(){
        return view('frontend.mark');
    }
    public function number(){
        return view('frontend.number');
    }    
}