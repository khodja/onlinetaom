<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Review;
use App\Order;

class ReviewController extends Controller
{
    public function index($id)
    {
        // $data = Order::find($id)->review();
        $data = Review::where('order_id', $id)->get();
        return view('backend.review.index',compact('data','id'));
    }
    public function show()
    {
        $data = Review::all();
         return view('backend.review.index',compact('data'));
    }
    /**
     * Show Review create form
     * @return View
     */
    public function create($id)
    {
        return view('backend.review.create',compact('id'));
    }

    /**
     * Store Review
     * @param Request $request
     * @return Response
     */
    public function store(Request $request,$id)
    {
        request()->validate([
            'mark' => 'required',
            'message' => 'required',
        ]);
        $review = Review::create([
            'mark'=>request('mark'),
            'message'=>request('message'),
            'order_id'=> $id
        ]);

        return response()->json('Ok',200);
    }

    /**
     * Show Review edit form
     * @param $id
     * @return View
     */
    public function edit($id)
    {
        $data = Review::find($id);
        return view('backend.review.edit', compact('data','id'));
    }
    /**
     * Delete Review
     * @param Request $request
     * @return Response
     */
    public function delete($id)
    {
        $data = Review::where('order_id',$id);
        $data->delete();
        return redirect()->action('PreviewController@index')->with('success','Успешно удален');
    }
}
