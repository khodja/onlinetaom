<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Favorite;
use App\Restaurant;
use App\Food;
use App\Order;
use App\OrderFood;
use App\Role;
use App\User;
use Auth;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->hasRole('manager')){
            $restaurant = Auth::user()->restaurant;
            $data = Order::withTrashed()->where('restaurant_id',$restaurant->id)->take(10)->get();
            $rest = $restaurant->name;
            $food = Food::where('restaurant_id',$restaurant->id)->count();
            $order = Order::withTrashed()->where('restaurant_id',$restaurant->id)->count();
//            $notification = Order::withTrashed()->where('restaurant_id',$restaurant->id)->take(4)->get();
            $favorite = Favorite::where(['foreign_id' => $restaurant->id,'type'=>'restaurant'])->count();
            $manager = Auth::user()->name;
            $member = null;
            $users = null;

            $today = OrderFood::withTrashed()->whereIn('food_id',Auth::user()->foodIds())->whereDay('updated_at', Carbon::today())->groupBy('food_id')->select('food_id',\DB::raw('count(order_foods.food_id) as food_count'))->orderBy(\DB::raw('count(food_id)'), 'DESC')->with('food')->get();
            $yesterday = OrderFood::withTrashed()->whereIn('food_id',Auth::user()->foodIds())->whereDay('updated_at', Carbon::yesterday())->groupBy('food_id')->select('food_id',\DB::raw('count(order_foods.food_id) as food_count'))->orderBy(\DB::raw('count(food_id)'), 'DESC')->with('food')->get();
            $month = OrderFood::withTrashed()->whereIn('food_id',Auth::user()->foodIds())->whereMonth('updated_at', Carbon::today()->month)->groupBy('food_id')->select('food_id',\DB::raw('count(order_foods.food_id) as food_count'))->orderBy(\DB::raw('count(food_id)'), 'DESC')->with('food')->get();

            $orderToday = Order::withTrashed()->where('restaurant_id',$restaurant->id)->whereDay('updated_at', Carbon::today())->sum('total_price') - Order::withTrashed()->where('restaurant_id',$restaurant->id)->where('updated_at', '=', Carbon::today())->sum('shipping_price');
            $orderYesterday = Order::withTrashed()->where('restaurant_id',$restaurant->id)->whereDay('updated_at',Carbon::yesterday())->sum('total_price') - Order::withTrashed()->where('restaurant_id',$restaurant->id)->whereDay('updated_at',Carbon::yesterday())->sum('shipping_price');
            $orderWeek = Order::withTrashed()->where('restaurant_id',$restaurant->id)->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('total_price')
                - Order::withTrashed()->where('restaurant_id',$restaurant->id)->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('shipping_price');
            $orderMonth = Order::withTrashed()->where('restaurant_id',$restaurant->id)->whereMonth('updated_at', Carbon::today()->month)->sum('total_price') - Order::withTrashed()->where('restaurant_id',$restaurant->id)->whereMonth('updated_at', Carbon::today()->month)->sum('shipping_price');

            $orderTodayPayme = Order::withTrashed()->where([['restaurant_id',$restaurant->id],['payment_method','1']])->whereDay('updated_at', Carbon::today())->sum('total_price') - Order::withTrashed()->where([['restaurant_id',$restaurant->id],['payment_method','1']])->where('updated_at', '=', Carbon::today())->sum('shipping_price');
            $orderYesterdayPayme = Order::withTrashed()->where([['restaurant_id',$restaurant->id],['payment_method','1']])->whereDay('updated_at',Carbon::yesterday())->sum('total_price') - Order::withTrashed()->where([['restaurant_id',$restaurant->id],['payment_method','1']])->whereDay('updated_at',Carbon::yesterday())->sum('shipping_price');
            $orderWeekPayme = Order::withTrashed()->where([['restaurant_id',$restaurant->id],['payment_method','1']])->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('total_price')
                - Order::withTrashed()->where([['restaurant_id',$restaurant->id],['payment_method','1']])->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('shipping_price');
            $orderMonthPayme = Order::withTrashed()->where([['restaurant_id',$restaurant->id],['payment_method','1']])->whereMonth('updated_at', Carbon::today()->month)->sum('total_price') - Order::withTrashed()->where([['restaurant_id',$restaurant->id],['payment_method','1']])->whereMonth('updated_at', Carbon::today()->month)->sum('shipping_price');
        }
        else{
            $restaurant = Restaurant::withTrashed()->get();
            $rest = $restaurant->count();
            $food = Food::withTrashed()->count();
            $order = Order::withTrashed()->count();
//            $notification = Order::take(4)->get();
            $favorite = Favorite::withTrashed()->count();
            $data1 = Role::where('type','manager')->first();
            $data2 = Role::where('type','member')->first();
            $manager = $data1->users->count();
            $member = $data2->users->count();
            $data = Order::withTrashed()
                ->with(['restaurant' => function ($q) {
                   $q->withTrashed();
                }])
            ->latest()->take(10)->get();

            $users = User::withCount('orders')->orderBy('orders_count','DESC')->take(5)->get();


            $today = OrderFood::withTrashed()->whereDay('updated_at', Carbon::today())->groupBy('food_id')->select('food_id',\DB::raw('count(order_foods.food_id) as food_count'))->orderBy(\DB::raw('count(food_id)'), 'DESC')->with('food')->get();
            $yesterday = OrderFood::withTrashed()->whereDay('updated_at',Carbon::yesterday())->groupBy('food_id')->select('food_id',\DB::raw('count(order_foods.food_id) as food_count'))->orderBy(\DB::raw('count(food_id)'), 'DESC')->with('food')->get();
            $month = OrderFood::withTrashed()->whereMonth('updated_at', Carbon::today()->month)->groupBy('food_id')->select('food_id',\DB::raw('count(order_foods.food_id) as food_count'))->orderBy(\DB::raw('count(food_id)'), 'DESC')->with('food')->get();

            $orderToday = Order::withTrashed()->whereDay('updated_at', Carbon::today())->sum('total_price') - Order::withTrashed()->whereDay('updated_at', Carbon::today())->sum('shipping_price');
            $orderYesterday = Order::withTrashed()->whereDay('updated_at',Carbon::yesterday())->sum('total_price') - Order::withTrashed()->whereDay('updated_at',Carbon::yesterday())->sum('shipping_price');
            $orderWeek = Order::withTrashed()->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('total_price')
                - Order::withTrashed()->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('shipping_price');
            $orderMonth = Order::withTrashed()->whereMonth('updated_at', Carbon::today()->month)->sum('total_price') - Order::withTrashed()->where('updated_at', '=', Carbon::today()->month)->sum('shipping_price');


            $orderTodayPayme = Order::withTrashed()->where('payment_method','1')->whereDay('updated_at', Carbon::today())->sum('total_price') - Order::withTrashed()->where('payment_method','1')->whereDay('updated_at', Carbon::today())->sum('shipping_price');
            $orderYesterdayPayme = Order::withTrashed()->where('payment_method','1')->whereDay('updated_at',Carbon::yesterday())->sum('total_price') - Order::withTrashed()->where('payment_method','1')->whereDay('updated_at',Carbon::yesterday())->sum('shipping_price');
            $orderWeekPayme = Order::withTrashed()->where('payment_method','1')->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('total_price')
                - Order::withTrashed()->where('payment_method','1')->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('shipping_price');
            $orderMonthPayme = Order::withTrashed()->where('payment_method','1')->whereMonth('updated_at', Carbon::today()->month)->sum('total_price') - Order::withTrashed()->where('payment_method','1')->where('updated_at', '=', Carbon::today()->month)->sum('shipping_price');
        }
        return view('home',
            compact('rest', 'food','order','favorite','manager','member','users','data',
                            'today','yesterday','month','orderToday','orderYesterday','orderMonth',
                            'orderTodayPayme','orderYesterdayPayme','orderMonthPayme','orderWeekPayme',
                            'orderWeek','restaurant'));
    }
    public function info()
    {
        if (Auth::user()->hasRole('manager')) {
            $restaurant = Auth::user()->restaurant;
            $rest = $restaurant->name;
            $food = Food::where('restaurant_id', $restaurant->id)->count();
            $order = Order::withTrashed()->where('restaurant_id', $restaurant->id)->count();
            $favorite = Favorite::where(['foreign_id' => $restaurant->id, 'type' => 'restaurant'])->count();
            $manager = Auth::user()->name;
            $manager_phone = Auth::user()->email;

            $today = OrderFood::withTrashed()->whereIn('food_id', Auth::user()->foodIds())->whereDay('updated_at', Carbon::today())->groupBy('food_id')->select('food_id', \DB::raw('count(order_foods.food_id) as food_count'))->orderBy(\DB::raw('count(food_id)'), 'DESC')->with('food')->get();
            $yesterday = OrderFood::withTrashed()->whereIn('food_id', Auth::user()->foodIds())->whereDay('updated_at', Carbon::yesterday())->groupBy('food_id')->select('food_id', \DB::raw('count(order_foods.food_id) as food_count'))->orderBy(\DB::raw('count(food_id)'), 'DESC')->with('food')->get();
            $month = OrderFood::withTrashed()->whereIn('food_id', Auth::user()->foodIds())->whereMonth('updated_at', Carbon::today()->month)->groupBy('food_id')->select('food_id', \DB::raw('count(order_foods.food_id) as food_count'))->orderBy(\DB::raw('count(food_id)'), 'DESC')->with('food')->get();

            $orderToday = Order::withTrashed()->where('restaurant_id', $restaurant->id)->whereDay('updated_at', Carbon::today())->sum('total_price') - Order::withTrashed()->where('restaurant_id', $restaurant->id)->where('updated_at', '=', Carbon::today())->sum('shipping_price');
            $orderYesterday = Order::withTrashed()->where('restaurant_id', $restaurant->id)->whereDay('updated_at', Carbon::yesterday())->sum('total_price') - Order::withTrashed()->where('restaurant_id', $restaurant->id)->whereDay('updated_at', Carbon::yesterday())->sum('shipping_price');
            $orderWeek = Order::withTrashed()->where('restaurant_id', $restaurant->id)->whereBetween('updated_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('total_price')
                - Order::withTrashed()->where('restaurant_id', $restaurant->id)->whereBetween('updated_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('shipping_price');

//            $orderMonth = Order::withTrashed()->where('restaurant_id', $restaurant->id)->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('total_price')
//                - Order::withTrashed()->where('restaurant_id', $restaurant->id)->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('shipping_price');
            $orderMonth = Order::withTrashed()->where('restaurant_id', $restaurant->id)->whereMonth('updated_at', Carbon::today()->month)->sum('total_price') - Order::withTrashed()->where('restaurant_id', $restaurant->id)->whereMonth('updated_at', Carbon::today()->month)->sum('shipping_price');

            $orderTodayPayme = Order::withTrashed()->where([['restaurant_id', $restaurant->id], ['payment_method', '1']])->whereDay('updated_at', Carbon::today())->sum('total_price') - Order::withTrashed()->where([['restaurant_id', $restaurant->id], ['payment_method', '1']])->where('created_at', '=', Carbon::today())->sum('shipping_price');
            $orderYesterdayPayme = Order::withTrashed()->where([['restaurant_id', $restaurant->id], ['payment_method', '1']])->whereDay('updated_at', Carbon::yesterday())->sum('total_price') - Order::withTrashed()->where([['restaurant_id', $restaurant->id], ['payment_method', '1']])->whereDay('updated_at', Carbon::yesterday())->sum('shipping_price');
            $orderWeekPayme = Order::withTrashed()->where([['restaurant_id', $restaurant->id], ['payment_method', '1']])->whereBetween('updated_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('total_price')
                - Order::withTrashed()->where([['restaurant_id', $restaurant->id], ['payment_method', '1']])->whereBetween('updated_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('shipping_price');
//            $orderMonthPayme = Order::withTrashed()->where([['restaurant_id', $restaurant->id], ['payment_method', '1']])->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('total_price')
//                - Order::withTrashed()->where([['restaurant_id', $restaurant->id], ['payment_method', '1']])->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('shipping_price');
            $orderMonthPayme = Order::withTrashed()->where([['restaurant_id', $restaurant->id], ['payment_method', '1']])->whereMonth('updated_at', Carbon::today()->month)->sum('total_price') - Order::withTrashed()->where([['restaurant_id', $restaurant->id], ['payment_method', '1']])->whereMonth('updated_at', Carbon::today()->month)->sum('shipping_price');
        }
        return response()->json(array('rest'=>$rest, 'food'=>$food,'order'=>$order,'favorite'=>$favorite,'manager'=>$manager,'manager_phone'=>$manager_phone,
                            'today'=>$today,'yesterday'=>$yesterday,'month'=>$month,'orderToday'=>$orderToday,'orderYesterday'=>$orderYesterday,'orderMonth'=>$orderMonth,
                            'orderTodayPayme'=>$orderTodayPayme,'orderYesterdayPayme'=>$orderYesterdayPayme,'orderMonthPayme'=>$orderMonthPayme,'orderWeekPayme'=>$orderWeekPayme,
                            'orderWeek'=>$orderWeek,'restaurant'=>$restaurant));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function managment()
    {
        $rest = Restaurant::count();
        $food = Food::count();
        $order = Order::count();
        $favorite = Favorite::count();
        $manager = Role::where('type','manager')->with('users')->count();
        $member = Role::where('type','member')->with('users')->count();

        return view('managment',compact('rest', 'food','order','favorite','manager','member'));
    }
}
