<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Role;
use App\Restaurant;

class UserController extends Controller
{
    public function index()
    {
        $data = User::whereHas('roles', function ($query) {
                $query->where('type', 'member');
        })->with('restaurant','roles')->get();
        return view('backend.users.index',compact('data'));
    }
    public function orders($id)
    {
        $data = Order::where('user_id',$id)->orderBy('updated_at','DESC')->paginate(10);
        $user = User::find($id)->phone;
        return view('backend.order.index',compact('data','user'));
    }
    public function manager()
    {
        $data = User::whereHas('roles', function ($query) {
                $query->where('type', 'manager');
        })->with('restaurant','roles')->get();
        $count = 0;
        foreach($data as $item){
            if($item->restaurant_id != null){
                $count = $count + ($item->restaurant->deleted_at == null ? 0 : 1);
            }
        }
        if($count == 0) {
            $count = '0.00';
        }
        return view('backend.users.index',compact('data','count'));
    }
    public function moderator()
    {
        $data = User::whereHas('roles', function ($query) {
                $query->where('type', 'admin');
        })->with('restaurant','roles')->get();
        return view('backend.users.index',compact('data'));
    }
    public function callcenter()
    {
        $data = User::whereHas('roles', function ($query) {
                $query->where('type', 'callcenter');
        })->with('restaurant','roles')->get();
        return view('backend.users.index',compact('data'));
    }

    public function check()
    {
        $data = Auth::user();
        return response()->json($data);
    }
    public function edit($id)
    {
        $data = User::findOrFail($id);
        $role = Role::all();
        $rest = Restaurant::all();

        return view('backend.users.edit',compact('data','role','rest'));
    }
    public function create()
    {
        $data = Role::all();
        $rest = Restaurant::all();
        return view('backend.users.create', compact('data','rest'));
    }
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required',
            'phone' => 'required|unique:users',
        ]);
        $role = Role::where('type', request('role'))->first();
        $data = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'restaurant_id' => request('restaurant_id'),
            'phone' => request('phone'),
            'password' => bcrypt(request('password'))

        ]);
        $data->roles()->attach($role);
        return redirect()->action('UserController@index')->with('success', 'Вы добавили ползователя');
    }
    public function update(Request $request , $id)
    {
        $role = Role::where('type', request('role'))->first();
        $data = User::find($id);
        if($request->password != null){
            User::find($id)->update(['password' => bcrypt($request->password)]);
        }
        $data->update($request->except('password'));
        $data->roles()->sync($role);

        return redirect()->action('UserController@index')->with('success', 'Вы изменили ползователя');
    }
    public function delete($id)
    {
        $user = User::find($id);
//        if($user->hasRole('manager')){
//            $user->forceDelete();
//            }
//        else{
        $user->delete();
//        }
        return redirect()->back()->with('success', 'Вы удалили ползователя');
    }
    public function change(Request $request )
    {
        $user = Auth::user();
        $user->update([
            'name' => request('name'),
            'email' => request('email'),
            'adress' => request('adress'),
        ]);

        return response()->json($user, 200);
    }
    public function setToken(Request $request){
        if(User::where('id',request()->user()->id)->update(['token'=>$request->input('token')])) {
            return response()->json('Ok',200);
        }
        else{
            return response()->json('Ok',400);
        }
    }
}
