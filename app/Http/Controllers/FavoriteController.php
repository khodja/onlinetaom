<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Favorite;
use App\Restaurant;
use App\Food;

use Auth;
use Intervention\Image\ImageManagerStatic as Image;

class FavoriteController extends Controller
{
    public function index()
    {
        if(!Auth::user()->hasRole('admin')){
            $rest = Auth::user()->restaurant;
            $data = Favorite::where(['foreign_id' => $rest->id,'type'=>'restaurant'])->get();
//            $data = Favorite::where(['foreign_id' => $rest->id,'type'=>'restaurant'])->orWhere(['type'=>'food','foreign_id'=>$rest->getFoodId()])->get();
        }
        else{
            $data = Favorite::all();
        }

        return view('backend.favorite.index',compact('data'));
    }
    public function get()
    {
        $user = Auth::user();
        $data = $user->favorites;

        return response()->json($data,200);
    }
    public function getFavorite($id)
    {
        $user = Auth::user();
        $data = Favorite::where(['type' => 'restaurant' ,'user_id' => $user->id , 'foreign_id' => $id])->get();

        return response()->json($data);
    }
    public function getFavoriteFood($id)
    {
        $user = Auth::user();
        $data = Favorite::where(['type' => 'food' ,'user_id' => $user->id , 'foreign_id' => $id])->get();

        return response()->json($data);
    }
    public function store()
    {
        $user = Auth::user();
        $type = request('type');
        $foreign_id = request('id');

        $check =  Favorite::withTrashed()->where(['type' => $type ,'user_id' => $user->id , 'foreign_id' => $foreign_id])->first();
        if($check != null){
            Favorite::withTrashed()->find($check->id)->restore();
            $data = $check->id;
        }
        else{
            $data = Favorite::create([
                'user_id'=> $user->id,
                'type'=>$type,
                'foreign_id'=>$foreign_id
            ]);
        }
        return response()->json($data, 201);
    }
    public function delete($id)
    {
        $data = Favorite::find($id);
        $data->delete();
        return response()->json(true, 204);
    }

}
