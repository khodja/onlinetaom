<?php

namespace App\Http\Controllers;

use App\Carrier;
use App\CarrierOrder;
use App\Restaurant;
use Illuminate\Http\Request;

use App\Order;
use App\Food;
use App\OrderFood;
use App\User;
use App\Size;
use App\Param;
use App\Notifications\OrderAdded;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use Auth;
use Carbon\Carbon;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\Topics;
use GuzzleHttp\Client;

class OrderController extends Controller
{
    public function index()
    {
//            $this->sendToRest(Restaurant::find(1)->user->token,'Поступил новый заказ','Нажмите для открытия');

        if(Auth::user()->hasRole('manager')){
            $restId = Auth::user()->restaurant()->first()->id;
            $data = Order::withTrashed()->where('restaurant_id',$restId)->orderBy('updated_at','DESC')->paginate(10);
        }
        else{
            $data = Order::withTrashed()->orderBy('updated_at','DESC')->paginate(10);
        }

        return view('backend.order.index',compact('data'));
    }
    public function indexManager()
    {
        $restId = Auth::user()->restaurant()->first()->id;
        $data = Order::withTrashed()->where('restaurant_id',$restId)->whereNotIn('status',[0,3,4])->orderBy('updated_at','DESC')->get();

        return response()->json($data);
    }
    public function showInnerManager($id)
    {
        $data =  Order::where('id',$id)->with(['order_food.food','restaurant','user'])->first();
        return response()->json($data);
    }
    public function show()
    {
        $user = Auth::user();
        $data =  Order::where([['user_id' ,$user->id]])->with(['order_food.food','user','restaurant.categories'])->latest()->first();
        return response()->json($data);
    }
    public function showInner($id)
    {
        $data =  Order::withTrashed()->where('id',$id)->with(['order_food_trashed.food','restaurant_trashed','user'])->first();
        return view('backend.order.show',compact('data'));
    }
    public function inner($id)
    {
        $user = Auth::user();
        $data =  Order::where([['user_id' ,$user->id],['id',$id]])->with(['order_food.food','user','restaurant.categories'])->latest()->first();

        return response()->json($data);
    }
    public function history()
    {
        $user = Auth::user();
        $data =  Order::where([['user_id' ,$user->id],['restaurant_id', '!=', 0],['status', '!=', 0]])->with(['order_food.food','user','restaurant.categories'])->latest()->get();
        return response()->json($data);
    }
    public function historyManager()
    {
         $restId = Auth::user()->restaurant()->first()->id;
        $data =  Order::where('restaurant_id', $restId)->whereNotIn('status',[0,1,2])->with(['order_food.food','user','restaurant.categories'])->get();
        return response()->json($data);
    }
    public function import()
    {
        $response = Excel::download(new UsersExport, 'OnlineTaom-Заказы.xlsx');
        return $response;
    }
    public function basket()
    {
        $user = Auth::user();
        $order =  $user->orders->last();
        $order_food = $order->order_food;
        $price = $order_food->pluck('price');
        $amount = $order_food->pluck('amount');
        $data['restId'] = $order->restaurant_id;
        if($order->restaurant_id != 0){
            $data['payments'] = $order->restaurant->payments;
        }
        else{
            $data['payments'] = null;
        }
        $data['price'] = 0;
        $data['amount'] = 0;
        foreach($price as $item){
            $data['price'] += $item;
        }
        foreach($amount as $item){
            $data['amount'] += $item;
        }
        return response()->json($data);
    }
    public function getOrderBy($id){
        if(Auth::user()->hasRole('manager')){
            $restId = Auth::user()->restaurant()->first()->id;
            $order = Order::withTrashed()->where('restaurant_id',$restId);
        }
        else{
            $order = Order::withTrashed();
        }
        if($id == 'month'){
            $data = $order->whereMonth('updated_at',Carbon::today()->month)->with('carrier_order')->latest()->paginate(500);
        }
        if($id == 'week'){
            $data = $order->whereBetween('updated_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->with('carrier_order')->latest()->paginate(500);
        }
        if($id == 'day'){
            $data = $order->whereDay('updated_at',Carbon::today())->with('carrier_order')->latest()->paginate(500);
        }

        return view('backend.order.index',compact('data','id'));
    }
    public function getBetween(){
        if(Auth::user()->hasRole('manager')){
            $restId = Auth::user()->restaurant()->first()->id;
            $order = Order::withTrashed()->where('restaurant_id',$restId);
        }
        else{
            $order = Order::withTrashed();
        }
        $from = request('from');
        $to = request('to');
        $data = $order->whereBetween('updated_at', [$from, $to])->latest()->get();
        return view('backend.order.index',compact('data','from','to'));
    }
    public function store(Request $request, $id)
    {
        $user = Auth::user();
        $order = Order::where('user_id',$user->id)->get();
        $food = Food::findOrFail($id);
        $size_price = 0;
        $param_price = 0;

        if(request('size_id') !== null){
            $size = Size::withTrashed()->where('id',request('size_id'))->first();
            $size_price = $size->price;
        }

        $params = json_decode(request('params'));
        if(request('params') !== null){
            $param = Param::withTrashed()->whereIn('id',$params)->get();
            foreach($param as $itemprice){
                $param_price += $itemprice->price;
            }
        }

        $order_food = OrderFood::create([
            'order_id'=>$order->last()->id,
            'amount'=>request('amount'),
            'size_id'=>request('size_id'),
            'price'=> ($size_price == 0 ? $food->price + $param_price : $size_price + $param_price) * request('amount'),
            'params'=> request('params'),
            'food_id'=>$food->id
        ]);
        $order->last()->update([
            'restaurant_id'=> $food->restaurant_id,
        ]);
         $order_food->order()->associate($order);
        return response()->json($order_food, 201);
    }
    public function storeMobile(Request $request)
    {
        $user = Auth::user();
        $order = Order::where('user_id',$user->id)->get();
        $size_price = 0;
        $param_price = 0;
        $order_food = json_decode(request('order_food'));
        foreach ($order_food as $data){
        if($data->size_id !== null){
            $size = Size::withTrashed()->where('id',$data->size_id)->first();
            $size_price = $size->price;
        }

        $params = json_decode($data->params);
        if($data->params !== null){
            $param = Param::withTrashed()->whereIn('id',$params)->get();
            foreach($param as $itemprice){
                $param_price += $itemprice->price;
            }
        }
        $food = Food::where('id',$data->food_id)->first();
        $order_food = OrderFood::create([
            'order_id'=>$order->last()->id,
            'amount'=> $data->amount,
            'size_id'=> $data->size_id || null,
            'price'=> ($size_price == 0 ? $data->price + $param_price : $size_price + $param_price) * $data->amount,
            'params'=>  $data->params,
            'food_id'=>$food->id
        ]);
        $order->last()->update([
            'restaurant_id'=> $food->restaurant_id,
        ]);
        $order_food->order()->associate($order);
        }
        return response()->json($order_food, 201);
    }
    public function update($id)
    {
        $order = OrderFood::findOrFail($id);
        if($order->size_id == 0){
            $food_price = $order->food->price;
        }
        else{
            $food_price = Size::withTrashed()->where('id',$order->size_id)->first()->price;
        }
        $param_price = 0;
        $param = Param::withTrashed()->whereIn('id',json_decode($order->params))->get();
        foreach($param as $price){
            $param_price += $price->price;
        }
        $order->update([
            'amount'=>request('amount'),
            'price'=> ($food_price + $param_price) * request('amount'),
        ]);
        return response()->json($order, 200);
    }
    public function checkout( $id)
    {
        $user = Auth::user();
        $order = $user->orders()->latest()->first();
        $price = $order->order_food->pluck('price');
        $total_price = 0;
        foreach($price as $item){
            $total_price += $item;
        }
        if($order->id == $id){
        $data = Order::findOrFail($id);
        $check = $data->update([
            'status'=> 1,
            'payment_method'=> request('payment_method'),
            'shipping_time'=> request('shipping_time'),
            'shipping_price'=> request('shipping_price'),
            'distance'=> request('distance'),
            'adress'=> request('adress'),
            'comment'=> request('comment'),
            'long'=> request('long'),
            'lat'=> request('lat'),
            'total_price'=> $total_price + (int)request('shipping_price'),
            'delivery_delta'=> $order->restaurant->delivery_delta,
            'ot_delta'=> $order->restaurant->ot_delta,
        ]);
        if(request('payment_method') != 1) {
            $client = new Client();
            $response = $client->post('http://91.204.239.42:8083/broker-api/send', [
                'auth' => ['onlinetaom', 'c6ppTR7'],
                'json' => [
                    "messages" => [
                        [
                            "recipient" => 998 . $order->user->phone,
                            "message-id" => "abc000000001",

                            "sms" => [
                                "originator" => "3700",
                                "content" => ["text" => "Ваш заказ №" . $order->id . " в обработке!"]
                            ]
                        ]
                    ]
                ]
            ]);
        }
        if($check){
            Order::create([
                'long'=> 1,
                'lat'=> 1,
                'adress'=> 'Не указан',
                'distance'=> 'Не указан',
                'device_info'=> 'Не указан',
                'payment_method'=> 0,
                'status'=> 0,
                'shipping_time'=> 0,
                'shipping_price'=> 0,
                'total_price'=> 0,
                'user_id'=>$user->id,
                'restaurant_id'=> 0,
                'comment'=> '',
            ]);
            User::find(1)->notify(new OrderAdded);
            if(request('payment_method') != 1){
                if($data->restaurant->user){
                    $data->restaurant->user->notify(new OrderAdded);
                    $managers = Restaurant::find($data->restaurant_id)->user()->get();
                    if($managers){
                        foreach ($managers as $manager){
                            if($manager->token){
                                $this->sendToRest($manager->token,null,null,['id'=>$id]);
                                $this->sendToRest($manager->token,'Поступил новый заказ','Нажмите для открытия');
                            }
                        }
                    }
                }
            }
        }
        $prc = $total_price + (int)request('shipping_price');
        $last = $prc * 100;
        $url = base64_encode('m=5ce696bff616bb624ed33352;ac.order_id='.$order->id.';a='.$last.';c=https://onlinetaom.uz/order/'.$order->id);
        $data = 'https://checkout.paycom.uz/'.$url;
            if(request('payment_method') == '1'){
                return response()->json(['url'=>$data], 200);
            } else{
                return response()->json(['url'=>''], 200);
            }
        }
        else{
            return response()->json('Not found');
        }
    }

    public function clear($id)
    {
        $order = Order::findOrFail($id);
        $order->order_food()->forceDelete();
        $order->update([
            'long'=> 1,
            'lat'=> 1,
            'adress'=> 'Не указан',
            'distance'=> 'Не указан',
            'restaurant_id'=> 0,
            'shipping_time'=> 0,
            'shipping_price'=> 0,
            'total_price'=> 0,
        ]);
        return response()->json('Ok', 204);
    }
    public function delete($id)
    {
        $orderfood = OrderFood::findOrFail($id);
        $order = Order::findOrFail($orderfood->order_id);
        $orderfood->delete();
        if($order->order_food()->count() == 0){
            $order->update([
                'long'=> 1,
                'lat'=> 1,
                'adress'=> 'Не указан',
                'distance'=> 'Не указан',
                'restaurant_id'=> 0,
                'shipping_time'=> 0,
                'shipping_price'=> 0,
                'total_price'=> 0,
            ]);
        }
        return response()->json(null, 204);
    }
    public function deleteOrder($id)
    {
        $order = Order::findOrFail($id);
        Order::create([
            'long'=> 1,
            'lat'=> 1,
            'adress'=> 'Не указан',
            'distance'=> 'Не указан',
            'device_info'=> 'Не указан',
            'payment_method'=> 0,
            'status'=> 0,
            'shipping_time'=> 0,
            'shipping_price'=> 0,
            'total_price'=> 0,
            'user_id'=>$order->user->id,
            'restaurant_id'=> 0,
            'comment'=> '',
        ]);
        $order->delete();
        return redirect()->back();
    }
    public function successOrder($id)
    {
        $order = Order::findOrFail($id);
        $check = CarrierOrder::where('order_id',$order->id)->first();
        if(!$check){
        CarrierOrder::create([
            'driver_id' => 0,
            'order_id' => $order->id,
            'restaurant_id' => $order->restaurant_id,
            'street_name' => $order->adress,
            'distance' => $order->distance,
            'delivery_time' => $order->shipping_time,
            'cook_time' => request('cooking_time'),
            'status' => 0,
            'delivery_price' => $order->shipping_price,
            'full_price' => $order->total_price,
            'delivery_delta'=> $order->delivery_delta,
            'ot_delta'=> $order->ot_delta,
        ]);
        $this->send(null,'Поступил новый заказ','Нажмите для открытия');
        if($order->user->token){
            $this->sendToClient($order->user->token,'Ресторан принял Ваш заказ!','Нажмите для открытия');
        }
        $order->update([
            'status'=>'2',
            'cooking_time'=> request('cooking_time'),
        ]);
        }
        return redirect()->back();
    }
    public function successOrderManager($id)
    {
        $order = Order::where([['id', $id], ['status', 1]])->firstOrFail();
        $check = CarrierOrder::where('order_id',$order->id)->first();
        if(!$check){
        CarrierOrder::create([
            'driver_id' => 0,
            'order_id' => $order->id,
            'restaurant_id' => $order->restaurant_id,
            'street_name' => $order->adress,
            'distance' => $order->distance,
            'delivery_time' => $order->shipping_time,
            'cook_time' => request('cooking_time'),
            'status' => 0,
            'delivery_price' => $order->shipping_price,
            'full_price' => $order->total_price,
            'delivery_delta'=> $order->delivery_delta,
            'ot_delta'=> $order->ot_delta,
        ]);
        $this->send(null,'Поступил новый заказ','Нажмите для открытия');
        $order->update([
            'status'=>'2',
            'cooking_time'=> request('cooking_time'),
        ]);
        return response()->json('Ok',200);
        }
    }
    public function ignoreOrder($id)
    {
        $order = Order::findOrFail($id);
        $send = $order;
        $order->update([
            'status'=>'-1'
        ]);
        $client = new Client();
        $response = $client->post( 'http://91.204.239.42:8083/broker-api/send', [
            'auth' => ['onlinetaom', 'c6ppTR7'],

            'json' => [
                    "messages" => [
                        [
                            "recipient" => 998 . $send->user->phone,
                            "message-id"=>"abc000000001",

                            "sms"=> [
                                "originator"=> "3700",
                                "content"=> ["text" => "Ваш заказ №".$send->id." отменен."]
                                ]
                            ]
                    ]
            ]
        ]);
        return redirect()->back();
    }
    public function ignoreOrderManager($id)
    {
        $order = Order::where([['id', $id], ['status', 1]])->firstOrFail();
        $send = $order;
        $order->update([
            'status'=>'-1'
        ]);
        $client = new Client();
        $response = $client->post( 'http://91.204.239.42:8083/broker-api/send', [
            'auth' => ['onlinetaom', 'c6ppTR7'],
            'json' => [
                    "messages" => [
                        [
                            "recipient" => 998 . $send->user->phone,
                            "message-id"=>"abc000000001",

                            "sms"=> [
                                "originator"=> "3700",
                                "content"=> ["text" => "Ваш заказ №".$send->id." отменен."]
                                ]
                            ]
                    ]
            ]
        ]);
        return response()->json('Ok',200);
    }
    public function send($token, $title, $body, $data=null){

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notification = $notificationBuilder->build();
        $notificationBuilder->setBody($body)->setSound('default')->setColor('#C2F62C');
        $topic = new Topics();
        $topic->topic('all');

        $topicResponse = FCM::sendToTopic($topic, null, $notification, null);
        $topicResponse->isSuccess();
        $topicResponse->shouldRetry();
        $topicResponse->error();
}
public function sendToRest($token, $title, $body, $data=null){
        config(['fcm.http.server_key' => 'AAAAs_tWLuY:APA91bGoXPwJFs3mk2MgChadcX0mYS0XdLW6Vm6Hp8RvljjsiEkkyv0UwX27tUs5lkXtnuZcS6NZDC9iioDjyU8xYmO_YQBgGXNpOd3ZrD9m1dKhTP_Vdz-y0I7rFwln_ekHsYs47dY-']);
        config(['fcm.http.sender_id' => '773015875302']);
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)->setColor('#C2F62C')->setClickAction('detailsscreen');

        $dataBuilder = new PayloadDataBuilder();
        if($data)$dataBuilder->addData($data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        if($title==null) $notification=null;

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
    }
public function sendToClient($token, $title, $body, $data=null){
        config(['fcm.http.server_key' => 'AAAAN932maA:APA91bHPsrfqyJ6Z9go0lhE2N3i86eNBB8uzOXeg-Kd-ktXjLk0MNyS2Q5vfYRPSzvUD8uFcZR8oHUI7psyCSzWEv79bsTMKAUig80TMHf-gY06rt6K5MyosK6PmSiDD5wnIRdqcesr0']);
        config(['fcm.http.sender_id' => '239947127200']);
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)->setColor('#C2F62C');

        $dataBuilder = new PayloadDataBuilder();
        if($data)$dataBuilder->addData($data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
    }
}
