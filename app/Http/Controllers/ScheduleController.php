<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Schedule;
use App\Restaurant;

class ScheduleController extends Controller
{
    public function index($id)
    {
        $data = Restaurant::where('alias', $id)->first()->schedules;
        return view('backend.schedule.index',compact('id','data'));
    }
    public function show($id)
    {
        $data = Restaurant::where('alias', $id)->first()->schedules;
        return $data;
    }
    public function create($id)
    {
        return view('backend.schedule.create',compact('id'));
    }
    public function edit($id)
    {
        $data = Schedule::find($id);
        return view('backend.schedule.edit',compact('data','id'));
    }
    public function update(Request $request)
    {
        $items = request()->primary_id;
        foreach($items as $k => $id){

            $values = [
                'open_hour' => request('open_hour')[$k],
                'close_hour' => request('close_hour')[$k],
            ];

            Schedule::where('id','=', $id)->update($values);
        }
        return redirect()->back()->with('success','Успешно изменено');
    }
    public function delete($id)
    {
        $data = Schedule::findOrFail($id);
        $data->delete();
        return response()->json(null, 204);
    }
}
