<?php

namespace App\Http\ViewComposers;

use App\Order;

class NavComposer
{
    public function compose($view)
    {
        $view->with('notification', Order::latest()->take(4)->get());
    }
}
