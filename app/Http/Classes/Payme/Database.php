<?php

namespace App\Http\Classes\Payme;

class Database
{
    public $config;

    protected static $db;

    public function __construct(array $config = null)
    {
        $this->config = $config;
    }

    public function new_connection()
    {
        $db = null;

        // connect to the database
        $db_options = [
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC, // fetch rows as associative array
            \PDO::ATTR_PERSISTENT         => true // use existing connection if exists
        ];

        $db = new \PDO(
            'mysql:dbname=' . $this->config['db']['database'] . ';host=' . $this->config['db']['host'] . ';charset=utf8',
            $this->config['db']['username'],
            $this->config['db']['password'],
            $db_options
        );

        return $db;
    }

    /**
     * Connects to the database
     * @return null|\PDO connection
     */
    public static function db()
    {
        if (!self::$db) {
            $config   =  [
                'merchant_id' => '5cb5f7e9e5c4124f014a1bbc',
                'login'       => 'Paycom',
                'keyFile'     => 'bBGgo7wR@WaS1Ww9w0oo%&pITsdo%g9Z?P8e',
                'db'          => [
                    'host'     => '127.0.0.1',
                    'database' => 'admin_default',
                    'username' => 'admin_default',
                    'password' => 'uDMcyZSPn4'
                ]
            ];;
            $instance = new self($config);
            self::$db = $instance->new_connection();
        }

        return self::$db;
    }
}
