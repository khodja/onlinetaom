<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarrierBilling extends Model
{
    protected $connection='carrier';
    protected $table='billings';
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['amount','payment'];


    public function user()
    {
        return $this->belongsTo('App\Carrier');
    }
}
