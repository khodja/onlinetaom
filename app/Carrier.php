<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrier extends Model
{
    protected $connection='carrier';
    protected $table='users';

    protected $fillable = [
        'name', 'phone_number', 'car_number', 'card_number','password'
    ];
    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'rating' => 0,
        'location' => '',
        'online' => true,
        'device' => '',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
    public function order(){
        return $this->belongsTo('App\Order');
    }
}
