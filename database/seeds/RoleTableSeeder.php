<?php

use Illuminate\Database\Seeder;

use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $seedrole_1 = new Role;
      $seedrole_1->type = 'admin';
      $seedrole_1->save();
      $seedrole_2 = new Role;
      $seedrole_2->type = 'manager';
      $seedrole_2->save();
      $seedrole_3 = new Role;
      $seedrole_3->type = 'member';
      $seedrole_3->save();
      $seedrole_4 = new Role;
      $seedrole_4->type = 'callcenter';
      $seedrole_4->save();
    }
}
