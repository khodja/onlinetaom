<?php
/**
 * Created by PhpStorm.
 * User: javlontulkinoff
 * Date: 2019-03-12
 * Time: 14:03
 */

use Illuminate\Database\Seeder;

use App\Restaurant;

class RestaurantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $restaurant = new Restaurant;
        $restaurant->name = 'admin';
        $restaurant->desc_ru = 'admin@gmail.com';
        $restaurant->desc_uz = 'admin@gmail.com';
        $restaurant->phone = '998899';
        $restaurant->lat = 'admin@gmail.com';
        $restaurant->long = 'admin@gmail.com';
        $restaurant->long = 'address';
        $restaurant->alias = 'alias';
        $restaurant->save();

    }
}
