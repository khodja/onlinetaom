import React, { Component } from 'react';

export default class RestMobileBox extends Component {
    render() {
        return (
    <div className="restaurant--wrap">
        <a href="" className="restaurant__ins">
            <img src="/img/mobile1.png" className="restaurant__img" />
            <div className="restaurant__ins-p">
                <img src="/img/brand1.png" width="70" className="restaurant__brand" />
                <h5 className="restaurant__title">Caffee’issimo</h5>
                <p className="restaurant__desc">Европейская</p>
            </div>
            <div className="restaurant__ins-price">
            <div className="restaurant__price"><img src="/img/popularmob.svg" height="10" /> 12 000 сум</div> 
                <p className="restaurant__long">4.2 км</p>
            </div>           
        </a>
    </div>
        );
    }
}