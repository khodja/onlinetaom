import React, { Component } from 'react';
import axios from 'axios';

export default class FavoriteRest extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false,
            distance: null,
        }
    }
    componentDidMount() {
        this.getDistance();
    }
    getDistance = () => {
        let destination = (JSON.parse(localStorage.getItem('location')));
        let data = {
            restaurantId: this.props.id,
            origin: this.props.lat + ',' + this.props.long,
            destination: destination.lat + ',' + destination.long
        };
        let headers = {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('token')} }`
            }
        };
        axios.post("https://onlinetaom.uz/api/distance", data, headers)
            .then(result => {
                    this.setState(prevState => ({
                        ...prevState,
                        distance: result.data,
                    }));
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }
    render() {
        return (
<div>            
<div className="restaurant-desktop__ins desktop">
    <div className="col-md-6 col-sm-6 col-xs-6">
        <img src={this.props.image} width="70" className="img br50" />
        <div className="restaurant-xs vm">
                <div className="restaurant-desktop__ins--title">
                    {this.props.name}
                </div>
                <div className="restaurant-desktop__ins-p">
                    {Object.keys(this.props.categories).length === 0 ? `Без категории` : this.props.categories[0].name_ru }
                </div>
        </div>
    </div>
    <div className="col-md-6 col-sm-6 col-xs-6 text-right">
        <div className="restaurant-desktop__long">
            {this.state.distance ? `${this.state.distance.distance}` : 'Вычесление ...'}
        </div>
        <div className="restaurant-desktop__location">
            <img src="/img/greentruck.svg" /> 
            {this.state.distance ? `${this.state.distance.price} сум` : 'Вычесление ...'}
        </div>     
    </div>
    <button className="delete" onClick={this.props.delete} id={this.props.deleteId}>
        <img src="/img/delete.svg" className="vm" id={this.props.deleteId} />
    </button>
    <div className="clearfix"></div>
</div>
<div className="restaurant mobile">
    <div className="restaurant__ins mobile">
        <div className="restaurant__name">
            <img src={this.props.image} width="60" className="img" />
            <span>
                {this.props.name}
            </span>
            <button className="delete" onClick={this.props.delete} id={this.props.deleteId}>
                <img src="/img/delete.svg"  id={this.props.deleteId} />
            </button>                    
        </div>
        <div className="restaurant__ins-p--2 pt-5">
            {Object.keys(this.props.categories).length === 0 ? ` ` : this.props.categories[0].name_ru }
        </div>
        <div className="restaurant__ins-price--2">
        <p className="restaurant__long pr-25">{this.state.distance ? `${this.state.distance.distance}` : 'Вычесление ...'}</p>
        <div className="restaurant__price"><img src="/img/popularmob.svg" height="10" /> {this.state.distance ? `${this.state.distance.price} сум` : 'Вычесление ...'}</div>
        </div>           
    </div>
</div>
</div>
        );
    }
}
