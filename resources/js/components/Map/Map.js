import React, { Component } from 'react';
import { compose, withProps, lifecycle } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap , Marker} from "react-google-maps"

let _lat , _lng , location, _map;
const MyMapComponent = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyDs3cvxdAATTvzZ-srgPAID1d2IZHuZcZE&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `100%` }} />,
        mapElement: <div style={{ height: `100%` }} />,
    }),
    lifecycle({
        componentWillMount() {
            const refs = {};
            const save = this.props.saveLocation();
            const hide = this.props.hide;
            const setLocation = this.props.setLocation;
            const change = this.props.change;
            this.setState({
                onMapMounted: ref => {
                    refs.map = ref;
                },

                onPositionChanged: () => {
                    let el = refs.map
                    _map = el.getCenter().toUrlValue(6)
                    var result = _map.split(",")
                    _lat = 1 * result[0]
                    _lng = 1 * result[1]
                    change(_lat,_lng)
                },
                goToMyLocation: () => {
                  setLocation()
                },
                onSubmit: (long,lat) => {
                    hide()
                    save(long,lat)
                }
            })
        },
        componentWillReceiveProps(nextProps){
            if(nextProps.lat){
                _lat = 1*nextProps.lat;
                _lng = 1*nextProps.lng;
            }
        },
    }),
    withScriptjs,
    withGoogleMap
)((props) =>
    props.lat
        ?
        <GoogleMap
            defaultZoom={15}
            center={{ lat: props.lat , lng : props.lng }}
            defaultOptions={{
                gestureHandling: "greedy",
                streetViewControl: false,
                scaleControl: false,
                mapTypeControl: false,
                panControl: false,
                zoomControl: false,
                rotateControl: false,
                fullscreenControl: false
            }}
             ref={props.onMapMounted} onDragEnd={props.onPositionChanged}>
            <button className="button" onClick={() => props.onSubmit(_lng,_lat)}>
                Подтвердить адрес
            </button>
            <button className="my-location" onClick={() => props.goToMyLocation()}>
                <img src="/img/mylocation.svg" />
            </button>
            <Marker
                icon="/bluecircle.png"
                position={{ lat: props.lat , lng : props.lng }}
            />
        </GoogleMap>
        : null
    )


export default class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            latitude : this.props.latitude,
            longitude : this.props.longitude
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.latitude){
        if(nextProps.latitude !== this.state.latitude) {
            this.setState({
                latitude: nextProps.latitude !== 0 ? nextProps.latitude : 41.311081,
                longitude: nextProps.longitude !== 0 ? nextProps.longitude : 69.240562,
            })
        }
        }
    }

    onChange = (lat,long) => {
        this.setState({
            latitude : lat,
            longitude: long
        })
    }
    setLocation = () => {
        let options = {
          enableHighAccuracy: false,
          timeout: 10000,
          maximumAge: 0
        };
        navigator.geolocation.getCurrentPosition((position) => {
            if(position.coords.longitude && position.coords.latitude){
                this.setState({
                    latitude:position.coords.latitude,
                    longitude:position.coords.longitude,
                });
            }
        },
        (err) => {
            this.setState({
                longitude: 69.240562,
                latitude: 41.311081,
            })
        }, options);
    }
    render() {
        return (
            <div className="map">
                <MyMapComponent
                    hide={this.props.hide}
                    change={this.onChange}
                    setLocation={this.setLocation}
                    saveLocation={(long , lat) => this.props.saveLocation}
                    lat={this.state.latitude}
                    lng={this.state.longitude}
                />
            </div>
        )
    }
}


