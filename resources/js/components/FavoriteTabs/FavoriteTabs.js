import React, { Component } from 'react';

export default class FavoriteTabs extends Component {
    render() {
        return (
        <div className="favorite">
            <button className={`favorite__btn ${this.props.active === 'restaurant' ? 'active' : ''}`} onClick={this.props.setActive} id="restaurant">Рестораны</button>
            <button className={`favorite__btn ${this.props.active === 'food' ? 'active' : ''}`} onClick={this.props.setActive} id="food">Блюда</button>
        </div>
        );
    }
}
