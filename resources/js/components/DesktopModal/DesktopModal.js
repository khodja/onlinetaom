import React, { Component } from 'react'
import Loader from 'react-loader-spinner'
import axios from "axios";


export default class DesktopModal extends Component {
    _isMounted = false;
    constructor(props) {
        super(props);

        this.state = {
            amount: 1,
            size_id: null,
            price: null,
            params: [],
            redirect: false,
            favorite: false,
            favoriteId: null,
            paramsPrice: 0,
        };

    }
    componentDidMount() {
        this._isMounted = true;
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.getFavorite();
        if(nextProps.information.price !== null){
            if (this._isMounted) {
                if(nextProps.information.size[0]){
                this.setState({
                    price: nextProps.information.size[0].price,
                    size_id: nextProps.information.size[0].id,
                })
                }
                else{
                this.setState({
                    price: nextProps.information.price
                })
                }
            }
        }
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    getFavorite = () => {
        if(localStorage.getItem('token') !== null && this.props.information){
            axios
                .get(`https://onlinetaom.uz/api/favorite/food/${this.props.information.id}`,
                    {
                        headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`}
                    })
                .then(result => {
                        if(Object.keys(result.data).length === 0){
                            this.setState(prevState => ({
                                ...prevState,
                                favorite: false,
                            }));
                        }
                        else {
                            this.setState(prevState => ({
                                ...prevState,
                                favorite: true,
                                favoriteId: result.data[0].id
                            }))
                        }
                    },
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                );
        }
    }
    chooseFavorite = () => {
        if(localStorage.getItem('token') !== null ) {
            let data = {
                type: 'food',
                id: this.props.information.id
            };
            let headers = {
                headers : {
                    'Authorization': `Bearer ${localStorage.getItem('token')} }`
                }
            };
            axios.post("https://onlinetaom.uz/api/favorite", data, headers)

                .then(result => {
                        if(result.data){
                            this.setState(prevState => ({
                                ...prevState,
                                favorite: true,
                                favoriteId: result.data
                            }));
                        }
                    },
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                );
        }
    }
    deleteFavorite = () => {
        axios
            .delete(`https://onlinetaom.uz/api/favorite/${this.state.favoriteId}`,
                {
                    headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`}
                })
            .then(result => {
                    this.setState(prevState => ({
                        ...prevState,
                        favorite: false
                    }));
                }
            );
    }
    incrementAmount = () => {
        this.setState(prevState => ({
            ...prevState,
            amount : this.state.amount + 1,
        }));
    }
    decrementAmount = () => {
        if(this.state.amount !== 1){
            this.setState(prevState => ({
                ...prevState,
                amount : this.state.amount - 1,
            }));
        }
    }
    cantPerform = () => {
        this.props.close()
        this.props.toggle()
    }
    sendPost = () =>{
        if(this.props.restId === this.props.check || this.props.check == 0){
        let axiosConfig = {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')} }`,
                "Access-Control-Allow-Origin": "*"
            }
        };
        var postData = {
            amount: this.state.amount ,
            params: JSON.stringify(this.state.params) ,
            size_id: parseInt(this.state.size_id)
        };
        axios
            .post(`https://onlinetaom.uz/api/order/${this.props.information.id}`, postData, axiosConfig)
            .then(result => {
                this.props.desktopUpdate(result.data.amount,result.data.price);
            },)
            .catch((err) => {
                console.log(err);
            })
            this.props.close()
            this.props.notify('success')
        }
        else{
            this.props.notify('info')
        }
    }
    setSize(event) {
        let target = event.target.value;
        let price = event.target.classList * 1;

        this.setState(prevState => ({
            ...prevState,
            size_id : target,
            price : price
        }));
    }

    setParam(e) {
        let params = this.state.params
        let total_price = this.state.paramsPrice;
        let item_price = e.target.name*1;
        let index
        if (e.target.checked) {
          params.push(+e.target.value)
          total_price = total_price + item_price;
        } else {
          index = params.indexOf(+e.target.value)
          params.splice(index, 1)
            total_price = total_price - item_price;
        }
        this.setState(prevState => ({ ...prevState, params: params , paramsPrice : total_price}))
      }
    render() {
        if(this.props.loading){
            return (
                <div className="deskt">
                    <div className="description-choice--desktop text-center pt-200 loader-height">
                        <Loader type="Ball-Triangle" color="#C2F62C" height="100" width="100" />
                    </div>
                </div>
            )
        }
        else{
        if(this.props.information && this.state.price ) {
        return (
            <div className="deskt">
                <div className="description-choice--desktop">
                    <div className="description" style={{background: `url(${this.props.information.image}) center no-repeat`,backgroundSize:`cover`}}>
                        <div className="pl-20">
                            <button className="inner__arrow">
                                <img src="/img/x.svg" onClick={this.props.close} />
                            </button>
                            <button className={`inner__fav ${localStorage.getItem('token') !== null ? '': 'none'}`} onClick={!this.state.favorite ? this.chooseFavorite : this.deleteFavorite }>
                                <img src={!this.state.favorite ? `/img/favorite.svg` : `/img/activefav.svg`}/>
                            </button>
                        </div>
                    </div>
                    <div className="description-info bg-white pb-10">
                        <div className="pl-20 relative">
                            <div className="description-info__title dots">{this.props.information.name_ru}</div>
                            <div className="description-info__name">{this.props.information.restaurant.name}</div>
                            <div className="description-info__desc">
                                {this.props.information.desc_ru}
                            </div>
                            <div className="description-info__price" >{((this.state.price ? this.state.price  : null) + this.state.paramsPrice) * this.state.amount} сум</div>
                            <div className="description-info__add">
                                <span className="input-number-decrement" onClick={() => this.decrementAmount()}>–</span>
                                <div className="input-number input-number-1" >{this.state.amount}</div>
                                <span className="input-number-increment" onClick={() => this.incrementAmount()}>+</span>
                            </div>
                        </div>
                    </div>
                    <div className={`pl-20 ${this.props.information.size ? '' : 'display-none'}`}>
                        <div className={`description-choice__title`}>
                            Выберите вид
                        </div>
                    </div>
                    <div className="description-choice__size" onChange={(event) => this.setSize(event)}>
                        {this.props.information.size.map((item , i) => (
                            <React.Fragment key={item.id}>
                                <input type="radio" defaultChecked={i === 0} name="choice" className={item.price} id={`choice_${item.id}`} defaultValue={item.id}  />
                                <label htmlFor={`choice_${item.id}`}>
                                    <span className="size">{item.name}</span>
                                    <span className="price">{item.price} сум</span>
                                </label>
                            </React.Fragment>
                        ))}
                    </div>
                        <div className={`description-choice__line ${this.props.information.size ? '' : 'display-none'}`}></div>
                        <div className={`pl-20 ${this.props.information.params ? '' : 'display-none'}`}>
                        <div className="description-choice__line"></div>
                        <div className="description-choice__title">Добавки</div>
                        <div className="description-choice__portion">
                            <div className="checkbox" onChange={(event) => this.setParam(event) }>
                                {this.props.information.params.map(item => (
                                    <React.Fragment key={item.id}>
                                        <input id={`check${item.id}`} type="checkbox" defaultValue={item.id} name={item.price} />
                                        <label htmlFor={`check${item.id}`}>
                                            <img src={item.image}/>
                                            <div className="checkbox__title">
                                                {item.name}
                                            </div>
                                            <div className="checkbox__price">
                                                {item.price} сум
                                            </div>
                                        </label>
                                    </React.Fragment>
                                ))}
                            </div>
                        </div>
                    </div>
                    <div className="description-choice__bottom" onClick={localStorage.getItem('token') != null ? this.sendPost : this.cantPerform}>
                        <button><img src="/img/plus.svg" /></button> Положить в корзину
                    </div>
                </div>
            </div>
        );
        }
        else{
            return null;
        }
    }
    }
}
