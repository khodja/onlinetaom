import React, { Component } from 'react';
import axios from "axios";

export default class BasketInfo extends Component {
    render() {
        if(this.props.distance){
        return (
            <div className="container cont-fix rest">
                <div className="col-md-8 col-sm-8 col-xs-7 padding-fix">
                    <img src={this.props.image} className="rest--img" />
                    <div className="rest--info">
                        <h3 className="rest__name">{this.props.name}</h3>
                        <p className="rest__type">{this.props.categories[0].name_ru}</p>
                    </div>
                </div>
                <div className="rest__delivery text-center col-md-2 col-sm-2 col-xs-3 padding-fix">
                    <div className="rest__delivery--wrap">
                        <img src="/img/truck.svg" />
                    </div>
                    <p>{this.props.distance.price} сум</p>
                </div>
                <div className="rest__location text-center col-md-2 col-sm-2 col-xs-2 padding-fix">
                    <img src="/img/longitude.svg" />
                    <p>{this.props.distance.distance}</p>
                </div>
            </div>
        );
        }
        else{
            return null;
        }
    }
}
