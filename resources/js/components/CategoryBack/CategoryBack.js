import React, { Component } from 'react';
import CategoryFilter from '../CategoryFilter';


export default class CategoryBack extends Component {
    render() {
        return (
            <section className="categories desktop">
                <div className="categories__title">
                    Пиццерии
                </div>
                <CategoryFilter />
            </section>
        );
    }
}