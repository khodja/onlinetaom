import React, { Component } from 'react';
import FooterMobile from '../FooterMobile';

export default class Footer extends Component {
    render() {
        return (
<div>
<footer className="footer desktop">
    <div className="container cont-fix">
        <div className="col-md col-sm-4 col-xs-3">
            <h5 className="footer__h5">Online Taom</h5>
            {/*<ul className="footer__ul">*/}
            {/*    <li>О сервисе</li>*/}
            {/*    <li>Акции</li>*/}
            {/*    <li>Ресторанам</li>*/}
            {/*    <li>Стать курьером</li>*/}
            {/*    <li>Служба поддержки</li>*/}
            {/*</ul>*/}
        </div>
        <div className="col-md col-sm-3 col-xs-3">
            {/*<h5 className="footer__h5">Еда</h5>*/}
            {/*<ul className="footer__ul">*/}
            {/*    <li>Национальная</li>*/}
            {/*    <li>Европейская</li>*/}
            {/*    <li>Азиатская</li>*/}
            {/*    <li>Фастфуд</li>*/}
            {/*    <li>Пицца</li>*/}
            {/*</ul>*/}
            {/*<ul className="footer__ul md-block ml-10">*/}
            {/*    <li>Суши</li>*/}
            {/*    <li>Барбекю</li>*/}
            {/*    <li>Сладости</li>*/}
            {/*</ul>*/}
        </div>
        <div className="col-md col-sm-5 col-xs-6">
            <div className="footer__number">
                <div className="footer__h5">Поддержка</div>
                <div className="footer__phone mt-14">998 71 200-7-200</div>
                <div className="footer__mail mt-14">manager@onlinetaom.uz</div>
                {/*<div className="footer__days">Без выходных, 8:00 до 22:00</div>*/}
                {/*<a href="#" className="footer__rights">Пользовательское соглашение</a>*/}
                <ul className="footer__socials md-show md-socials">
                    <li><img src="/img/social1.svg" /></li>
                    {/*<li><img src="/img/social2.svg" /></li>*/}
                    <li><img src="/img/social3.svg" /></li>
                </ul>
            </div>
        </div>
        <div className="col-md md-hidden">
            <ul className="footer__socials">
            <li><img src="/img/social1.svg" /></li>
            {/*<li><img src="/img/social2.svg" /></li>*/}
            <li><img src="/img/social3.svg" /></li>
            </ul>
        </div>
        <div className="col-md col-sm-12 col-xs-12 md-stores">
            <div className="footer__stores">
                <div className="footer__stores--1"><img src="/img/store1.svg" />Загрузить для Android</div>
                <div className="footer__stores--1 footer__stores--padd"><img src="/img/store2.svg" />Загрузить для Apple</div>
            </div>
        </div>
    </div>
    <div className="footer__bottom md-show--block">
        <div className="px-15">
            {/*<div className="col-xs-6 padding-fix">*/}
            {/*    <a href="#" className="footer__rights">Пользовательское соглашение</a>*/}
            {/*</div>*/}
            <div className="col-xs-6 padding-fix text-right">
                <div className="footer-developed">
                    Разработка сайта
                <img src="/img/vektor.svg" width="100" />
                </div>
            </div>
            <div className="clearfix"></div>
        </div>
    </div>
</footer>
<FooterMobile/>
</div>
        );
    }
}
