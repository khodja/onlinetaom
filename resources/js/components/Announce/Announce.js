import React, { Component } from 'react';

export default class Announce extends Component {
    render() {
        return (
        <div>
            <section className="announce desktop">
                <div className="container cont-fix">
                    <div className="announce__back col-md-4 col-sm-4 col-xs-6">
                        <img src="/img/arrowleft.svg" />                
                        Все блюда и рестораны
                    </div>
                    <div className="announce__title col-md-4 col-sm-4 col-xs-6">
                        <img src="/img/event.svg" />
                        Акции
                    </div>
                </div>
            </section>
            <div className="events mobile">
                <div className="container">
                    <button className="events__arrow">
                        <img src="/img/arrowgreen.svg" />                
                    </button>
                    <div className="text-center">
                        Акции
                    </div>
                </div>
            </div>
        </div>
        );
    }
}