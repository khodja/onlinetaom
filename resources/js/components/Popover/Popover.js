import React, { Component } from 'react';
import Login from '../../containers/Login';
import Verify from '../../containers/Verify';
import Profile from '../../containers/Profile';
import Info from '../../containers/Info';

export default class Popover extends Component {
    constructor(props){
        super(props);

        this.state={
            url: 'login',
        }
    }
    componentDidMount(){
        if(localStorage.getItem('token') !== null){
            this.setState({
                url: 'profile'
            });
        }
        else{
            this.setState({
                url: 'login'
            });
        }
    }
    setUrl = (url) => {
        this.setState({
           url: `${url}`
        });
    }
    render() {
        if(this.state.url === 'login' ){
            return (
                <div className={`popup-over ${this.props.popoverOpen ? '' : 'none' }`}>
                    <Login set={this.setUrl} />
                </div>
            );
        }
        if(this.state.url === 'verify' ){
            return (
                <div className={`popup-over ${this.props.popoverOpen ? '' : 'none' }`}>
                    <Verify set={this.setUrl} refresh={this.props.refresh}  />
                </div>
            );
        }
        if(this.state.url === 'profile' ){
            return (
                <div className={`popup-over ${this.props.popoverOpen ? '' : 'none' }`} onClick={this.props.toggle}>
                    <Profile set={this.setUrl} refresh={this.props.refresh} />
                </div>
            );
        }
    }
}
