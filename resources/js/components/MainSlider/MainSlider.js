import React, { Component } from 'react';
import Slider from "react-slick";
import axios from 'axios';
import Loader from 'react-loader-spinner';

export default class MainSlider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nav1: null,
            nav2: null,
            data: null,
            isLoaded: false,
        };
      }

      componentDidMount() {
        axios
            .get(`https://onlinetaom.uz/api/promo`)
            .then(result => {
                this.setState({
                  nav1: this.slider1,
                  nav2: this.slider2,
                  data: result.data,
                  isLoaded: true,
                });
            })
            .catch((err) => {
                console.log(err);
            });
      }

    render() {
        const {data} = this.state;
        const sliderFor = {
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            fade: true
        };
        const sliderNav = {
            infinite: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true
        };
        if (!this.state.isLoaded) {
            return <div className="text-center">
                <Loader type="Ball-Triangle" color="#C2F62C" height="100" width="100"/>
            </div>;
        } else {
            return (
                <div>
                    <section className="slider">
                        <Slider className="slider-for" {...sliderFor}
                                asNavFor={this.state.nav2}
                                ref={slider => (this.slider1 = slider)}
                        >
                                {this.state.data.map(item => (
                                    <div className="slider__inner"
                                         style={{background : `url(${item.image}) center no-repeat`, backgroundSize:'cover'}}
                                         key={item.id}>
                                    </div>
                                ))}
                        </Slider>
                            <Slider className="slider-nav" {...sliderNav}
                            asNavFor={this.state.nav1}
                            ref={slider => (this.slider2 = slider)}>
                                {this.state.data.map(item => (
                                        <div className="slide-nav__inner" key={item.id}>
                                            <div className="container text-center">
                                                <div className="slider__text">
                                                    {item.restaurant.name}
                                                </div>
                                                <div className="slider__title">
                                                    {item.title_uz}
                                                </div>
                                                <div className="slider__date">
                                                    {item.end_time}
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                            </Slider>
                    </section>
                </div>
            );
        }
    }
}
