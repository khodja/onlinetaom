import React, { Component } from 'react';

export default class CategoryFilter extends Component {
   render() {
      return (
          <div className="container">
             <div className="categories__filter">
                <div className="categories__filter-el categories__filter-el-1">
                   <img src="/img/filt1.svg" />
                   <div className="categories-filter">
                      <div className="categories-filter__title">
                         Стоимость блюд
                      </div>
                      <div className="categories-filter__info">
                         до <span className="fitler__price">480 000</span> сум
                      </div>
                      <div className="categories-filter__range range-slider">
                         <input type="text" className="js-range-slider" />
                      </div>
                   </div>
                </div>
                <div className="categories__filter-el categories__filter-el-2">
                   <img src="/img/filt3.svg" />
                   <div className="categories-filter">
                      <div className="categories-filter__title">
                         Время приготовления
                      </div>
                      <div className="categories-filter__info">
                         до <span className="categories-filter__time">40</span> минут
                      </div>
                      <div className="categories-filter__range range-slider">
                         <input type="text" className="js-range-slider-2" />
                      </div>
                   </div>
                </div>
                <div className="categories__filter-el categories__filter-el-3">
                   <img src="/img/filt2.svg" />
                   <div className="categories-filter">
                      <div className="categories-filter__title">
                         Бесплатная доставка
                      </div>
                      <div className="description-choice__type">
                         <input type="radio" name="ship" id="shipping_1" value="small" defaultChecked={true} />
                         <label htmlFor="shipping_1">да</label>
                         <input type="radio" name="ship" id="shipping_2" value="small" />
                         <label htmlFor="shipping_2">не важно</label>
                      </div>
                   </div>
                </div>
                <div className="clearfix"></div>
             </div>
          </div>
      );
   }
}