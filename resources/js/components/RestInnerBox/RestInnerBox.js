import React, { Component } from 'react';

export default class RestInnerBox extends Component {
    constructor(props){
        super(props);

        this.state={
            isLoaded: false,
        };
    } 
       
    render() {
        return (
        <div className="food__el oh" onClick={(e) => this.props.getId(e)} id={this.props.id}>
                <img src={this.props.image} onClick={this.props.onClick} className="food__el-img" />
                <button className="food__el-add" onClick={this.props.onClick}>
                    <img src="/img/add.svg" />
                </button>
                <div className="food__el-main">
                        <span className="food__el-main--title dots-lil" onClick={this.props.onClick}>
                        { this.props.name_ru }
                        </span>
                    <span className="food__el-main--price">
                        { this.props.price } сум
                        </span>
                    <p className="food__el-main--info">
                        <span className="dots-big">
                            { this.props.desc_ru }
                        </span>
                    </p>
                </div>
            </div>
        );
    }
}
