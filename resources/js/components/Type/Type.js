import React, { Component } from 'react';
import Slider from "react-slick";
import Loader from 'react-loader-spinner';
import {Link} from "react-router-dom";

export default class Type extends Component {

    constructor(props){
        super(props);

        this.state={
            error: null,
            isLoaded: false,
            items: []
        }
    }

    componentDidMount() {
        fetch("https://onlinetaom.uz/api/categories")
            .then(response => response.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div className="text-center">
                <Loader type="Ball-Triangle" color="#C2F62C" height="100" width="100" />
            </div>;
        } else {
            const settings = {
                infinite: true,
                slidesToShow: 9,
                slidesToScroll: 1,
                arrows: false,
                dots:false,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 6,
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 4,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                    {
                        breakpoint: 360,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                        }
                    },
                ],
            };
            let path = window.location.pathname;
            return (
                <section className="type">
                    <div className="container cont-mobile text-center">
                        <Slider className="type__slider"  { ...settings }>
                            {items.map(item => (
                                <div className="type__wrapper" key={item.id}>
                                    <Link className={ `${'/'+ item.alias}` == path ? 'active' : ''} to={{ pathname: `/${item.alias}`, state: { alias: item.alias} }}>
                                        <img src={item.image} />
                                        <p className="type__p">{item.name_ru}</p>
                                    </Link>
                                </div>
                            ))}
                        </Slider>
                    </div>
                </section>
            );
        }
    }
}
