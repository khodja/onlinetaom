import React, { Component } from 'react';
import MediaQuery from 'react-responsive';
import Modal from 'react-modal';
import Loader from 'react-loader-spinner';
const customStyles = {
    content : {
        top                   : '0%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : '0',
        transform             : 'translate(-50%, 0%)',
        position              : 'fixed',
        overflowY             : 'auto',
        overflowX             : 'hidden',
        padding               : 0,
        background            : 'transparent',
        border                : 'none',
        borderRadius          : '0',
        width                 : '100%',
    }
};
export default class Check extends Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null,
            isActive : 'fast',
            modalIsOpen:false
        };
    }
    componentDidMount() {
      window.scrollTo(0, 0)
    }
    toggleActive = (event) => {
        let target = event.target.id;
        this.setState(prevState => ({
            ...prevState,
            isActive : target
        }));
    }
    handleEvent = (event) => {
        let target = event.target.id;
        this.props.checkout(target);
        this.setState({
            modalIsOpen: true
        });
    }
    render() {
        if(this.props.basket ){
            return (
            <div>
            <Modal id="modal"
                   ariaHideApp={false}
                   isOpen={this.state.modalIsOpen}
                   onRequestClose={this.closeModal}
                   style={customStyles}
                   shouldCloseOnOverlayClick={false}
                   overlayClassName="modal-bg"
                >
                <div className="preloader">
                    <Loader type="Ball-Triangle" color="#C2F62C" height="100" width="100"/>
                </div>
            </Modal>
                <div className="basket small-show">
                    <div className="restaurant__more">
                        <img src={this.props.basket.restaurant.image} className="vm" width="60" /> { this.props.basket.restaurant.name }
                        <div className="restaurant__more-inner">
                            <div className="restaurant__price"><img src="/img/popularmob.svg" height="10" /> {this.props.distance ? this.props.distance.price : 0} сум</div>
                            <p className="restaurant__long pt-5" style={{display: 'block'}}>{this.props.distance ? this.props.distance.distance : 0}</p>
                        </div>
                    </div>
                </div>
                <div className="restaurant small-hidden" style={{paddingBottom: '0px'}}>
                    <div className="container cont-fix">
                    <div className="restaurant-desktop__ins" style={{marginBottom: '10px'}}>
                        <div className="history-desktop">
                            <div className="main">
                                <div className="vm">
                                    <img src={this.props.basket ? this.props.basket.restaurant.image : "/img/brand.png"} width="70" className="vm br50" />
                                    <h5 className="restaurant__title pl-10">{this.props.basket ? this.props.basket.restaurant.name : ``}</h5>
                                    <table className="history__info-desk">
                                    <tbody>
                                    {this.props.basket ? this.props.basket.order_food.map(item => (
                                            <tr key={item.id}>
                                                <td>
                                                    {item.food.name_ru}
                                                </td>
                                                <td>
                                                    {item.food.price} сум
                                                </td>
                                                <td>
                                                    {item.amount} шт
                                                </td>
                                            </tr>
                                        )) : ''}
                                        </tbody>
                                    </table>
                                <div className="checkout__price">{this.props.basket ? this.props.price + ' сум' : ``}</div>
                                </div>
                            </div>
                        </div>
                    <div className="checkout" style={{borderTop: '1px #EDEDED solid',lineHeight:'22px'}}>
                        <div className="col-md-4 col-sm-4 text-left">
                            <p style={{fontSize: '20px', fontWeight:'500', paddingLeft: '20px'}}>Проверьте ваши данные</p>
                        </div>
                        <div className="col-md-4 col-sm-4 text-left">
                            <div className="checkout__info mr-80">
                            <span style={{fontSize: '14px', fontWeight:'600'}}>Получатель</span>
                            <p style={{paddingTop:'10px'}}>Пользователь #{this.props.basket.user.id}</p>
                            </div>
                            <div className="checkout__info">
                            <span style={{fontSize: '14px', fontWeight:'600'}}>Адрес доставки</span>
                            <p style={{paddingTop:'10px', maxWidth:'180px'}}>{this.props.address ?  this.props.address : ''}</p>
                            </div>
                            <div className="pb-30">
                            <span style={{fontSize: '14px', fontWeight:'600'}}>Телефон</span>
                            <p style={{paddingTop:'10px'}}>+998 {this.props.basket.user.phone}</p>
                            </div>
                        </div>
                        <div className="col-md-4 col-sm-4 text-center">
                            <button className="basket__btn" onClick={(auth) => this.props.forwardTo('basket')}>Изменить</button>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                    </div>
                </div>
                </div>
                <div className="restaurant small-show">
                {this.props.basket ? this.props.basket.order_food.map(item => (
                    <div className="restaurant__ins-2" key={item.id}>
                        <div className="restaurant__text">
                            <img src={item.food.image} width="50" style={{ height: '50px' , objectFit: 'cover' }} className="vm br50" />
                            <span className="dots-lil vm" style={{paddingLeft: '15px'}}>{item.food.name_ru}
                                <span style={{display:'block',fontWeight:'300'}}>{item.food.price} сум</span>
                            </span>
                            <span className="absolute" style={{right: '10px',top:'50%'}}>{item.amount}шт</span>
                        </div>
                    </div>
                    )) : ''}
                    <div className="restaurant__ins-2">
                        <div className="col-sm-12 col-xs-12 text-center">
                            <p style={{fontSize: '12px', fontWeight:'500'}} className="mt-10">Проверьте ваши данные</p>
                        </div>
                        <div className="col-sm-12 col-xs-12 text-center">
                            <div className="checkout__info margin-check">
                            <span style={{fontSize: '12px', fontWeight:'600'}}>Получатель</span>
                            <p style={{fontSize: '10px',paddingTop:'5px'}}>Пользователь #{this.props.basket.user.id}</p>
                            </div>
                            <div className="checkout__info margin-check">
                            <span style={{fontSize: '12px', fontWeight:'600'}}>Адрес доставки</span>
                            <p style={{fontSize: '10px',paddingTop:'5px'}}>{this.props.address ?  this.props.address : ''}</p>
                            </div>
                            <div className="checkout__info margin-check">
                            <span style={{fontSize: '12px', fontWeight:'600'}}>Телефон</span>
                            <p style={{fontSize: '10px',paddingTop:'5px'}}>+998 {this.props.basket.user.phone}</p>
                            </div>
                        </div>
                        <div className="col-sm-12 col-xs-12 text-center">
                            <button className="basket__btn" onClick={(auth) => this.props.forwardTo('basket')}>Изменить</button>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <div className="method">
                    <div className="container cont-fix d-flex">
                    {JSON.parse(this.props.basket.restaurant.payments).length === 2 ?
                    <div className="method__element" style={{marginRight :'5px'}} onClick={this.handleEvent} id="1">
                        <div className="method__element-text" id="1">
                            Оплатить через Payme
                        </div>
                        <img src="/img/payme.svg" height="100" id="1" />
                    </div>
                    : null}
                    <div className="method__element" onClick={this.handleEvent} id="0">
                        <div className="method__element-text" id="0">
                            Оплатить наличными
                        </div>
                        <img src="/img/wallet.png" height="100" id="0" />
                    </div>
                    </div>
                </div>

            </div>
        );
    }
    else{
            return null ;
        }
    }

}
