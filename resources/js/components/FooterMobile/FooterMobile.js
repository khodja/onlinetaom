import React, { Component } from 'react';
import {
    Accordion,
    AccordionItem,
    AccordionItemTitle,
    AccordionItemBody,
} from 'react-accessible-accordion';

// import 'react-accessible-accordion/dist/minimal-example.css';
import 'react-accessible-accordion/dist/fancy-example.css';

export default class FooterMobile extends Component {
    render() {
        return (

<footer className="footer-mobile mobile">
    <div className="pl-20">
            <div className="footer-mobile__number">
                <div className="footer__h5">Поддержка</div>
                <div className="footer__phone mt-14">998 71 200-7-200</div>
                <div className="footer__mail mt-14">manager@onlinetaom.uz</div>
                {/*<div className="footer__days">Без выходных, 8:00 до 22:00</div>*/}
            </div>
            <ul className="footer-mobile__socials">
            <li><img src="/img/social1.svg" /></li>
            <li><img src="/img/social3.svg" /></li>
            </ul>
            <div className="footer-mobile__stores">
                <div className="footer__stores--1"><img src="/img/store1.svg" />Загрузить для Android</div>
                <div className="footer__stores--1"><img src="/img/store2.svg" />Загрузить для Apple</div>
            </div>
        {/*<a href="#" className="footer-mobile__rights">Пользовательское соглашение</a>*/}
        <div className="footer-mobile__dev">
            <span className="footer-mobile__dev--txt">Разработка сайта</span><img src="/img/vektor.svg" />
        </div>
        <div className="clearfix"></div>
    </div>
</footer>
        );
    }
}
