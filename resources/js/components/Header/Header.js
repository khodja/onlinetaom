import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { slide as Menu } from 'react-burger-menu'
import MediaQuery from 'react-responsive';
import Filter from '../../containers/Filter';
import Finder from '../Finder';
import Popover from '../Popover';

var styles = {
    bmBurgerButton: {
      position: 'fixed',
      width: '30px',
      height: '20px',
      left: '15px',
      top: '20px'
    },
    bmBurgerBars: {
      background: '#373a47',
      height : '1px'
    },
    bmCrossButton: {
        display: 'none'
    },
    bmBurgerBarsHover: {
      background: '#a90000'
    },
    bmMenuWrap: {
      position: 'fixed',
      height: '100%'
    },
    bmMenu: {
      background: '#fff',
    },
    bmMorphShape: {
      fill: '#373a47'
    },
    bmItem: {
      display: 'inline-block'
    },
    bmOverlay: {
      background: 'rgba(255, 255, 255, 1)'
    }
  }
export default class Header extends Component {

    constructor(props){
        super(props);
        this.state ={
            menuOpen : false,
            finder : false,
            popoverOpen : false,
        };
    }

    componentDidMount() {
        this.props.onRef(this)
        window.scrollTo(0, 0)
    }
    componentWillUnmount() {
        this.props.onRef(undefined)
    }
    toggle = () => {
    this.setState(prevState => ({
        ...prevState,
        popoverOpen: !this.state.popoverOpen
    }));
    }
    toggleMenu = () => {
        this.setState(prevState=> ({
            ...prevState,
            menuOpen : !this.state.menuOpen
        }));
    }
    isMenuOpen = (state) => {
        if (state.isOpen === this.state.menuOpen) return
        this.setState({ menuOpen: state.isOpen });
    };


    toggleFinder = () => {
        this.setState(prevState=> ({
            ...prevState,
            finder : !this.state.finder
        })
    )}

    render() {
        if(this.props.cart.totalItems !== null){
        return (
            <div>
                <MediaQuery maxWidth={480}>
                <Menu burgerButtonClassName={"hamburger"} onStateChange={ this.isMenuOpen } burgerBarClassName={"hamburger-item"} isOpen={this.state.menuOpen} width={ '100%' } styles={ styles } >
                    <Filter toggleMenu={this.toggleMenu} runSearch={(data,keyword) => this.props.runSearch(data,keyword)} />
                </Menu>
                </MediaQuery>
                <MediaQuery minWidth={480}>
                    <Finder searchData={this.props.searchData} runSearch={(data,keyword) => this.props.runSearch(data,keyword)} finder={this.state.finder} toggle={this.toggleFinder} />
                </MediaQuery>
                <nav className="nav desktop pb-20">
                    <div className="container nav__ins cont-fix">
                        <div className="col-md-5 col-sm-5 col-xs-5 mt-25">
                            <span className="nav__number md-hide">998 71 200-7-200</span>
                            <div className="md-show nav__eve"><img src="/img/favorite-nav.svg" height="26" /></div>
                            <div className="md-show nav__eve"><img src="/img/phone-nav.svg" height="26" /></div>
                            <div className="nav__localization md-show md-local" style={{display: 'none'}}>
                                <a href="#" className="nav__localization-uz nav__localization-el active">UZ</a>
                                <a href="#" className="nav__localization-ru nav__localization-el">RU</a>
                            </div>
                            <span className="nav__supp md-hide">поддержка</span>
                        </div>
                        <div className="col-md-2 col-sm-2 col-xs-2 mt-25">
                            <div className="nav__logo">
                                                <Link to="/">
                                    <img src="/img/logo.svg" />
                                </Link>
                            </div>
                        </div>
                        <div className="col-md-5 col-sm-5 col-xs-5 mt-25 text-right vm-fix">
                            <div className="nav__localization" style={{display: 'none'}}>
                                <a href="#" className="nav__localization-uz nav__localization-el active">UZ</a>
                                <a href="#" className="nav__localization-ru nav__localization-el">RU</a>
                            </div>
                            <div className="nav__account" id="nav-push">
                            <button onClick={this.toggle}>
                                <img src="/img/account.svg" />
                            </button>
                            <div className={`modal deskt ${this.state.popoverOpen ? '' : 'none' }`}>
                                <div className={`modal-popup ${this.state.popoverOpen ? '' : 'none' }`} onClick={this.toggle}></div>
                            </div>
                            <Popover refresh={this.props.refresh} popoverOpen={this.state.popoverOpen} toggle={this.toggle} />
                            </div>
                            <div className="nav__search" onClick={this.toggleFinder}>
                                <button><img src="/img/search.svg" /></button></div>
                                <div className="nav__cart">
                                    <div className="notify_container">
                                        <span className="notify_bubble">{this.props.cart.totalItems}</span>
                                        <Link to="/basket">
                                            <img src="/img/cart.svg" className="cart" />
                                        </Link>
                                    </div>
                                    <Link to="/basket" className="nav__cart--price">
                                        {this.props.cart.totalPrice} сум
                                    </Link>
                                </div>
                        </div>
                    </div>
                </nav>
                <div className="mobnav mobile">
                    <div className="col-xs-3">
                    </div>
                    <div className="col-xs-6 text-center">
                        <Link to="/">
                            <img src="/img/logo.svg" className="mobnav__logo" onClick={this.state.menuOpen ? this.toggleMenu : this.doNothing} />
                        </Link>
                    </div>
                    <div className="col-xs-3 text-right">
                        <div className="notify_container top5">
                            <span className="notify_bubble">{this.props.cart.totalItems}</span>
                            <Link to="/basket">
                                <img src="/img/cart.svg" className="cart" />
                            </Link>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
        }
        else{
            return (
            <div>
                <MediaQuery maxWidth={480}>
                <Menu burgerButtonClassName={"hamburger"} onStateChange={ this.isMenuOpen } burgerBarClassName={"hamburger-item"} isOpen={this.state.menuOpen} width={ '100%' } styles={ styles } >
                    <Filter toggleMenu={this.toggleMenu} />
                </Menu>
                </MediaQuery>
                <MediaQuery minWidth={480}>
                    <Finder searchData={this.props.searchData} runSearch={(data , keyword) => this.props.runSearch(data , keyword)} finder={this.state.finder} toggle={this.toggleFinder} />
                </MediaQuery>
                <nav className="nav desktop pb-20">
                    <div className="container nav__ins cont-fix">
                        <div className="col-md-5 col-sm-5 col-xs-5 mt-25">
                            <span className="nav__number md-hide">998 71 200-7-200</span>
                            <div className="md-show nav__eve"><img src="/img/favorite-nav.svg" height="26" /></div>
                            <div className="md-show nav__eve"><img src="/img/phone-nav.svg" height="26" /></div>
                            <div className="nav__localization md-show md-local" style={{display: 'none'}}>
                                <a href="#" className="nav__localization-uz nav__localization-el active">UZ</a>
                                <a href="#" className="nav__localization-ru nav__localization-el">RU</a>
                            </div>
                            <span className="nav__supp md-hide">поддержка</span>
                        </div>
                        <div className="col-md-2 col-sm-2 col-xs-2 mt-25">
                            <div className="nav__logo">
                                <Link to="/">
                                    <img src="/img/logo.svg" />
                                </Link>
                            </div>
                        </div>
                        <div className="col-md-5 col-sm-5 col-xs-5 mt-25 text-right vm-fix">
                            <div className="nav__localization" style={{display: 'none'}}>
                                <a href="#" className="nav__localization-uz nav__localization-el active">UZ</a>
                                <a href="#" className="nav__localization-ru nav__localization-el">RU</a>
                            </div>
                            <div className="nav__account" id="nav-push">
                                <button onClick={this.toggle}>
                                    <img src="/img/account.svg" />
                                </button>
                                <div className={`modal deskt ${this.state.popoverOpen ? '' : 'none' }`}>
                                    <div className={`modal-popup ${this.state.popoverOpen ? '' : 'none' }`} onClick={this.toggle}></div>
                                </div>
                            <Popover refresh={this.props.refresh} popoverOpen={this.state.popoverOpen} toggle={this.toggle} />
                            </div>
                            <div className="nav__search" onClick={this.toggleFinder}>
                                <img src="/img/search.svg" />
                            </div>
                            <div className="nav__cart">
                                <div className="notify_container">
                                    <button onClick={this.toggle}>
                                        <img src="/img/cart.svg" className="cart" />
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <div className="mobnav mobile">
                    <div className="col-xs-3">
                    </div>
                    <div className="col-xs-6 text-center">
                        <Link to="/">
                            <img src="/img/logo.svg" className="mobnav__logo" />
                        </Link>
                    </div>
                    <div className="col-xs-3 text-right">
                        <div className="relative top5">
                            <Link to="/basket">
                                <img src="/img/cart.svg" />
                            </Link>
                        </div>
                    </div>

                    <div className="clearfix"></div>
                </div>
            </div>
            );
        }
    }
}
