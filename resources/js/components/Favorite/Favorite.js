import React, { Component } from 'react';
import FavoriteTabs from '../FavoriteTabs';
import FavoriteRest from '../FavoriteRest';
import FavoriteFood from '../FavoriteFood';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import {Link} from "react-router-dom";

export default class Favorite extends Component {
    constructor(props){
        super(props);

        this.state={
            food : null,
            restaurant : null,
            active: 'restaurant',
            redirect: false
        }
    }
    componentDidMount(){
          window.scrollTo(0, 0)

        if(localStorage.getItem('token') === null){
            this.setState({
                redirect : true
            });
        }
        else{
            this.getData();
        }
    }
    setActive = (data) => {
        let target = data.target.id;
        this.setState(prevState => ({
            ...prevState,
            active  : target
        }));
    }
    getData = () => {
        axios
            .get(`https://onlinetaom.uz/api/favorite`,
                {headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`} })
            .then(result => {
                let food = result.data.filter(el => {return el.type == 'food'});
                let rest = result.data.filter(el => {return el.type == 'restaurant'});
                this.setState( prevState =>({
                        ...prevState,
                        restaurant: rest ,
                        food: food ,
                    }));
                }
            )
            .catch((err) => {
                console.log(err);
            });
    }
    delete = (event) =>{
        let deleteId = event.target.id;
        axios
            .delete(`https://onlinetaom.uz/api/favorite/${deleteId}`,
                {
                    headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`}
                })
            .then(() => {
                   this.getData();
                }
            );

    }
    render() {
        if(this.state.redirect){
            return (<Redirect to={'/'} />)
        }
        if(this.state.restaurant || this.state.food){
        return (
            <div>
                <div className="favorite-food">
                    <img src="/img/favstart.svg" />
                    Избранное
                </div>
                <FavoriteTabs setActive={this.setActive} active={this.state.active} />
                <div className="restaurant">
                    <div className="container cont-fix">
                        <div className={this.state.active == 'restaurant' ? '' : 'display-none'}>
                            {this.state.restaurant ? this.state.restaurant.map(item => (
                                <Link to={{ pathname: `/restaurant/${item.ids.alias}`}} key={item.id}>
                                    <FavoriteRest deleteId={item.id} delete={this.delete}  {... item.ids} />
                                </Link>
                            )) : null}
                        </div>
                        <div className={`${this.state.active == 'food' ? '' : 'display-none'} flexbox`}>
                            {this.state.food ? this.state.food.map(item => (
                                <Link to={{ pathname: `/restaurant/${item.ids.restaurant.alias}`}} key={item.id}>
                                    <FavoriteFood deleteId={item.id} delete={this.delete} { ... item.ids } />
                                </Link>
                            )) : null}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
        else{
            return null;
        }
    }
}
