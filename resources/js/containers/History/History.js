import React, { Component } from 'react';
import Order from '../../components/Order'
import axios from 'axios';

export default class History extends Component {
    constructor(props){
        super(props);

        this.state={
            order : null,
            redirect: false
        }
    }

    componentDidMount(){
        if(localStorage.getItem('token') === null){
            this.setState({
                redirect : true
            });
        }
        else{
            this.getData();
        }
    }
    getData = () => {
        axios
            .get(`https://onlinetaom.uz/api/history`,
                {headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`} })
            .then(result => {
                this.setState( prevState =>({
                        ...prevState,
                        order: result.data ,
                    }));
                }
            )
            .catch((err) => {
                console.log(err);
            });
    }

    render() {
        if(this.state.redirect){
            return (<Redirect to={'/'} />)
        }
        return (
        <div>
        <section className="basket desktop">
        <div className="basket__title">
            <img src="/img/history.svg" alt="cart icon" />
            <h2>
                Мои заказы
            </h2>
        </div>
        </section>
        <div className="events mobile">
            <div className="container">
                <div className="text-center">
                    Мои заказы
                </div>
            </div>
        </div>
        {this.state.order ? this.state.order.map(item => (
            <Order key={item.id} deleteId={item.id} {... item} />
        )) : 'У вас нет Заказов'}
        </div>
        );
    }
}
