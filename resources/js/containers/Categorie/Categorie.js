import React, { Component } from 'react';
import CategoryBack from '../../components/CategoryBack'
import CategoryItem from '../../components/CategoryItem'
import Type from "../../components/Type";

export default class Categorie extends Component {
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
        <div>
            <Type  />
            {/*<CategoryBack />*/}
            <CategoryItem longitude={this.props.cart.longitude} ready={this.props.cart.ready} latitude={this.props.cart.latitude} />
        </div>
        );
    }
}
