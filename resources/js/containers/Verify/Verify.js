import React, { Component } from 'react';
import ReactCodeInput from 'react-code-input';
import MediaQuery from 'react-responsive';
import { Redirect } from 'react-router-dom';

const props = {
  inputStyle: {
    MozAppearance: 'textfield',
  },
  inputStyleInvalid: {
    MozAppearance: 'textfield',
  }
}
export default class Verify extends Component {
    constructor(props){
        super(props);

        this.state={
            phone: localStorage.getItem('phone'),
            password: localStorage.getItem('phone'),
            redirect : false,
            user : null,
        }
    }
    componentDidMount() {
        if(localStorage.getItem('token') !== null){
            this.setState({
                redirect : true
            })
        }
    }

    login = () =>{
        fetch("https://onlinetaom.uz/api/login",{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                phone: this.state.phone ,
                password: this.state.password
            })
        })
        .then(response => response.json())
        .then(
            (result) => {
                this.setState(prevState => ({
                ...prevState,
                isLoggedIn: true,
                user : result
            }),
            this.check
            );
        })
        .catch(
            (error) => {
                console.log(error)
        });
    }
    desktopLogin = () => {
        fetch("https://onlinetaom.uz/api/login",{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                phone: this.state.phone,
                password: this.state.password
            })
        })
        .then(response => response.json())
        .then(
            (result) => {
                this.setState(prevState => ({
                ...prevState,
                isLoggedIn: true,
                user : result
            }),
            this.checkDesktop
            );
        })
        .catch(
            (error) => {
                console.log(error)
        });
    }
    check = () => {
        if(this.state.user !== null){
            localStorage.setItem('token', this.state.user.accessToken)
            this.setState({
                redirect: true,
            });
            this.props.refresh();
        }
        else{
            console.log('Error')
        }
    }
    checkDesktop = () => {
        if(this.state.user !== null){
            localStorage.setItem('token', this.state.user.accessToken)
            this.props.refresh();
        }
        else{
            console.log('Error')
        }
    }
    eventHandler = (e) => {
        this.setState(prevState => ({
            ... prevState,
            password: e
        }),
        );
    }

    render() {
        if(this.state.redirect){
            return (<Redirect to={'/'} />)
        }
        return (
        <div>
        <div className="popup-white">
            <div className="pl-20">
                <div className="text-left">
                    Вход
                </div>
                <div className="profile__account">
                    <img src="/img/accountgreen.svg" />    
                </div>                   
            </div>
        </div>        
        <div className="verify">
            <div className="text-center">
                <img src="/img/phone.svg" className="verify__phone" />
                <div className="verify__sms">
                    Код из смс                    
                </div>
            </div>
        </div>
        <div id="wrapper">
            <div id="dialog">
                <div id="form">
                    <ReactCodeInput type='number' name="input" pattern="[0-9]{1}" fields={4} { ... props  } onChange={this.eventHandler} />
                </div>
            </div>
            <MediaQuery maxWidth={480}>
                <button className="verify__btn" onClick={this.login}>Войти</button>
            </MediaQuery>
            <MediaQuery minWidth={480}>
                <button className="verify__btn" onClick={this.desktopLogin}>Войти</button>
            </MediaQuery>

        </div>
        </div>
        );
    }
}
