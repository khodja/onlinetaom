import React, { Component } from 'react';
import FavoriteTabs from '../../components/FavoriteTabs'
import FavoriteRest from '../../components/FavoriteRest';
import FavoriteFood from '../../components/FavoriteFood';
import { Link } from 'react-router-dom';

export default class Find extends Component {

    constructor(props){
        super(props);

        this.state={
            food : null,
            restaurant : null,
            keyword : null,
            active: 'restaurant',
        }
    }
componentWillReceiveProps(nextProps, nextContext) {
    if(nextProps.data !== null){
        this.setState({
            restaurant: nextProps.data[0],
            food: nextProps.data[1],
            keyword: nextProps.keyword
        })
    }
}

    setActive = (event) => {
        let target = event.target.id;
        this.setState(prevState => ({
            ...prevState,
            active  : target
        }));
    }
    render() {
        if(this.props.data !== null) {
            return (
                <div>
                    <div className="search-result" style={{padding:'10px 0'}}>
                        <div className="container relative">
                            <label htmlFor="search-res">
                                <img src="/img/searchgreen.svg"/>
                            </label>
                            <input type="text" id="search-res" defaultValue={this.state.keyword ? this.state.keyword : ''} disabled={true}/>
                        </div>
                    </div>
                    <FavoriteTabs setActive={this.setActive} active={this.state.active}/>
                    <div className="restaurant">
                        <div className="container cont-fix">
                            <div className={this.state.active == 'restaurant' ? '' : 'display-none'}>
                            {this.state.restaurant ? this.state.restaurant.map(item => (
                                <Link to={{ pathname: `/restaurant/${item.alias}`}} key={item.id}>
                                    <FavoriteRest key={item.id} deleteId={item.id} delete={this.delete}  {... item} />
                                </Link>
                            )) : ''}
                            </div>
                            <div className={`${this.state.active == 'food' ? '' : 'display-none'} flexbox`}>
                            {this.state.food ?this.state.food.map(item => (
                                <Link to={{ pathname: `/restaurant/${item.restaurant.alias}`}} key={item.id}>
                                    <FavoriteFood key={item.id} deleteId={item.id} delete={this.delete} { ... item } />
                                </Link>
                            )) : ''}
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        else{
            return (<div className='search-result text-center' style={{padding:'10px 0'}} >Ничего не найдено</div>)
            // return (<Redirect to={'/'} />)
        }
    }
}
