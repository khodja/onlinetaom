import React, { Component } from 'react';
import Search from '../../components/Search'
import MainSlider from '../../components/MainSlider'
import Type from '../../components/Type'
import Popular from '../../components/Popular'
import Download from '../../components/Download'
import Text from '../../components/Text'

export default class FavFood extends Component {
    render() {
        return (
        <div>
            <Search />
            <MainSlider />  
            <Type />
            <Popular />
            <Download />
            <Text />                          
        </div>
        );
    }
}