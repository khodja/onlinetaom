import React, { Component } from 'react';
import axios from 'axios';
import { Link , Redirect } from "react-router-dom";
import MediaQuery from 'react-responsive';

export default class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false,
            toHome: false,
        };
    }

    componentDidMount() {
          window.scrollTo(0, 0)
        if(localStorage.getItem('token') === null){
            this.setState({
                redirect : true
            })
        }
        axios
        .get(`https://onlinetaom.uz/api/user`,
            {
                headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`}
            })
        .then(result => {
            this.setState({
                email : result.data.email,
                name : result.data.name,
                phone: result.data.phone,
            })
            },
            (error) => {
            console.warn(error);
            }
        );
    }
    logout = () => {
        axios
            .get(`https://onlinetaom.uz/api/logout`,
                {
                    headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`}
                })
            .then(result => {
                if(result.data == 'You have been succesfully logged out!'){
                    // this.setState({
                    //     toHome: true
                    // });
                    localStorage.removeItem('token');
                    this.props.refresh()
                }
                },
                (error) => {

                }
            );
    }

    render() {
        if(this.state.redirect){
            return (<Redirect to={`/auth`} />)
        }
        if(this.state.toHome){
            return (<Redirect to={`/`} />)
        }
        return (
        <div>
            <div className="profile">
                <div className="pl-20">
                    <div className="text-left">
                        Профиль
                    </div>
                    <div className="profile__account">
                        <img src="/img/account.svg" />
                    </div>
                    <img src="/img/profile.svg" className="profile-photo" />
                </div>
            </div>
            <div className="profile-padding text-left">
                <div className="profile__name">{this.state.name ? this.state.name : ''}</div>
                <div className="profile__phone">+998 {this.state.phone ? this.state.phone : ''}</div>
                <MediaQuery minWidth={480}>
                <Link to="/favorites" style={{display:'block'}} className="profile__btn">
                    Избранное
                </Link>
                </MediaQuery>
                <Link to="/history" style={{display:'block'}} className="profile__btn">
                    Мои заказы
                </Link>
                <Link to="/info" style={{display:'block'}} className="profile__btn">
                    Редактировать данные
                </Link>
                <div className="profile__btn" style={{cursor:'pointer'}} onClick={this.logout}>
                    <span className="pointer">Выйти</span>
                </div>
            </div>
        </div>
        );
    }
}
