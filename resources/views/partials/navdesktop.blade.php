<nav class="nav desktop pb-20">
    <div class="container nav__ins cont-fix">
        <div class="col-md-5 col-sm-5 col-xs-5 mt-25">
            <span class="nav__number md-hide">998 71 700-00-00</span>
            <div class="md-show nav__eve"><img src="{{ asset('img/favorite-nav.svg') }}" height="26"></div>
            <div class="md-show nav__eve"><img src="{{ asset('img/phone-nav.svg') }}" height="26"></div>
            <div class="nav__localization md-show md-local">
              <a href="#" class="nav__localization-uz nav__localization-el active">UZ</a>
              <a href="#" class="nav__localization-ru nav__localization-el">RU</a>
            </div>            
            <span class="nav__supp md-hide">поддержка</span>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2 mt-25">
            <div class="nav__logo">
                <img src="{{ asset('img/logo.svg') }}">
            </div>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-5 mt-25 text-right vm-fix">
            <div class="nav__localization md-hide">
              <a href="#" class="nav__localization-uz nav__localization-el active">UZ</a>
              <a href="#" class="nav__localization-ru nav__localization-el">RU</a>
            </div>
            <div class="nav__account" id="nav-push"><img src="{{ asset('img/account.svg') }}"></div>
            <div class="nav__search"><img src="{{ asset('img/search.svg') }}"></div>
            <div class="nav__cart">
                <div class="notify_container">
                    <span class="notify_bubble">
                        3
                    </span>
                    <img src="{{ asset('img/cart.svg') }}" class="cart">
                </div>
                <div class="nav__cart--price">
                    58 500 сум
                </div>
            </div>
        </div>
    </div>
</nav>