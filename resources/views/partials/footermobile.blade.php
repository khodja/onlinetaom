<footer class="footer-mobile mobile">
    <ul id="accordion" class="accordion">
        <li>
            <div class="link">Online Taom <img src="{{ asset('img/dropdown.svg') }}" class="dropdown-svg"></div>
            <ul class="submenu">
                <li><a href="#">О сервисе</a></li>
                <li><a href="#">Акции</a></li>
                <li><a href="#">Ресторанам</a></li>
                <li><a href="#">Стать курьером</a></li>
                <li><a href="#">Служба поддержки</a></li>
            </ul>
        </li>
        <li>
            <div class="link">Еда <img src="{{ asset('img/dropdown.svg') }}" class="dropdown-svg"></div>
            <ul class="submenu">
                <li><a href="#">Национальная</a></li>
                <li><a href="#">Европейская</a></li>
                <li><a href="#">Азиатская</a></li>
                <li><a href="#">Фастфуд</a></li>
                <li><a href="#">Пицца</a></li>
                <li><a href="#">Суши</a></li>
                <li><a href="#">Барбекю</a></li>
                <li><a href="#">Сладости</a></li>                
            </ul>
        </li>
    </ul>                                  
    <div class="pl-20">
            <ul class="footer__ul">

            </ul>
            <ul class="footer__ul">

            </ul>
            <ul class="footer__ul">

            </ul>
            <div class="footer-mobile__number">
                <div class="footer__h5">Поддержка</div>
                <div class="footer__phone mt-14">998 71 700-00-00</div>
                <div class="footer__mail mt-14">support@onlinetaom.uz</div>
                <div class="footer__days">Без выходных, 8:00 до 22:00</div>
            </div>
            <ul class="footer-mobile__socials">
            <li><img src="{{ asset('img/social1.svg') }}"></li>
            <li><img src="{{ asset('img/social2.svg') }}"></li>
            <li><img src="{{ asset('img/social3.svg') }}"></li>
            </ul>
            <div class="footer-mobile__stores">
                <div class="footer__stores--1"><img src="{{ asset('img/store1.svg') }}">Загрузить для Android</div>
                <div class="footer__stores--1"><img src="{{ asset('img/store2.svg') }}">Загрузить для Apple</div>
            </div>
        <a href="#" class="footer-mobile__rights">Пользовательское соглашение</a>
        <div class="footer-mobile__dev">
            <span class="footer-mobile__dev--txt">Разработка сайта</span><img src="{{ asset('img/vektor.svg') }}">
        </div>
        <div class="clearfix"></div>
    </div>
</footer>

@section('script')
<script>
    $(function() {
        var Accordion = function(el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;
    
            // Variables privadas
            var links = this.el.find('.link');
            // Evento
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
        }
    
        Accordion.prototype.dropdown = function(e) {
            var $el = e.data.el;
                $this = $(this),
                $next = $this.next();
    
            $next.slideToggle();
            $this.parent().toggleClass('open');
    
            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
            };
        }	
    
        var accordion = new Accordion($('#accordion'), false);
    });
</script>
@endsection