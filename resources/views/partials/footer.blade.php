<footer class="footer desktop">
    <div class="container cont-fix">
        <div class="col-md col-sm-4 col-xs-3">
            <h5 class="footer__h5">Online Taom</h5>
            <ul class="footer__ul">
                <li>О сервисе</li>
                <li>Акции</li>
                <li>Ресторанам</li>
                <li>Стать курьером</li>
                <li>Служба поддержки</li>
            </ul>
        </div>
        <div class="col-md col-sm-3 col-xs-3">
            <h5 class="footer__h5">Еда</h5>
            <ul class="footer__ul">
                <li>Национальная</li>
                <li>Европейская</li>
                <li>Азиатская</li>
                <li>Фастфуд</li>
                <li>Пицца</li>
            </ul>
            <ul class="footer__ul md-block ml-10">
                <li>Суши</li>
                <li>Барбекю</li>
                <li>Сладости</li>
            </ul>
        </div>
        <div class="col-md col-sm-5 col-xs-6">
            <div class="footer__number">
                <div class="footer__h5">Поддержка</div>
                <div class="footer__phone mt-14">998 71 700-00-00</div>
                <div class="footer__mail mt-14">support@onlinetaom.uz</div>
                <div class="footer__days">Без выходных, 8:00 до 22:00</div>
                <a href="#" class="footer__rights">Пользовательское соглашение</a>                
                <ul class="footer__socials md-show md-socials">
                    <li><img src="{{ asset('img/social1.svg') }}"></li>
                    <li><img src="{{ asset('img/social2.svg') }}"></li>
                    <li><img src="{{ asset('img/social3.svg') }}"></li>
                </ul>                
            </div>
        </div>
        <div class="col-md md-hidden">
            <ul class="footer__socials">
            <li><img src="{{ asset('img/social1.svg') }}"></li>
            <li><img src="{{ asset('img/social2.svg') }}"></li>
            <li><img src="{{ asset('img/social3.svg') }}"></li>
            </ul>
        </div>
        <div class="col-md col-sm-12 col-xs-12 md-stores">
            <div class="footer__stores">
                <div class="footer__stores--1"><img src="{{ asset('img/store1.svg') }}">Загрузить для Android</div>
                <div class="footer__stores--1 footer__stores--padd"><img src="{{ asset('img/store2.svg') }}">Загрузить для Apple</div>
            </div>
        </div>
    </div>
    <div class="footer__bottom md-show--block">
        <div class="px-15">
            <div class="col-xs-6 padding-fix">
                <a href="#" class="footer__rights">Пользовательское соглашение</a>
            </div>
            <div class="col-xs-6 padding-fix text-right">
                <div class="footer-developed">
                    Разработка сайта
                <img src="{{ asset('img/vektor.svg') }}" width="100">
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>    
</footer>
