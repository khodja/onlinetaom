<section class="type desktop">
    <div class="container text-center">
        <div class="type__slider">
            <div class="type__wrapper">
                <img src="{{ asset('/img/prod1.png') }}" width="150">
                <p class="type__p">Национальная</p>
            </div>
            <div class="type__wrapper">
                <img src="{{ asset('/img/prod2.png') }}" width="150">
                <p class="type__p">Европейская</p>
            </div>
            <div class="type__wrapper">
                <img src="{{ asset('/img/prod3.png') }}" width="150">
                <p class="type__p">Азиатская</p>
            </div>
            <div class="type__wrapper">
                <img src="{{ asset('/img/prod4.png') }}" width="150">
                <p class="type__p">Пицца</p>
            </div>
            <div class="type__wrapper">
                <img src="{{ asset('/img/prod5.png') }}" width="150">
                <p class="type__p">Бургеры</p>
            </div>
            <div class="type__wrapper">
                <img src="{{ asset('/img/prod6.png') }}" width="150">
                <p class="type__p">Суши</p>
            </div>
            <div class="type__wrapper">
                <img src="{{ asset('/img/prod1.png') }}" width="150">
                <p class="type__p">Барбекю</p>
            </div>            
            <div class="type__wrapper">
                <img src="{{ asset('/img/prod2.png') }}" width="150">
                <p class="type__p">Диетическое</p>
            </div>
            <div class="type__wrapper">
                <img src="{{ asset('/img/prod3.png') }}" width="150">
                <p class="type__p">Десерты</p>
            </div>                        
            <div class="type__wrapper">
                <img src="{{ asset('/img/prod4.png') }}" width="150">
                <p class="type__p">Суши</p>
            </div>            
        </div>
    </div>
</section>