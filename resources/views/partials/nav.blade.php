@include('partials.navdesktop')
<div class="mobnav mobile">
    <div class="col-xs-3">
        <div id="find">
            <span class="search-icon"></span>
            <input type="text" id="find-input" class="animated none">
        </div>
    </div>
    <div class="col-xs-6 text-center">
        <img src="{{ asset('img/logo.svg') }}" class="mobnav__logo">
    </div>
    <div class="col-xs-3 text-right">
        <button id="menu">
            <img src="{{ asset('img/delete.svg') }}" class="mobnav__hamburger" id="close">
            <img src="{{ asset('img/hamb.svg') }}" class="mobnav__hamburger" id="open">
        </button>
    </div>
    <div class="clearfix"></div>
</div>

