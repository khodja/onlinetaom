<div class="modal deskt">
    <div class="modal-bg"></div>
    <div class="popup-over" id="popup">
        <div class="popup-white">
            <div class="pl-20">
                <div class="text-left">
                    Вход
                </div>
                <div class="profile__account">
                    <img src="{{ asset('img/accountgreen.svg') }}">    
                </div>                   
            </div>
        </div>
        <div class="verify">
            <div class="text-center">
                <img src="{{ asset('img/phone.svg') }}" class="verify__phone">
                <div class="verify__sms">
                    Мой номер
                </div>
            </div>
            <div class="verify__input">
               <label for="tel">+998</label><input type="tel" id="tel">
            </div>
            <div class="verify__about">
                    Нажимая на кнопку “Получить код”, <a href="">я соглашаюсь с условиями использования приложения</a>
            </div>
        </div>              
    </div>     
    <div class="popup-over" id="popup">
        <div class="popup">
            <div class="pl-20">
                <div class="text-left">
                    Вход
                </div>
                <div class="profile__account">
                    <img src="{{ asset('img/accountgreen.svg') }}">    
                </div>                      
            </div>
        </div>
        <div class="verify">
            <div class="text-center">
                <img src="{{ asset('img/phone.svg') }}" class="verify__phone">
                <div class="verify__sms">
                    Код из смс                    
                </div>
            </div>
        </div>
        <div id="wrapper">
            <div id="dialog">
                <div id="form">
                    <input type='text' maxlength="1" size="1" min="1" pattern="[0-9]{1}" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" />
                    <input type='text' maxlength="1" size="1" min="1" pattern="[0-9]{1}" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" />
                    <input type='text' maxlength="1" size="1" min="1" pattern="[0-9]{1}" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" />
                    <input type='text' maxlength="1" size="1" min="1" pattern="[0-9]{1}" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" />
                </div>
            </div>        
        </div>
        <div class="verify__note">
            Отправить код еще раз
        </div>               
    </div>    
    <div class="popup-over" id="popup">
        <div class="popup">
            <div class="pl-20">
                <div class="text-left">
                    Изменить данные
                </div>
                <div class="profile__account">
                    <img src="{{ asset('img/account.svg') }}">    
                </div>                  
            </div>
        </div>
        <div class="info">
            <div class="info__inner">
                <div class="info__inner--title">Контактный телефон</div>
                <div class="info__inner--show">+998 94 619 67 67</div>
            </div>
            <div class="info__inner">
                <div class="info__inner--title">Адрес доставки</div>
                <div class="info__inner--show">Юнусабад, ул. Кашгар, Ц-4, д. 36</div>
            </div>
            <div class="info__inner">
                <div class="info__inner--title">Получатель</div>
                <div class="info__inner--show">Khalil Mukhammad-Rakan</div>
            </div>                        
        </div>        
    </div>
    <div class="popup-over" id="popup">
        <div class="profile">
            <div class="pl-20">
                <div class="text-left">
                    Профиль
                </div>
                <div class="profile__account">
                    <img src="{{ asset('img/account.svg') }}">    
                </div>               
                <img src="{{ asset('img/profile.png') }}" class="profile-photo">                
            </div>
        </div>
        <div class="profile-padding">
            <div class="profile__name">Khalil Mukhammad-Rakan</div>
            <div class="profile__phone">+998 94 619-67-67</div>
            <div class="profile__btn">
                Мои заказы
            </div>
            <div class="profile__btn">
                Редактировать данные
            </div>
            <div class="profile__btn">
                Выйти
            </div>                        
        </div>
    </div>      
</div>