@extends('layouts.app')

@section('content')
<div class="my-3 my-md-5">
<div class="container">
<div class="row row-cards">
  <div class="col-lg-3">
    <div class="row">
      <div class="col-md-6 col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="mb-4 text-center">
              <img src="{{ $data->restaurant->image  }}" class="img-fluid">
            </div>
            <h4 class="card-title"><a href="javascript:void(0)">{{ $data->restaurant->name  }}</a></h4>
            <div class="card-title">
              Статус:
            @if($data->status == -1)
                Отменен
            @endif
            @if($data->status == 0)
                В корзине
            @endif
            @if($data->status == 1)
                Оформлен
            @endif
            @if($data->status == 2)
                Принят
            @endif
            @if($data->status == 3)
                В пути
            @endif
            @if($data->status == 4)
                Доставлен
            @endif
            </div>
            <div class="card-subtitle">
                <a href="http://maps.google.com/?q={{$data->restaurant->lat.','.$data->restaurant->long}}" target="_blank">
                  Адрес: {{ $data->restaurant->address  }}
                </a>
            </div>
            <div class="card-subtitle">
              Курьер:
                @if($data->carrier_order)
                    @if($data->carrier_order->user)
                    <a href="{{ action('CarrierController@orders', $data->carrier_order->user->id)  }}">{{ $data->carrier_order->user->name  }}</a>
                    @endif
                @endif
            </div>
            <div class="card-subtitle">
              Метод Оплаты:
              @if($data->payment_method == 1)
                Payme @if($data->is_paid == 2) {{  '/ Оплачен'  }} @else {{  '/ Не оплачен'  }} @endif
              @else
                Наличные
              @endif
            </div>
            <div class="card-subtitle">
              Комментарий к заказу:
                {{ $data->comment }}
            </div>
          </div>
{{--          <form action="" class="text-center">--}}
{{--            <button class="my-1 mx-1 px-7 py-5 btn btn-success" style="font-size: 16px;">10</button>--}}
{{--            <button class="my-1 mx-1 px-7 py-5 btn btn-success" style="font-size: 16px;">20</button>--}}
{{--          </form>--}}
{{--          <form action="" class="text-center">--}}
{{--            <button class="my-1 mx-1 px-7 py-5 btn btn-success" style="font-size: 16px;">30</button>--}}
{{--            <button class="my-1 mx-1 px-7 py-5 btn btn-success" style="font-size: 16px;">50</button>--}}
{{--          </form>--}}
{{--          <form action="" class="text-center">--}}
{{--            <button class="my-1 mx-1 px-7 py-5 btn btn-success" style="font-size: 16px;">60</button>--}}
{{--            <button class="my-1 mx-1 px-7 py-5 btn btn-success" style="font-size: 16px;">90</button>--}}
{{--          </form>--}}
            @if($data->status == 1)
            <div class="text-center">
                @if(Auth::user()->hasRole('manager'))
              <form method="POST" class="d-inline" action="{{ action('OrderController@successOrder',$data->id) }}">
                @method('PUT')
                @csrf
                <input type="hidden" name="cooking_time" value="10" />
                <button class="my-1 mx-1 px-7 py-5 btn btn-success" style="font-size: 16px;">10</button>
              </form>
              <form method="POST" class="d-inline" action="{{ action('OrderController@successOrder',$data->id) }}">
                @method('PUT')
                @csrf
                <input type="hidden" name="cooking_time" value="20" />
                <button class="my-1 mx-1 px-7 py-5 btn btn-success" style="font-size: 16px;">20</button>
              </form>
              <form method="POST" class="d-inline" action="{{ action('OrderController@successOrder',$data->id) }}">
                @method('PUT')
                @csrf
                <input type="hidden" name="cooking_time" value="30" />
                <button class="my-1 mx-1 px-7 py-5 btn btn-success" style="font-size: 16px;">30</button>
              </form>
              <form method="POST" class="d-inline" action="{{ action('OrderController@successOrder',$data->id) }}">
                @method('PUT')
                @csrf
                <input type="hidden" name="cooking_time" value="50" />
                <button class="my-1 mx-1 px-7 py-5 btn btn-success" style="font-size: 16px;">50</button>
              </form>
              <form method="POST" class="d-inline" action="{{ action('OrderController@successOrder',$data->id) }}">
                @method('PUT')
                @csrf
                <input type="hidden" name="cooking_time" value="60" />
                <button class="my-1 mx-1 px-7 py-5 btn btn-success" style="font-size: 16px;">60</button>
              </form>
              <form method="POST" class="d-inline" action="{{ action('OrderController@successOrder',$data->id) }}">
                @method('PUT')
                @csrf
                <input type="hidden" name="cooking_time" value="90" />
                <button class="my-1 mx-1 px-7 py-5 btn btn-success" style="font-size: 16px;">90</button>
              </form>
                @endif
              <form method="POST" class="d-inline" action="{{ action('OrderController@ignoreOrder',$data->id) }}">
                @method('PUT')
                @csrf
                <button type="submit" class="btn px-8 py-4 btn-danger my-5" onclick="return confirm('Вы уверены?')" style="font-size: 16px;width: 240px;">Отменить</button>
              </form>
            </div>
            @endif
        </div>
      </div>
      <div class="col-md-6 col-lg-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title"><a href="{{ action('UserController@orders', $data->user->id)  }}">Пользователь : {{ $data->user->name  }}</a></h4>
            <div class="card-subtitle">
                <a href="http://maps.google.com/?q={{$data->lat.','.$data->long}}" target="_blank">
                  Адрес: {{ $data->user->adress  }}
                </a>
            </div>
            <div class="card-subtitle">
              Телефон: {{ $data->user->phone  }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-9">
    <div class="card">
      <table class="table card-table table-vcenter">
        <tbody>
        @foreach($data->order_food_trashed as $datas)
        <tr>
          @if($datas->food !== null)
          <td><img src="{{ $datas->food->image  }}" style="max-width: auto;max-height: 100px;"></td>
          <td class="text-center">
                <span class="px-1">
                {{  $datas->food->name_ru }} - <strong>{{  number_format($datas->price) }} сум</strong>
                </span>

              <br>
                Вид :
                  @if(count($datas->size)> 0 )
                    <span class="px-1">
                    {{ $datas->size[0]->name }} - <strong>{{  number_format($datas->size[0]->price * $datas->amount) }} сум</strong>
                    </span>
                  @endif
                  <br>
              Добавки :
                @foreach($datas->ids as $ids)
                <span class="px-1">
                {{ $ids->name }} - <strong>{{  number_format($ids->price) }} сум</strong>
                </span>
                @endforeach
          </td>
            @else
            <td>Удалено</td>
            <td></td>
          @endif
          <td class="text-right text-muted d-md-table-cell text-nowrap">{{  $datas->amount }} шт</td>
          <td class="text-right">
            <strong>{{  number_format($data->total_price - $data->shipping_price) }} сум</strong>
          </td>
        </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td class="text-right">Стоимость доставки : <strong>{{ number_format($data->shipping_price)  }} сум</strong></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td class="text-right">Стоимость заказа : <strong>{{ number_format($data->total_price)  }} сум</strong></td>
        </tr>
      </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>
@endsection
