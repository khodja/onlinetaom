@extends('layouts.app')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
    <h3 class="card-title">Акции</h3>
    <a class="btn btn-outline-success" href="{{ action('PromoController@create', $id) }}">Добавить</a>                    
    </div>
    <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            <th>Название (RU)</th>
            <th>Начало/Конец</th>
            <th>На Слайдере</th>
            <th>Image</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $datas)
        <tr>
            <td>{{ $datas->title_ru }}</td>
            <td>{{ $datas->start_time }}/{{ $datas->end_time }}</td>
            <td>@if($datas->slide == '1') Да @else Нет @endif</td>
            <td><img src="{{ $datas->image }}"></td>
            <td><a href="{{ action('PromoController@edit' , $datas->id) }}" class="btn btn-secondary btn-sm">Изменить</a></td>
            <td>
            <form action="{{ action('PromoController@delete' , $datas->id) }}" method="POST">
            @method('DELETE')
            @csrf
            <button class="btn icon border-0">
                <i class="fe fe-trash"></i>                        
            </button>
            </form>
            </td>
        </tr>
        @endforeach        
        </tbody>
    </table>
    </div>
</div>        
</div>
@endsection
