@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавить Акцию</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('PromoController@store',$id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="food-name-uz">Название Uz</label>
                            <input name="title_uz" type="text" class="form-control" id="food-name-uz" placeholder="Введите Название Uz">
                        </div>
                        <div class="form-group">
                            <label for="food-name-ru">Название Ру</label>
                            <input name="title_ru" type="text" class="form-control" id="food-name-ru" placeholder="Введите Название Ру">
                        </div>
                        <div class="form-group">
                            <label for="startTime">Начало Акции</label>
                            <input name="start_time" type="date" id="startTime" min="2018-01-01">
                        </div>
                        <div class="form-group">
                            <label for="endTime">Конец Акции</label>
                            <input name="end_time" type="date" id="endTime" min="2018-01-01">
                        </div>                        
                        <label class="custom-switch">
                            <input type="checkbox" name="slide" value="1" class="custom-switch-input">
                            <span class="custom-switch-indicator"></span>
                            <span class="custom-switch-description">На Главной</span>
                        </label>
                        <div class="form-group">
                            Изображение Акции <span class="font-weight-bold">1920x500</span>
                            <input required="required" type="file" class="form-control" name="image">
                        </div>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
