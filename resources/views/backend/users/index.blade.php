@extends('layouts.app')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
        @if( !empty($count) )
        <h3 class="card-title">Менеджеры</h3>
        @else
        <h3 class="card-title">Ползователи</h3>
        @endif
        <h3 class="card-title">
        @if( !empty($count) )
            <span class="table-primary">
                Всего {{ $data->count() }}
            </span>
            <span class="table-success">
                Актив {{ $data->count() - $count  }}
            </span>
        @endif
        </h3>
        <a class="btn btn-outline-success" href="{{ action('UserController@create') }}">Добавить</a>
    </div>
    @if(count($data) > 0)
        <div class="table-responsive">
        <table class="table card-table table-vcenter text-nowrap">
            <thead>
            <tr>
                <th>No</th>
                <th>Имя</th>
                <th>Логин</th>
                <th>Роль</th>
                @if($data[0]->roles[0]->type == 'member')
                    <th>Дата рег</th>
                @endif
                @if($data[0]->roles[0]->type !== 'member' && $data[0]->roles[0]->type !== 'callcenter')
                <th>Ресторан</th>
                @endif
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $datas)
            <tr @if(!empty($count)) @if(!$datas->restaurant->deleted_at != null) class="table-success" @else class="table-danger" @endif @endif>
                <td>{{ $datas->id }}</td>
                <td>{{ $datas->name }}</td>
                @if($data[0]->roles[0]->type == 'member')
                <td>{{ $datas->phone }}</td>
                @endif
                @if($data[0]->roles[0]->type == 'manager' || $data[0]->roles[0]->type == 'admin' || $data[0]->roles[0]->type == 'callcenter')
                <td>{{ $datas->email }}</td>
                @endif
                <td>
                    @if( $datas->roles[0]->type == 'admin' ) Модератор @endif
                    @if($datas->roles[0]->type == 'manager') Менеджер @endif
                    @if($datas->roles[0]->type == 'member') Гость @endif
                    @if($datas->roles[0]->type == 'callcenter') Оператор @endif
                </td>
                @if($data[0]->roles[0]->type == 'member')
                <td>{{ \Carbon\Carbon::parse($datas->created_at)->isoFormat('D MMMM HH:mm') }}</td>
                @endif
                @if($datas->roles[0]->type !== 'member' && $data[0]->roles[0]->type !== 'callcenter')
                @if( $datas->restaurant !== null )
                    <td>{{ $datas->restaurant->name }}</td>
                @else
                    <td>Нет ресторана</td>
                @endif
                @endif
                <td class="text-right">
                @if($datas->roles[0]->type == 'member')
                <a href="{{ action('UserController@orders', $datas->id)  }}" class="btn btn-secondary btn-sm">
                    Заказы пользователя
                </a>
                @endif
                <a href="{{ action('UserController@edit' , $datas->id) }}" class="btn icon border-0"><i class="fe fe-edit"></i></a>
                <form class="d-inline-block" action="{{ action('UserController@delete' , $datas->id) }}" method="POST">
                @method('DELETE')
                @csrf
                <button type="submit" class="btn icon border-0" onclick="return confirm('Вы уверены?')">
                        <i class="fe fe-trash"></i>
                </button>
                </form>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    @endif
    </div>
</div>
@endsection
