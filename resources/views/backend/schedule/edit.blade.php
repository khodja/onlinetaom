@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Params</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('ScheduleController@update',$id) }}" method="POST">
                        @method('PUT')                        
                        @csrf
                            <div class="form-group">
                                <label for="food-name-ru">Открытие</label>
                                <input value="{{ $data->open_hour }}" name="open_hour" type="text" class="form-control" id="food-name-ru">
                            </div>
                            <div class="form-group">
                                <label for="close_hour">Закрытие</label>
                                <input value="{{ $data->close_hour }}" name="close_hour" type="text" class="form-control" id="close_hour">
                            </div>
                        <button class="btn btn-success">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
