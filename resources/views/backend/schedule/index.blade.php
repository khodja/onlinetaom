@extends('layouts.app')

@section('content')
<div class="container">
        <div class="card">
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <br>
                <form action="{{ action('ScheduleController@update') }}" method="POST">
                @csrf
                @method('PATCH')
                <table style="width:100%;" class="text-center">
                        <tr>
                            <td>День Недели</td>
                            <td>Открытие</td>
                            <td>Закрытие</td>
                        </tr>
                            @foreach($data as $k=>$datas)
                            <tr>
                                <td>
                                    @if($datas->weekday == 1)
                                        Понедельник
                                    @endif
                                    @if($datas->weekday == 2)
                                        Вторник
                                    @endif
                                    @if($datas->weekday == 3)
                                        Среда
                                    @endif
                                    @if($datas->weekday == 4)
                                        Четверг
                                    @endif
                                    @if($datas->weekday == 5)
                                        Пятница
                                    @endif
                                    @if($datas->weekday == 6)
                                        Суботта
                                    @endif
                                    @if($datas->weekday == 7)
                                        Воскресение
                                    @endif                                                                                                                                                                                                                        
                                </td>
                                <td>
                                    <div class="input-group bootstrap-timepicker">
                                        <input id="timepick{{ $k }}" name="open_hour[]" value="{{ $datas->open_hour }}" type="text" class="input-small mcenter">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                    </div>                                    
                                </td>
                                <td>
                                    <div class="input-group bootstrap-timepicker">
                                        <input id="timepick{{ $k+7 }}" name="close_hour[]" value="{{ $datas->close_hour }}" type="text" class="input-small mcenter">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                    </div>
                                </td>
                                <input type="hidden" name="primary_id[]" value="{{ $datas->id }}">
                            </tr>
                            @endforeach
                            <td>
                                <button class="btn btn-success">Обновить</button>
                            </td>
                        </table>
                    </form>
            </div>
        </div>
</div>
@endsection
