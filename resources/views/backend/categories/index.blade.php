@extends('layouts.app')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
    <h3 class="card-title">Категории</h3>
    <a class="btn btn-sm btn-outline-success" href="{{ action('CategoryController@create') }}">Добавить</a>
    </div>
    <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            <th>Название</th>
            <th>Photo</th>
            <th>Алиас</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $datas)                    
        <tr>
            <td>{{ $datas->name_ru }}</td>
            <td><img src="{{ asset('uploads/categories/'.$datas->id.'.png') }}" width="80"></td>
            <td>{{ $datas->alias }}</td>
            <td class="text-right">
                <a href="{{ action('CategoryController@edit' , $datas->id) }}" class="btn btn-secondary btn-sm">Изменить</a>
            </td>
            <td></td>
            <td>
            <form action="{{ action('CategoryController@delete' , $datas->id) }}" method="POST">
            @method('DELETE')
            @csrf
                <button class="btn icon border-0" onclick="return confirm('Вы уверены?')">
                    <i class="fe fe-trash"></i>                        
                </button>            
            </form>
            </td>            
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>        
</div>
@endsection
