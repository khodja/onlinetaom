@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Изменить Блюдо</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('FoodController@update', $data->id) }}" method="POST" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="food-name-uz">Название Uz</label>
                            <input value="{{ $data->name_uz }}" name="name_uz" type="text" class="form-control" id="food-name-uz">
                        </div>
                        <div class="form-group">
                            <label for="food-name-ru">Название Ru</label>
                            <input value="{{ $data->desc_uz }}" name="name_ru" type="text" class="form-control" id="food-name-ru">
                        </div>
                        <div class="form-group">
                            Выберите категорию
                            <select name="category_id" class="form-control">
                                @foreach( $rest->categories as $datas )
                                <option value="{{ $datas->id }}"
                                @if($datas->id == $data->category_id) selected @endif>
                                    {{ $datas->name_ru }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="food-desc-uz">Описание Uz</label>
                            <input value="{{ $data->desc_uz }}" name="desc_uz" type="text" class="form-control" id="food-desc-uz">
                        </div>
                        <div class="form-group">
                            <label for="food-desc-ru">Описание Ru</label>
                            <input value="{{ $data->desc_uz }}" name="desc_ru" type="text" class="form-control" id="food-desc-ru">
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <label for="food-cook">Время Приготовления</label>--}}
                            <input value="{{ $data->cook_time }}" name="cook_time" type="hidden" class="form-control" id="food-cook"  min="0">
{{--                        </div>--}}
                        <div class="form-group">
                            <label for="food-price">Цена UZS</label>
                            <input value="{{ $data->price }}" name="price" type="number" class="form-control" id="food-price"  min="0">
                        </div>
                        <div class="form-group">
                            <img src="{{ asset('uploads/food/'.$data->id.'.jpg') }}" width="140">
                            Для изменения загрузите новое фото <span class="font-weight-bold">840x520</span>
                            <input type="file" class="form-control" name="image"> 
                        </div>                        
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
