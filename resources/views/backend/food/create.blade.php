@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавить Еду</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('FoodController@store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                            <div class="form-group">
                                <label for="food-name-uz">Название Uz</label>
                                <input name="name_uz" type="text" class="form-control" id="food-name-uz" placeholder="Введите название">
                            </div>
                        @if(request()->restaurant_id != null)
                        <input type="hidden" name="restaurant_id" value="{{ request()->restaurant_id }}">
                        @endif    
                        {{-- @if( request()->restaurant_id == null)                                            
                        <div class="form-group">
                            Выберите ресторан
                            <select name="restaurant_id" class="form-control">
                                @foreach( $data as $datas )
                                <option value="{{ $datas->id }}">{{ $datas->name }}</option>
                                @endforeach
                            </select>                        
                        </div>
                        @endif --}}
                        <div class="form-group">
                            Выберите категорию
                            <select name="category_id" class="form-control">
                                @foreach( $data->categories as $datas )
                                <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="food-name-ru">Название Ru</label>
                            <input name="name_ru" type="text" class="form-control" id="food-name-ru" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label for="food-desc-uz">Описание Uz</label>
                            <input name="desc_uz" type="text" class="form-control" id="food-desc-uz" placeholder="Введите описание">
                        </div>
                        <div class="form-group">
                            <label for="food-desc-ru">Описание Ru</label>
                            <input name="desc_ru" type="text" class="form-control" id="food-desc-ru" placeholder="Введите описание">
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <label for="food-cook">Время Приготовления</label>--}}
                            <input name="cook_time" type="hidden" value="1" class="form-control" id="food-cook" placeholder="Введите время приготовления" min="0">
{{--                        </div>--}}
                        <div class="form-group">
                            <label for="food-price">Цена UZS</label>
                            <input name="price" type="number" class="form-control" id="food-price" placeholder="Введите Цену" min="0">
                        </div>
                        <div class="form-group">
                            Изображение Блюда <span class="font-weight-bold">840x520</span>
                            <input type="file" class="form-control" name="image"> 
                        </div>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
<script>
      $(function() {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
      });    
</script>
@endsection
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
@endsection
