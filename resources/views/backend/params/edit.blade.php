@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавить изменить</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('ParamController@update',$id) }}" method="POST" enctype="multipart/form-data">
                        @method('PUT')                        
                        @csrf
                        <div class="form-group">
                            <label for="food-name-uz">Название</label>
                            <input value="{{ $data->name }}" name="name" type="text" class="form-control" id="food-name-uz" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label for="food-price">Цена UZS</label>
                            <input value="{{ $data->price }}" name="price" type="number" class="form-control" id="food-price" placeholder="Введите цену" min="0">
                        </div>
                        <div class="form-group">
                           <img src="{{ asset('uploads/params/'.$data->id.'.jpg') }}" width="140">
                            Для изменения загрузите новое фото <span class="font-weight-bold">200x200</span>
                            <input type="file" class="form-control" name="image">
                        </div>
                        <button class="btn btn-success">Изменить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
