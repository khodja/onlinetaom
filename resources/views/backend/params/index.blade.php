@extends('layouts.app')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
    <h3 class="card-title">Параметры блюда: {{ $name }}</h3>
    <a class="btn btn-outline-success" href="{{ action('ParamController@create', $id) }}">Добавить</a>                    
    </div>
    <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            <th>Название</th>
            <th>Photo</th>
            <th>Цена</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>      
        @foreach($data as $datas)
        <tr>
            <td>{{ $datas->name }}</td>
            <td><img src="{{ asset('uploads/params/'.$datas->id.'.jpg') }}"  width="80" /></td>
            <td>{{ number_format($datas->price) }} сум</td>
            <td><a href="{{ action('ParamController@edit' , $datas->id) }}" class="btn btn-secondary btn-sm">Изменить</a></td>
            <td>
            <form action="{{ action('ParamController@delete' , $datas->id) }}" method="POST">
            @method('DELETE')
            @csrf
            <button class="btn icon border-0">
                <i class="fe fe-trash"></i>                        
            </button>            
            </form>
            </td>
        </tr>
        @endforeach        
        </tbody>
    </table>
    </div>
</div>           
</div>
@endsection
