@extends('layouts.app')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
    <h3 class="card-title">Размеры</h3>
    <a class="btn btn-outline-success" href="{{ action('SizeController@create', $id) }}">Добавить</a>                    
    </div>
    <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            <th>Название</th>
            <th>Цена</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $datas)                    
        <tr>
            <td>{{ $datas->name }}</td>
            <td>{{ $datas->price }} UZS</td>
            <td class="text-right"><a href="{{ action('SizeController@edit' , $datas->id) }}" class="btn btn-secondary btn-sm">Изменить</a></td>
            <td class="text-center">
            <form action="{{ action('SizeController@delete' , $datas->id) }}" method="POST">
            @method('DELETE')
            @csrf
                <button class="btn icon border-0">
                    <i class="fe fe-trash"></i>                        
                </button>            
            </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>        
</div>
@endsection
