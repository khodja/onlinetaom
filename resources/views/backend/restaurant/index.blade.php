@extends('layouts.app')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
    <h3 class="card-title">@if(Auth::user()->hasRole('admin')) Рестораны  @else Ресторан @endif</h3>
            @if(Auth::user()->hasRole('admin'))
            <a class="btn btn-sm btn-outline-success" href="{{ action('RestaurantController@create') }}">Добавить</a>
            @endif
    </div>
    <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            <th>Название</th>
            <th>Адрес</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $datas)
        <tr>
            <td width="200"><img src="{{ asset('uploads/restaurant/'.$datas->id.'.jpg') }}" width="60" style="margin-right: 10px;">{{ $datas->name }}</td>
            </td>
            <td>@if($datas->district)Район {{ $datas->district->name_ru }}@endif {{ $datas->address }}</td>
            @if($datas->deleted_at === null)
            <td class="text-right">
            @if(Auth::user()->hasRole('admin'))
            <a href="{{ action('FoodController@menu' , $datas->alias) }}" class="btn btn-secondary btn-sm">Меню</a>
            <a href="{{ action('PromoController@index' , $datas->alias) }}" class="btn btn-secondary btn-sm">Акции</a>
            <a href="{{ action('ScheduleController@index' , $datas->alias) }}" class="btn btn-secondary btn-sm"><i class="fe fe-watch"></i> График</a>
{{--            <a href="{{ action('FoodController@create' , 'restaurant_id='.$datas->id) }}" class="btn btn-secondary btn-sm">Добавить Блюдо</a>--}}
            <a href="{{ action('RestaurantController@edit' , $datas->id) }}" class="btn icon border-0"><i class="fe fe-edit"></i></a>
                @endif
            </td>
            @else
            <td class="text-right">
                @if(Auth::user()->hasRole('admin'))
                    <a href="{{ action('FoodController@menu' , $datas->alias) }}" class="btn btn-secondary btn-sm">Меню</a>
                    <a href="{{ action('PromoController@index' , $datas->alias) }}" class="btn btn-secondary btn-sm">Акции</a>
                    <a href="{{ action('ScheduleController@index' , $datas->alias) }}" class="btn btn-secondary btn-sm"><i class="fe fe-watch"></i> График</a>
                    <a href="{{ action('RestaurantController@edit' , $datas->id) }}" class="btn icon border-0"><i class="fe fe-edit"></i></a>
                @endif
            </td>
            @endif
            <td>
                @if(Auth::user()->hasRole('admin'))
                    @if($datas->deleted_at === null)
                    <form action="{{ action('RestaurantController@delete' , $datas->id) }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button class="btn btn-sm btn-danger" onclick="return confirm('Вы уверены?')">
                        <i class="fe fe-x"></i> Отключить
                    </button>
                    </form>
                    @else
                    <form action="{{ action('RestaurantController@restore' , $datas->id) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <button class="btn btn-sm btn-success">
                        <i class="fe fe-refresh-ccw"></i> Включить
                    </button>
                    </form>
                    @endif
                @endif
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>
</div>
@endsection
