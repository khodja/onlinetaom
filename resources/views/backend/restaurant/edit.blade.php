@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Изменить Ресторан</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form action="{{ action('RestaurantController@update', $rest->id) }}" method="POST" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-group">
                                <label for="restaurant-name">Название</label>
                                <input required="required" value="{{ $rest->name }}" name="name" type="text" class="form-control" id="restaurant-name">
                            </div>
                            <div class="form-group">
                                <label for="restaurant-desc-uz">Описание (Uz)</label>
                                <input required="required" value="{{ $rest->desc_uz }}" name="desc_uz" type="text" class="form-control" id="restaurant-desc-uz">
                            </div>
                            <div class="form-group">
                                <label for="restaurant-desc-ru">Описание (Ru)</label>
                                <input required="required" value="{{ $rest->desc_ru }}" name="desc_ru" type="text" class="form-control" id="restaurant-desc-ru">
                            </div>
                            @if(!Auth::user()->hasRole('manager'))
                            <div class="form-group">
                                Выберите категорию
                                <select multiple name="category_id[]" class="chosen-select form-control">
                                    @foreach( $data as $datas )
                                    <option value="{{ $datas->id }}" @if( in_array($datas->id, $categories)) selected @endif>{{ $datas->name_ru }}</option>
                                    @endforeach
                                </select>
                                </div>
                            <div class="form-group">
                                Выберите район
                                <select name="district_id" class="form-control">
                                    @foreach( $dist as $datas )
                                    <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="restaurant-adress">Адрес</label>
                                <input required="required" value="{{ $rest->address }}" name="address" type="text" class="form-control" id="restaurant-adress">
                            </div>
                            <div class="form-group">
                                <div id="map" style="height: 400px; width: 500px;"></div>
                                <input required="required" id="long" value="{{ $rest->long }}" name="long" type="hidden" class="form-control">
                                <input required="required" id="lat" value="{{ $rest->lat }}" name="lat" type="hidden" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="food-cook">Телефон</label>
                                <input required="required" value="{{ $rest->phone }}" name="phone" type="tel" class="form-control" id="restaurant-phone">
                            </div>
                            <div class="form-group">
                                <label for="food-phone-1">Телефон</label>
                                <input required="required" value="{{ $rest->phone1 }}" name="phone1" type="tel" class="form-control" id="food-phone-1">
                            </div>
                            <div class="form-group">
                                <label for="food-phone-2">Телефон</label>
                                <input required="required" value="{{ $rest->phone2 }}" name="phone2" type="tel" class="form-control" id="food-phone-2">
                            </div>
                            <div class="form-group">
                                <label for="percentage">Процент услуг</label>
                                <input required="required" name="percentage" value="{{ $rest->percentage }}" type="number" class="form-control" id="percentage">
                            </div>
                            <div class="form-group">
                                <label for="ot_delta">Стоимость доставки OnlineTaom</label>
                                <input required="required" name="ot_delta" value="{{ $rest->ot_delta }}"  type="number" class="form-control" id="percentage">
                            </div>
                            <div class="form-group">
                                <label for="delivery_delta">Стоимость доставки Ресторана</label>
                                <input required="required" name="delivery_delta" value="{{ $rest->delivery_delta }}"  type="number" class="form-control" id="percentage">
                            </div>
                            <div class="form-group">
                            <label class="custom-switch">
                                <span class="mr-3">Бесплатная Доставка</span>
                                <input type="checkbox" @if($rest->shipping_free == 1) checked="checked" @endif class="custom-switch-input" name="shipping_free" value="1">
                                <span class="custom-switch-indicator"></span>
                            </label>
                            </div>
                            <div class="form-group">
                            <label class="custom-switch">
                                <span class="mr-3">На Главной</span>
                                <input type="checkbox" @if($rest->in_main == 1) checked="checked" @endif class="custom-switch-input" name="in_main" value="1">
                                <span class="custom-switch-indicator"></span>
                            </label>
                            </div>
                            <div class="form-group">
                                <div class="form-label">Способы Оплаты</div>
                                <div class="custom-switches-stacked">
                                <label class="custom-switch">
                                    <input type="radio" name="payments" value="[0]" @if($rest->payments == '[0]') checked="checked" @endif class="custom-switch-input" checked>
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Наличные</span>
                                </label>
                                <label class="custom-switch">
                                    <input type="radio" name="payments" value="[0,1]" @if($rest->payments == '[0,1]') checked="checked" @endif class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Наличные/Payme</span>
                                </label>
                                </div>
                            </div>
                            <div class="form-group">
                                    <img src="{{ asset('uploads/restaurant/'.$rest->id.'.jpg') }}" width="140">
                                    Для изменения лого загрузите новое фото <span class="font-weight-bold"> 200x200 </span>
                                    <input type="file" class="form-control" name="image">
                            </div>
                            <div class="form-group">
                                    <img src="{{ asset('uploads/restcover/'.$rest->id.'.jpg') }}" width="140">
                                    Для изменения Ковэра загрузите новое фото <span class="font-weight-bold"> 640x392 </span>
                                    <input type="file" class="form-control" name="cover">
                            </div>
                            @endif
                            <button class="btn btn-success">Изменить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js') }}"></script>
<script>
    var lattitude = $('#lat');
    var longitude = $('#long');
function initMap() {
    $(document).ready(function(){
var latlng = new google.maps.LatLng(lattitude.val(), longitude.val());
var map = new google.maps.Map(document.getElementById('map'), {
    center: latlng,
    zoom: 12,
        animation:google.maps.Animation.BOUNCE
});
var marker = new google.maps.Marker({
    position: latlng,
    map: map,
    draggable: true
});
var lat ,long;
google.maps.event.addListener(marker, 'dragend', function (event) {
    lat  = this.getPosition().lat().toFixed(6);
    long = this.getPosition().lng().toFixed(6);
    lattitude.val(lat);
    longitude.val(long);
});
})
}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs3cvxdAATTvzZ-srgPAID1d2IZHuZcZE&callback=initMap"></script>
<script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
<script>
      $(function() {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
      });
</script>
@endsection
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
@endsection
