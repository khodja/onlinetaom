@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Review</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('ReviewController@store',$id) }}" method="POST">
                        <div class="form-group">
                            <label for="food-name-uz">Mark</label>
                        <input value="{{ $data->mark }}" name="mark" type="text" class="form-control" id="food-name-uz" placeholder="Enter Mark">
                        </div>
                        <div class="form-group">
                            <label for="food-message">message</label>
                            <input value="{{ $data->message }}" name="message" type="text" class="form-control" id="food-message" placeholder="Enter message" min="0">
                        </div>
                        <button class="btn btn-success">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
