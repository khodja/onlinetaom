@extends('layouts.app')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
        <h3 class="card-title">Курьеры
        </h3>
        <h3 class="card-title">
        @if( !empty($count) )
            <span class="table-primary">
                Всего {{ $data->count() }}
            </span>
            <span class="table-success">
                Актив {{ $data->count() - $count  }}
            </span>
        @endif
        </h3>
        <a class="btn btn-outline-success" href="{{ action('CarrierController@create') }}">Добавить</a>
        </div>
        <div class="table-responsive">
        <table class="table card-table table-vcenter text-nowrap">
            <thead>
            <tr>
                <th>Имя</th>
                <th>Логин/Телефон</th>
                <th>Роль</th>
                <th>Номер машины</th>
                <th>Дата регистрации</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $datas)
            <tr @if(!$datas->online) class="table-success" @else class="table-danger" @endif>
                <td>{{ $datas->name }}</td>
                <td>{{ $datas->phone_number }}</td>
                <td>Курьер</td>
                <td>{{ $datas->car_number }}</td>
                <td>{{ \Carbon\Carbon::parse($datas->created_at)->isoFormat('D MMMM HH:mm') }}</td>
                <td class="text-right">
                <a href="{{ action('CarrierController@orders', $datas->id)  }}" class="btn btn-secondary btn-sm">
                    Заказы курьера
                </a>
                <a href="{{ action('CarrierController@edit' , $datas->id) }}" class="btn icon border-0"><i class="fe fe-edit"></i></a>
                <form class="d-inline-block" action="{{ action('CarrierController@delete' , $datas->id) }}" method="POST">
                @method('DELETE')
                @csrf
                <button type="submit" class="btn icon border-0" onclick="return confirm('Вы уверены?')">
                        <i class="fe fe-trash"></i>
                </button>
                </form>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection
