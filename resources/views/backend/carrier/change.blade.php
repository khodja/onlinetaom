@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Курьер</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('CarrierController@updateOrder', $data->id) }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label>Выберите Курьера</label>
                        <select name="driver_id" class="form-control">
                            @foreach( $user as $datas )
                            <option value="{{ $datas->id }}" @if($data->driver_id == $datas->id) selected @endif>{{ $datas->name }}</option>
                            @endforeach
                            <option value="0">Нет</option>
                        </select>
                        </div>
                        <button class="btn btn-success">Изменить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
