@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавить Курьера</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('CarrierController@store') }}" method="POST">
                    @csrf
                        <div class="form-group">
                            <label for="food-name-uz">Имя</label>
                            <input name="name" type="text" class="form-control" id="food-name-uz" placeholder="Введите Имя" checked="checked" />
                        </div>
                        <div class="form-group">
                            <label for="food-desc-uz">Номер телефона</label>
                            <input name="phone_number" type="number" class="form-control" id="food-desc-uz" placeholder="Введите телефон" checked="checked" />
                        </div>
                        <div class="form-group">
                            <label for="phone">Номер машины</label>
                            <input name="car_number" type="text" class="form-control" id="phone" placeholder="Введите Номер машины" checked="checked" />
                        </div>
                        <div class="form-group">
                            <label for="food-password">Пароль</label>
                            <input name="password" type="password" class="form-control" id="food-password" placeholder="Введите Пароль" checked="checked" />
                        </div>
                        <button class="btn btn-success">Добавить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
