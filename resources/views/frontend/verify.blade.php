<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OnlineTaom</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
    <body>
        <div class="events mobile">
            <div class="container">
                <button class="events__arrow">
                    <img src="{{ asset('img/arrowgreen.svg') }}">
                </button>
                <div class="text-center">
                    Вход
                </div>
            </div>
        </div>
        <div class="verify">
            <div class="text-center">
                <img src="{{ asset('img/phone.svg') }}" class="verify__phone">
                <div class="verify__sms">
                    Код из смс                    
                </div>
            </div>
        </div>
        <div id="wrapper">
            <div id="dialog">
                <div id="form">
                    <input type='text' maxlength="1" size="1" min="1" pattern="[0-9]{1}" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" />
                    <input type='text' maxlength="1" size="1" min="1" pattern="[0-9]{1}" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" />
                    <input type='text' maxlength="1" size="1" min="1" pattern="[0-9]{1}" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" />
                    <input type='text' maxlength="1" size="1" min="1" pattern="[0-9]{1}" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" />
                </div>
            </div>        
        </div>
        <div class="verify__note">
            Отправить код еще раз
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script>
            var vcode = (function(){
            //cache dom
            var $inputs = $("#form").find("input");

            //bind events
            $inputs.on('keyup', processInput);
            
            //define methods
            function processInput(e) {
                var x = e.charCode || e.keyCode;
                if( (x == 8 || x == 46) && this.value.length == 0) {
                var indexNum = $inputs.index(this);
                if(indexNum != 0) {
                    $inputs.eq($inputs.index(this) - 1).focus();
                }
                }
                
                if( ignoreChar(e) ) 
                return false;
                else if (this.value.length == this.maxLength) {
                $(this).next('input').focus();
                }
            }
            function onlyNumbers(){
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) || 
                    // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                }

                var charValue = String.fromCharCode(e.keyCode)
                    , valid = /^[0-9]+$/.test(charValue);

                if (!valid) {
                    e.preventDefault();
                }                
            }
            function ignoreChar(e) {
                var x = e.charCode || e.keyCode;
                if (x == 37 || x == 38 || x == 39 || x == 40 )
                return true;
                else 
                return false
            }
            
            })();                                                    
        </script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        @yield('scripts')
    </body>
</html>
