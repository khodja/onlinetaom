<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OnlineTaom</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
<body>
    <div class="wrapper">
        @include('partials.navdesktop')
        <div class="favorite-food">
            <img src="{{ asset('img/favstart.svg') }}">
            Избранное
        </div>      
        <div class="favorite desktop">
            <button class="favorite__btn active">Рестораны</button>
            <button class="favorite__btn">Блюда</button>
        </div>
        <div class="restaurant desktop">
            <div class="container cont-fix">
            <div class="restaurant-desktop__ins">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <img src="{{ asset('img/brand1.png') }}" width="70" class="img">
                    <div class="restaurant-xs vm">
                            <div class="restaurant-desktop__ins--title">
                                Caffee’issimo
                            </div>
                            <div class="restaurant-desktop__ins-p">
                                Европейская
                            </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                    <div class="restaurant-desktop__long">
                        8 км
                    </div>
                    <div class="restaurant-desktop__location">
                        <img src="{{ asset('img/greentruck.svg') }}"> 
                        14 000 сум                   
                    </div>     
                </div>
                <button class="delete">
                    <img src="{{ asset('img/delete.svg') }}" class="vm">                    
                </button>
                <div class="clearfix"></div>
            </div>
            <div class="restaurant-desktop__ins">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <img src="{{ asset('img/brand1.png') }}" width="70" class="img">
                    <div class="restaurant-xs vm">
                            <div class="restaurant-desktop__ins--title">
                                Caffee’issimo
                            </div>
                            <div class="restaurant-desktop__ins-p">
                                Европейская
                            </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                    <div class="restaurant-desktop__long">
                        8 км
                    </div>
                    <div class="restaurant-desktop__location">
                        <img src="{{ asset('img/greentruck.svg') }}"> 
                        14 000 сум                   
                    </div>     
                </div>
                <button class="delete">
                    <img src="{{ asset('img/delete.svg') }}">                    
                </button>
                <div class="clearfix"></div>
            </div> 
            <div class="restaurant-desktop__ins">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <img src="{{ asset('img/brand1.png') }}" width="70" class="img">
                    <div class="restaurant-xs vm">
                            <div class="restaurant-desktop__ins--title">
                                Caffee’issimo
                            </div>
                            <div class="restaurant-desktop__ins-p">
                                Европейская
                            </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                    <div class="restaurant-desktop__long">
                        8 км
                    </div>
                    <div class="restaurant-desktop__location">
                        <img src="{{ asset('img/greentruck.svg') }}"> 
                        14 000 сум                   
                    </div>     
                </div>
                <button class="delete">
                    <img src="{{ asset('img/delete.svg') }}">                    
                </button>
                <div class="clearfix"></div>
            </div> 
            <div class="restaurant-desktop__ins">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <img src="{{ asset('img/brand1.png') }}" width="70" class="img">
                    <div class="restaurant-xs vm">
                            <div class="restaurant-desktop__ins--title">
                                Caffee’issimo
                            </div>
                            <div class="restaurant-desktop__ins-p">
                                Европейская
                            </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                    <div class="restaurant-desktop__long">
                        8 км
                    </div>
                    <div class="restaurant-desktop__location">
                        <img src="{{ asset('img/greentruck.svg') }}"> 
                        14 000 сум                   
                    </div>     
                </div>
                <button class="delete">
                    <img src="{{ asset('img/delete.svg') }}">                    
                </button>
                <div class="clearfix"></div>
            </div>                                        
            </div>
        </div>                                                      
    </div>        
        <div class="events mobile">
            <div class="container">
                <button class="events__arrow">
                    <img src="{{ asset('img/arrowgreen.svg') }}">
                </button>
                <div class="text-center">
                    Акции
                </div>
            </div>
        </div>
        <div class="favorite mobile">
            <button class="favorite__btn active">Рестораны</button>
            <button class="favorite__btn">Блюда</button>
        </div>
        <div class="restaurant mobile">
            <div class="restaurant__ins">
                <div class="restaurant__name">
                    <img src="{{ asset('img/brand1.png') }}" width="70" class="img">
                    Caffee’issimo
                    <button class="delete">
                        <img src="{{ asset('img/delete.svg') }}">                    
                    </button>                    
                </div>
                <div class="restaurant__ins-p--2 pt-5">
                    <p class="restaurant__desc pb-10">Европейская</p>
                </div>
                <div class="restaurant__ins-price--2">
                <p class="restaurant__long pr-25">4.2 км</p>
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                </div>           
            </div>
            <div class="restaurant__ins">
                <div class="restaurant__name">
                    <img src="{{ asset('img/brand1.png') }}" width="70" class="img">
                    Caffee’issimo
                    <button class="delete">
                        <img src="{{ asset('img/delete.svg') }}">                    
                    </button>                    
                </div>
                <div class="restaurant__ins-p--2 pt-5">
                    <p class="restaurant__desc pb-10">Европейская</p>
                </div>
                <div class="restaurant__ins-price--2">
                <p class="restaurant__long pr-25">4.2 км</p>
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                </div>           
            </div>
            <div class="restaurant__ins">
                <div class="restaurant__name">
                    <img src="{{ asset('img/brand1.png') }}" width="70" class="img">
                    Caffee’issimo
                    <button class="delete">
                        <img src="{{ asset('img/delete.svg') }}">                    
                    </button>                    
                </div>
                <div class="restaurant__ins-p--2 pt-5">
                    <p class="restaurant__desc pb-10">Европейская</p>
                </div>
                <div class="restaurant__ins-price--2">
                <p class="restaurant__long pr-25">4.2 км</p>
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                </div>           
            </div>
            <div class="restaurant__ins">
                <div class="restaurant__name">
                    <img src="{{ asset('img/brand1.png') }}" width="70" class="img">
                    Caffee’issimo
                    <button class="delete">
                        <img src="{{ asset('img/delete.svg') }}">                    
                    </button>                    
                </div>
                <div class="restaurant__ins-p--2 pt-5">
                    <p class="restaurant__desc pb-10">Европейская</p>
                </div>
                <div class="restaurant__ins-price--2">
                <p class="restaurant__long pr-25">4.2 км</p>
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                </div>           
            </div>                                                       
        </div>
    @include('partials.footer')
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
@yield('scripts')
</body>
</html>
