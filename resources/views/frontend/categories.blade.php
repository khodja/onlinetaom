@extends('layouts.frontend')
@section('content')
<div class="mobnav__bottom mobile">
   <div class="text-center">
      <img src="{{ asset('img/loc.svg') }}"> Юнусабад, ул. Кашгар, д. 32
   </div>
</div>
   @include('partials.type')
<section class="categories desktop">
   <div class="categories__title">
      Пиццерии
   </div>
   <div class="container">
   <div class="categories__filter">
      <div class="categories__filter-el categories__filter-el-1">
         <img src="{{ asset('/img/filt1.svg') }}">
         <div class="categories-filter">
            <div class="categories-filter__title">
               Стоимость блюд
            </div>
            <div class="categories-filter__info">
               до <span class="fitler__price">480 000</span> сум
            </div>
            <div class="categories-filter__range range-slider">
               <input type="text" class="js-range-slider" value="" />
            </div>
         </div>
      </div>
      <div class="categories__filter-el categories__filter-el-2">
         <img src="{{ asset('/img/filt3.svg') }}">
         <div class="categories-filter">
            <div class="categories-filter__title">
               Время приготовления
            </div>
            <div class="categories-filter__info">
               до <span class="categories-filter__time">40</span> минут
            </div>
            <div class="categories-filter__range range-slider">
               <input type="text" class="js-range-slider-2" value="" />
            </div>
         </div>
      </div>
      <div class="categories__filter-el categories__filter-el-3">
         <img src="{{ asset('/img/filt2.svg') }}">
         <div class="categories-filter">
            <div class="categories-filter__title">
               Бесплатная доставка
            </div>
            <div class="description-choice__type">
               <input type="radio" name="ship" id="shipping_1" value="small" checked />
               <label for="shipping_1">да</label>
               <input type="radio" name="ship" id="shipping_2" value="small" />
               <label for="shipping_2">не важно</label>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
    </div>      
   </div>
</section>
@include('partials.popular')
<section class="rest-nav mobile">
   <div class="rest-nav__inner">
      <img src="{{ asset('/img/prod1.png') }}" class="rest-nav__img">
      <p class="rest-nav__p">Азиатская</p>
   </div>
   <div class="rest-nav__inner">
      <img src="{{ asset('/img/prod2.png') }}" class="rest-nav__img">
      <p class="rest-nav__p">Национальная</p>
   </div>
   <div class="rest-nav__inner">
      <img src="{{ asset('/img/prod3.png') }}" class="rest-nav__img">
      <p class="rest-nav__p">Пицца</p>
   </div>
   <div class="rest-nav__inner">
      <img src="{{ asset('/img/prod4.png') }}" class="rest-nav__img">
      <p class="rest-nav__p">Бургеры</p>
   </div>
   <div class="rest-nav__inner">
      <img src="{{ asset('/img/prod5.png') }}" class="rest-nav__img">
      <p class="rest-nav__p">Европейская</p>
   </div>
</section>
@include('frontend.mobile')
@include('partials.download')
@include('partials.text')
@endsection