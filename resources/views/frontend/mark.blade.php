<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OnlineTaom</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
    <body>
        <div class="mark mobile">
            <div class="container">
                <button class="events__arrow">
                    <img src="{{ asset('img/arrowgreen.svg') }}">
                </button>
            </div>
        </div>
        <div class="verify">
            <div class="text-center">
                <img src="{{ asset('img/logo.svg') }}" class="mt-30 mb-30">
            </div>
            <div class="mark__wrap">
                <div class="mark__text">
                    Оцените нас, пожалуйста
                    <form class="rating mark__stars">
                        <input type="radio" id="star5" name="rating" value="5" /><label class="full" for="star5"></label>
                        <input type="radio" id="star4" name="rating" value="4" /><label class="full" for="star4"></label>
                        <input type="radio" id="star3" name="rating" value="3" /><label class="full" for="star3"></label>
                        <input type="radio" id="star2" name="rating" value="2" /><label class="full" for="star2"></label>
                        <input type="radio" id="star1" name="rating" value="1" /><label class="full" for="star1"></label>
                    </form>
                </div>
            </div>
            <div class="mark__wrap">
                <div class="mark__add">
                    <input type="radio" name="same" id="fast1">
                    <label for="fast1">Bad</label>
                    <input type="radio" name="same" id="fast2">
                    <label for="fast2">Ok</label>
                    <input type="radio" name="same" id="fast3">
                    <label for="fast3">Good</label>
                    <br><br>
                    <input type="text" placeholder="Добавьте отзыв">
                    
                </div>
            </div>
            <div class="mark__info">
                    Помогите нам повысить качество обслуживания и всегда поддерживать его на высоком уровне!                
            </div>
        </div>
    </body>
</html>
