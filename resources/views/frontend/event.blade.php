<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OnlineTaom</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
    <body>
        <div class="wrapper">
        @include('partials.navdesktop')
        <section class="announce desktop">
            <div class="container cont-fix">
                <div class="announce__back col-md-4 col-sm-4 col-xs-6">
                    <img src="{{ asset('img/arrowleft.svg') }}">                
                    Все блюда и рестораны
                </div>
                <div class="announce__title col-md-4 col-sm-4 col-xs-6">
                    <img src="{{ asset('img/event.svg') }}">
                    Акции
                </div>
            </div>
        </section>
        <section class="slider desktop">
                <div class="slider-for">
                    <div class="slider__inner">
                    </div>
                    <div class="slider__inner">
                    </div>
                </div>
                <div class="slider-nav">
                    <div class="slide-nav__inner">
                        <div class="container text-center">
                            <div class="slider__text">
                                april restaurant
                            </div>
                            <div class="slider__title">
                                Закажи стейк, получи подарок от шефа!
                            </div>
                            <div class="slider__date">
                                до 18 марта, 2019
                            </div>
                        </div>
                    </div>
                    <div class="slide-nav__inner">
                        <div class="container text-center">
                            <div class="slider__text">
                                Issimo restaurant
                            </div>
                            <div class="slider__title">
                                Закажи стейк, получи подарок от Issimo!
                            </div>
                            <div class="slider__date">
                                до 18 February, 2019
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <div class="restaurant desktop">
            <div class="container cont-fix">
                <div class="flexbox">                    
            <div class="restaurant__ins--desktop">
                <img src="{{ asset('img/mobile1.png') }}" class="restaurant__img">
                <div class="restaurant__event">
                    <img src="{{ asset('img/brand1.png') }}" class="restaurant__brand">
                    Закажи на сумму свыше 60 000 сум и получи подарок!
                </div>
                <div class="restaurant__line"></div>
                <div class="restaurant__ins-p">
                    <h5 class="restaurant__title">Caffee’issimo</h5>
                    <p class="restaurant__desc">Европейская</p>
                </div>
                <div class="restaurant__ins-price">
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                    <p class="restaurant__long-desktop--event">4.2 км</p>
                </div>           
            </div>
            <div class="restaurant__ins--desktop">
                <img src="{{ asset('img/mobile1.png') }}" class="restaurant__img">
                <div class="restaurant__event">
                    <img src="{{ asset('img/brand1.png') }}" class="restaurant__brand">
                    Закажи на сумму свыше 60 000 сум и получи подарок!
                </div>
                <div class="restaurant__line"></div>
                <div class="restaurant__ins-p">
                    <h5 class="restaurant__title">Caffee’issimo</h5>
                    <p class="restaurant__desc">Европейская</p>
                </div>
                <div class="restaurant__ins-price">
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                    <p class="restaurant__long-desktop--event">4.2 км</p>
                </div>           
            </div>
            <div class="restaurant__ins--desktop">
                <img src="{{ asset('img/mobile1.png') }}" class="restaurant__img">
                <div class="restaurant__event">
                    <img src="{{ asset('img/brand1.png') }}" class="restaurant__brand">
                    Закажи на сумму свыше 60 000 сум и получи подарок!
                </div>
                <div class="restaurant__line"></div>
                <div class="restaurant__ins-p">
                    <h5 class="restaurant__title">Caffee’issimo</h5>
                    <p class="restaurant__desc">Европейская</p>
                </div>
                <div class="restaurant__ins-price">
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                    <p class="restaurant__long-desktop--event">4.2 км</p>
                </div>           
            </div>
            <div class="restaurant__ins--desktop">
                <img src="{{ asset('img/mobile1.png') }}" class="restaurant__img">
                <div class="restaurant__event">
                    <img src="{{ asset('img/brand1.png') }}" class="restaurant__brand">
                    Закажи на сумму свыше 60 000 сум и получи подарок!
                </div>
                <div class="restaurant__line"></div>
                <div class="restaurant__ins-p">
                    <h5 class="restaurant__title">Caffee’issimo</h5>
                    <p class="restaurant__desc">Европейская</p>
                </div>
                <div class="restaurant__ins-price">
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                    <p class="restaurant__long-desktop--event">4.2 км</p>
                </div>        
                </div>                
                </div>                   
            </div>                                    
        </div>               
        <div class="events mobile">
            <div class="container">
                <button class="events__arrow">
                    <img src="{{ asset('img/arrowgreen.svg') }}">
                </button>
                <div class="text-center">
                    Акции
                </div>
            </div>
        </div>
        <div class="restaurant mobile">
            <div class="restaurant__ins">
                <img src="{{ asset('img/mobile1.png') }}" class="restaurant__img">
                <div class="restaurant__event">
                    <img src="{{ asset('img/brand1.png') }}" class="restaurant__brand">
                    Закажи на сумму свыше 60 000 сум и получи подарок!
                </div>
                <div class="restaurant__line"></div>
                <div class="restaurant__ins-p">
                    <h5 class="restaurant__title">Caffee’issimo</h5>
                    <p class="restaurant__desc">Европейская</p>
                </div>
                <div class="restaurant__ins-price">
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                    <p class="restaurant__long">4.2 км</p>
                </div>           
            </div>
            <div class="restaurant__ins">
                <img src="{{ asset('img/mobile1.png') }}" class="restaurant__img">
                <div class="restaurant__event">
                    <img src="{{ asset('img/brand1.png') }}" class="restaurant__brand">
                    Закажи на сумму свыше 60 000 сум и получи подарок!
                </div>
                <div class="restaurant__line"></div>
                <div class="restaurant__ins-p">
                    <h5 class="restaurant__title">Caffee’issimo</h5>
                    <p class="restaurant__desc">Европейская</p>
                </div>
                <div class="restaurant__ins-price">
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                    <p class="restaurant__long">4.2 км</p>
                </div>           
            </div>
            <div class="restaurant__ins">
                <img src="{{ asset('img/mobile1.png') }}" class="restaurant__img">
                <div class="restaurant__event">
                    <img src="{{ asset('img/brand1.png') }}" class="restaurant__brand">
                    Закажи на сумму свыше 60 000 сум и получи подарок!
                </div>
                <div class="restaurant__line"></div>
                <div class="restaurant__ins-p">
                    <h5 class="restaurant__title">Caffee’issimo</h5>
                    <p class="restaurant__desc">Европейская</p>
                </div>
                <div class="restaurant__ins-price">
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                    <p class="restaurant__long">4.2 км</p>
                </div>           
            </div>
            <div class="restaurant__ins">
                <img src="{{ asset('img/mobile1.png') }}" class="restaurant__img">
                <div class="restaurant__event">
                    <img src="{{ asset('img/brand1.png') }}" class="restaurant__brand">
                    Закажи на сумму свыше 60 000 сум и получи подарок!
                </div>
                <div class="restaurant__line"></div>
                <div class="restaurant__ins-p">
                    <h5 class="restaurant__title">Caffee’issimo</h5>
                    <p class="restaurant__desc">Европейская</p>
                </div>
                <div class="restaurant__ins-price">
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                    <p class="restaurant__long">4.2 км</p>
                </div>           
            </div>                                    
        </div>       
        </div>
        @include('partials.download')
        @include('partials.footer')        
         @include('partials.footermobile')
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        @yield('script')
        <script>
        $('.slider-for').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots:true,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            fade:true
        });
        $('.rest-nav').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
            dots:false,
        });        
        </script>
    </body>
</html>
