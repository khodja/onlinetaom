<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OnlineTaom</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
    <body>
        <div class="events mobile">
            <div class="container">
                <button class="events__arrow">
                    <img src="{{ asset('img/arrowgreen.svg') }}">
                </button>
                <div class="text-center">
                    Личные данные
                </div>
            </div>
        </div>
        <div class="info">
            <div class="info__inner">
                <div class="info__inner--title">Контактный телефон</div>
                <div class="info__inner--show">+998 94 619 67 67</div>
            </div>
            <div class="info__inner">
                <div class="info__inner--title">Адрес доставки</div>
                <div class="info__inner--show">Юнусабад, ул. Кашгар, Ц-4, д. 36</div>
            </div>
            <div class="info__inner">
                <div class="info__inner--title">Получатель</div>
                <div class="info__inner--show">Khalil Mukhammad-Rakan</div>
            </div>                        
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        @yield('scripts')
    </body>
</html>
