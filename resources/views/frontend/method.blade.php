<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OnlineTaom</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
    <body>
        <div class="wrapper">
        @include('partials.navdesktop')
        <div class="events mobile">
            <div class="container">
                <button class="events__arrow">
                    <img src="{{ asset('img/arrowgreen.svg') }}">
                </button>
                <div class="text-center">
                    Оплата
                </div>
            </div>
        </div>
        <div class="method desktop">
            <div class="events-title">Выберите способ оплаты</div>
            <div class="method__element">
                <img src="{{ asset('img/payme.png') }}" height="100">
                <div class="method__element-text">
                    Картой UzCard он-лайн    
                </div>                
            </div>
            <div class="method__element">
                <img src="{{ asset('img/wallet.png') }}" height="100">
                <div class="method__element-text">
                    Наличными, при получении
                </div>                
            </div>            
        </div>
        @include('partials.footer')
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        @yield('scripts')
    </body>
</html>
