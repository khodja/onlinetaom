<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OnlineTaom</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
    <body>
        <div class="wrapper">
        @include('partials.nav')
        <section class="basket desktop">
        <div class="basket__title">
            <img src="{{ asset('img/history.svg') }}" alt="cart icon">
            <h2>
                Мои заказы
            </h2>
        </div>
        </section>        
        <div class="restaurant small-hidden">
            <div class="container cont-fix">
            <div class="restaurant-desktop__ins">
                <div class="history-desktop">
                    <div class="aside aside-1">
                        <div class="history__ins-p">
                             №317 
                        </div>                    
                    </div>
                    <div class="main">
                        <div class="vm">
                            <img src="{{ asset('img/brand1.png') }}" width="70" class="vm">
                            <h5 class="restaurant__title pl-10">Caffee’issimo</h5>
                            <div class="history__info-desk">
                                <span class="history__info-name">
                                    Пицца Пепперони
                                </span>
                                <span class="history__info-price">
                                    45 000 сум
                                </span>                            
                                <span class="history__info-quantity">
                                    1 шт
                                </span>
                                <span class="history__info-name">
                                    Пицца Пепперони
                                </span>
                                <span class="history__info-price">
                                    45 000 сум
                                </span>                            
                                <span class="history__info-quantity">
                                    1 шт
                                </span>
                                <span class="history__info-name">
                                    Пицца Пепперони
                                </span>
                                <span class="history__info-price">
                                    45 000 сум
                                </span>                            
                                <span class="history__info-quantity">
                                    1 шт
                                </span>                                                                                                                      
                            </div>                        
                        </div>                 
                    </div>
                    <div class="foot text-right">
                        <div class="history__ins-price">
                            124 000 сум
                        </div>                    
                        <div class="history__status">
                        В обработке
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div> 
        </div>                       
        </div>
        <div class="events mobile">
            <div class="container">
                <button class="events__arrow">
                    <img src="{{ asset('img/arrowgreen.svg') }}">
                </button>
                <div class="text-center">
                    Мои заказы
                </div>
            </div>
        </div>
        <div class="restaurant small-show">
            <div class="restaurant__ins">
                <div class="history">
                    <div class="restaurant__ins-p vm">
                        <img src="{{ asset('img/brand1.png') }}" class="vm" width="70">
                        <h5 class="restaurant__title pl-10">Caffee’issimo</h5>
                    </div>                    
                    <div class="restaurant__ins-price">
                        <div class="restaurant__inner"> №317</div> 
                    </div>                    
                </div>
                <div class="history__info">
                    <span class="history__info-name">
                        Пицца Пепперони
                    </span>
                    <span class="history__info-price">
                        45 000 сум
                    </span>
                    <span class="history__info-quantity">
                        1 шт
                    </span>
                </div>
                <div class="history__info">
                    <span class="history__info-name">
                        Пицца Пепперони
                    </span>
                    <span class="history__info-price">
                        45 000 сум
                    </span>
                    <span class="history__info-quantity">
                        1 шт
                    </span>
                </div>
                <div class="history__info">
                    <span class="history__info-name">
                        Пицца Пепперони
                    </span>
                    <span class="history__info-price">
                        45 000 сум
                    </span>
                    <span class="history__info-quantity">
                        1 шт
                    </span>
                </div>                                
                <div class="restaurant__line mt-10"></div>
                <div class="history__status">
                    <div class="col-xs-6 bold">
                        В обработке
                    </div>
                    <div class="col-xs-6 text-right">
                        142 000 сум
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="restaurant__ins">
                <div class="history">
                    <div class="restaurant__ins-p vm">
                        <img src="{{ asset('img/brand1.png') }}" class="vm" width="70">
                        <h5 class="restaurant__title pl-10">Caffee’issimo</h5>
                    </div>                    
                    <div class="restaurant__ins-price">
                        <div class="restaurant__inner"> №317</div> 
                    </div>                    
                </div>
                <div class="history__info">
                    <span class="history__info-name">
                        Пицца Пепперони
                    </span>
                    <span class="history__info-price">
                        45 000 сум
                    </span>
                    <span class="history__info-quantity">
                        1 шт
                    </span>
                </div>
                <div class="history__info">
                    <span class="history__info-name">
                        Пицца Пепперони
                    </span>
                    <span class="history__info-price">
                        45 000 сум
                    </span>
                    <span class="history__info-quantity">
                        1 шт
                    </span>
                </div>
                <div class="history__info">
                    <span class="history__info-name">
                        Пицца Пепперони
                    </span>
                    <span class="history__info-price">
                        45 000 сум
                    </span>
                    <span class="history__info-quantity">
                        1 шт
                    </span>
                </div>                                
                <div class="restaurant__line mt-10"></div>
                <div class="history__status">
                    <div class="col-xs-6 bold">
                        В обработке
                    </div>
                    <div class="col-xs-6 text-right">
                        142 000 сум
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="restaurant__ins">
                <div class="history">
                    <div class="restaurant__ins-p vm">
                        <img src="{{ asset('img/brand1.png') }}" class="vm" width="70">
                        <h5 class="restaurant__title pl-10">Caffee’issimo</h5>
                    </div>                    
                    <div class="restaurant__ins-price">
                        <div class="restaurant__inner"> №317</div> 
                    </div>                    
                </div>
                <div class="history__info">
                    <span class="history__info-name">
                        Пицца Пепперони
                    </span>
                    <span class="history__info-price">
                        45 000 сум
                    </span>
                    <span class="history__info-quantity">
                        1 шт
                    </span>
                </div>
                <div class="history__info">
                    <span class="history__info-name">
                        Пицца Пепперони
                    </span>
                    <span class="history__info-price">
                        45 000 сум
                    </span>
                    <span class="history__info-quantity">
                        1 шт
                    </span>
                </div>
                <div class="history__info">
                    <span class="history__info-name">
                        Пицца Пепперони
                    </span>
                    <span class="history__info-price">
                        45 000 сум
                    </span>
                    <span class="history__info-quantity">
                        1 шт
                    </span>
                </div>                                
                <div class="restaurant__line mt-10"></div>
                <div class="history__status">
                    <div class="col-xs-6 bold">
                    <img src="{{ asset('img/succes.svg') }}" class="vm mr-10">
                        Доставлен
                    </div>
                    <div class="col-xs-6 text-right">
                        142 000 сум
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        @include('partials.footer')   
    </div>        
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        @yield('scripts')
    </body>
</html>
