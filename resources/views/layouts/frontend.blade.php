<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="Content-Language" content="{{ app()->getLocale() }}" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="manifest" href="/manifest.json">
    <title>OnlineTaom</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700&amp;subset=cyrillic,latin-ext" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
    <meta name="apple-mobile-web-app-title" content="OnlineTaom">
    <link rel="apple-touch-icon" href="/img/icons/icon-512x512.png">
    <link href="/img/icons/splash-640x1136.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-750x1334.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1242x2208.png" media="(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1125x2436.png" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-828x1792.png" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1242x2688.png" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1536x2048.png" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1668x2224.png" media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1668x2388.png" media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-2048x2732.png" media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <meta name="msapplication-TileImage" content="/img/icons/icon-512x512.png">
    <meta name="application-name" content="OnlineTaom">
    <link rel="icon" sizes="512x512" href="/img/icons/icon-512x512.png">
</head>
    <body>  
        <div class="wrapper">
            @include('partials.nav')
            @include('partials.animation')
            @yield('content')
            @include('partials.footer')
            @include('partials.footermobile')
            @include('partials.popup')
        </div>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js"></script>
            <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
            
        <script>
        $(".js-range-slider").ionRangeSlider({
            skin: "square",
            type: "double",
            min: 0,
            // hide_min_max:true,
            hide_from_to:true,
            max: 1000000,
            from: 0,
            to: 480000,
            postfix: " Сум"
        });
        $(".js-range-slider-2").ionRangeSlider({
            skin: "square",
            type: "double",
            min: 0,
            // hide_min_max:true,
            hide_from_to:true,
            max: 90,
            from: 0,
            to: 40,
            postfix: " мин"
        });
        var scene = document.getElementById('scene');
        var parallaxInstance = new Parallax(scene);
        $('.rest-nav').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
            dots:false,
        });        
        $('.type__slider').slick({
            infinite: true,
            slidesToShow: 9,
            slidesToScroll: 1,
            arrows: false,
            dots:false,
            responsive: [
            {
            breakpoint: 1024,
            settings: {
                slidesToShow: 6,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
            },
            {
            breakpoint: 600,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
            }
        ]    
        });        
        </script>        
        <script>
        var box = $('.box');
        var hamb = $('#menu');
            $('#menu').click(function(){
            $(this).toggleClass('is-active');
            if($(this).hasClass('is-active')){ 
                $('.main').removeClass('none');
                $('body').addClass('fixed');
                $('#menu').addClass('disabled');
                setTimeout(function(){
                    $('#menu').removeClass('disabled');
                },1500);
                    $('.main').removeClass('slideOutRight');
                    $('.main').addClass('slideInRight');
                setTimeout(function(){
                    $('.animation').removeClass('animated slideOutRight');
                    $('.animation').addClass('animated slideInRight');
                },500);                  
                $('#open').fadeOut('fast');                
                $('#close').fadeIn('fast');                
            }
            else{                
                $('#close').fadeOut('fast');  
                $('#open').fadeIn('fast');                                              
                $('body').removeClass('fixed');                
                $('#menu').addClass('disabled');
                setTimeout(function(){                
                    $('#menu').removeClass('disabled');
                },1500);                  
                $('.animation').removeClass('animated slideInRight');
                $('.animation').addClass('animated slideOutRight');     
                $('.main').removeClass('slideInRight');                
                $('.main').addClass('slideOutRight');                
            }
        });
        $(".search-icon").on('click', function() {
            $(this).toggleClass('active');
            if($(this).hasClass('active')){
                $('#find-input').removeClass('none');
                $('.mobnav__logo').addClass('animated fadeOut');
                $("#find-input" ).focus();                        
                $('#find-input').removeClass('fadeOutLeft');
                $('#find-input').addClass('fadeInLeft');
            }
            else{
                $('.mobnav__logo').removeClass('animated fadeOut');
                $('#find-input').removeClass('fadeInLeft');
                $('#find-input').addClass('fadeOutLeft');
            }
        });
        $(function() {
            var Accordion = function(el, multiple) {
                this.el = el || {};
                this.multiple = multiple || false;
        
                // Variables privadas
                var links = this.el.find('.link');
                // Evento
                links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
            }
        
            Accordion.prototype.dropdown = function(e) {
                var $el = e.data.el;
                    $this = $(this),
                    $next = $this.next();
        
                $next.slideToggle();
                $this.parent().toggleClass('open');
        
                if (!e.data.multiple) {
                    $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
                };
            }	
        
            var accordion = new Accordion($('#accordion'), false);
        });


        /// Dealing with popup
            var target = $("#nav-push");
            var popup = $('#popup').first().next().next(); // .next();
            var offset = target.offset();
            var leftDist = offset.left;
            var topDist = offset.top;
            var rightDist = $(window).width() - leftDist - 40;

            $(window).on("load resize", function() {
                popup.css({
                    right : rightDist + 'px',
                });
            });
            target.on('click',function(){
                $('.modal').fadeIn('fast');
                $('.modal-bg').fadeIn('fast');
                popup.fadeIn('fast');
            });
            $('.modal-bg').on('click',function(){
                $('.modal').fadeOut('fast');
                $('.modal-bg').fadeOut('fast');
                popup.fadeOut('fast');
            });                        
        </script>
        @yield('script')
    </body>
</html>
