<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="manifest" href="/manifest.json">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>OnlineTaom::Back</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    @yield('style')
<!-- Web Application Manifest -->
    <!-- Add to homescreen for Chrome on Android -->
    <meta name="application-name" content="OnlineTaom">
    <link rel="icon" sizes="512x512" href="/img/icons/icon-512x512.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-title" content="OnlineTaom">
    <link rel="apple-touch-icon" href="/img/icons/icon-512x512.png">
    <link href="/img/icons/splash-640x1136.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-750x1334.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1242x2208.png" media="(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1125x2436.png" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-828x1792.png" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1242x2688.png" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1536x2048.png" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1668x2224.png" media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1668x2388.png" media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-2048x2732.png" media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />

    <!-- Tile for Win8 -->
    <meta name="msapplication-TileImage" content="/img/icons/icon-512x512.png">
    <script src="{{ asset('backend/js/require.min.js')}}"></script>
    <script>
      requirejs.config({
          baseUrl: '/'
      });
    </script>
    <link href="{{ asset('backend/css/dashboard.css')}}" rel="stylesheet" />
    <link href="{{ asset('backend/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/js/dashboard.js')}}"></script>
    <link href="{{ asset('backend/plugins/charts-c3/plugin.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/plugins/charts-c3/plugin.js')}}"></script>
    <link href="{{ asset('backend/plugins/maps-google/plugin.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/plugins/maps-google/plugin.js')}}"></script>
    <script src="{{ asset('backend/plugins/input-mask/plugin.js')}}"></script>
  </head>
  <body>
<div class="page">
    <div class="page-main">
    <div class="header py-4">
        <div class="container">
        <div class="d-flex">
            <a class="header-brand" href="{{ action('OrderController@getOrderBy','day')  }}">
            <img src="{{ asset('img/logo.svg') }}"  width="70">
            </a>
            <div class="d-flex order-lg-2 ml-auto">
            <div class="dropdown d-none d-md-flex">
                <a class="nav-link icon" data-toggle="dropdown">
                <i class="fe fe-bell"></i>
                <span class="nav-unread"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                    @include('backend.partials.nav')
                </div>
            </div>
            <div class="dropdown">
                <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                <span class="avatar" style="    "></span>
                <span class="ml-2 d-none d-lg-block">
                    <span class="text-default">{{ Auth::user()->name }}</span>
                </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                <a class="dropdown-item" href="/notify">
                    <i class="dropdown-icon fe fe-settings"></i> Уведомление
                </a>
                <div class="dropdown-divider"></div>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                        <button class="dropdown-item" type="submit"><i class="dropdown-icon fe fe-log-out"></i> Выйти</button>
                </form>
                </div>
            </div>
            </div>
            <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
            <span class="header-toggler-icon"></span>
            </a>
        </div>
        </div>
    </div>
    <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
        <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
            <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                @if(!Auth::user()->hasRole('callcenter'))
                <li class="nav-item">
                    <a href="{{ action('HomeController@index') }}" class="nav-link {{ request()->is('home*') ? 'active' : '' }}"><i class="fe fe-activity"></i> Статистика</a>
                </li>
                @endif
                @if(Auth::user()->hasRole('manager'))
                <li class="nav-item">
                    <a href="{{ action('FoodController@index') }}" class="nav-link {{ request()->is('food*') ? 'active' : '' }}"><i class="fe fe-pie-chart"></i> Меню</a>
                </li>
                @endif
                @if(!Auth::user()->hasRole('callcenter'))
                <li class="nav-item">
                    <a href="{{ action('RestaurantController@index') }}" class="nav-link {{ request()->is('restaurant*') ? 'active' : '' }}"><i class="fe fe-home"></i>  @if(Auth::user()->hasRole('manager')) Ресторан @else Рестораны @endif</a>
                </li>
                @endif
{{--                <li class="nav-item">--}}
{{--                    <a href="{{ action('FavoriteController@index') }}" class="nav-link {{ request()->is('favorite*') ? 'active' : '' }}"><i class="fe fe-star"></i> Избранные</a>--}}
{{--                </li>--}}
                <li class="nav-item">
                    <a href="{{ action('OrderController@getOrderBy','day') }}" class="nav-link {{ request()->is('orders*') ? 'active' : '' }}"><i class="fe fe-clipboard"></i> Заказы</a>
                </li>
                @if(Auth::user()->hasRole('admin'))
                <li class="nav-item">
                    <a href="{{ action('CategoryController@index') }}" class="nav-link {{ request()->is('categories*') ? 'active' : '' }} "><i class="fe fe-grid"></i> Категории</a>
                </li>
                @endif
                @if(Auth::user()->id == 1)
                <li class="nav-item">
                    <a href="{{ action('DistrictController@index') }}" class="nav-link {{ request()->is('districts*') ? 'active' : '' }}"><i class="fe fe-map"></i> Районы</a>
                </li>
                <li class="nav-item">
                    <a href="{{ action('UserController@index') }}" class="nav-link {{ request()->is('users*') ? 'active' : '' }}"><i class="fe fe-user"></i> Ползователи</a>
                </li>
                <li class="nav-item">
                    <a href="{{ action('UserController@manager') }}" class="nav-link {{ request()->is('managers*') ? 'active' : '' }}"><i class="fe fe-users"></i> Менеджеры</a>
                </li>
                <li class="nav-item">
                    <a href="{{ action('UserController@moderator') }}" class="nav-link {{ request()->is('moderators*') ? 'active' : '' }}"><i class="fe fe-eye"></i> Модераторы</a>
                </li>
                <li class="nav-item">
                    <a href="{{ action('UserController@callcenter') }}" class="nav-link {{ request()->is('operators*') ? 'active' : '' }}"><i class="fe fe-phone-call"></i> Операторы</a>
                </li>
                <li class="nav-item">
                    <a href="{{ action('CarrierController@index') }}" class="nav-link {{ request()->is('carriers*') ? 'active' : '' }}"><i class="fe fe-truck"></i> Курьеры</a>
                </li>
                @endif
            </ul>
            </div>
        </div>
        </div>
    </div>
    @include('partials.message')
    @yield('content')
    </div>
<footer class="footer">
    <div class="container">
    <div class="row align-items-center flex-row-reverse">
        <div class="col-12 mt-3 mt-lg-0 text-center">
        Copyright © 2019. Все права защищены.
        </div>
    </div>
    </div>
</footer>
</div>
<script>
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/sw.js', {
            scope: '.'
        }).then(function (registration) {
            // Registration was successful
            console.log('Laravel PWA: ServiceWorker registration successful with scope: ', registration.scope);
        }, function (err) {
            // registration failed :(
            console.log('Laravel PWA: ServiceWorker registration failed: ', err);
        });
    }
</script>
@yield('script')
  </body>
</html>
