    var filesToCache = [
        '/offline',
        '/backend/css/dashboard.css',
        '/backend/js/dashboard.js',
        '/img/icons/icon-72x72.png',
        '/img/icons/icon-96x96.png',
        '/img/icons/icon-128x128.png',
        '/img/icons/icon-144x144.png',
        '/img/icons/icon-152x152.png',
        '/img/icons/icon-192x192.png',
        '/img/icons/icon-384x384.png',
        '/img/icons/icon-512x512.png',
        '/img/icons/splash-640x1136.png',
        '/img/icons/splash-750x1334.png',
        '/img/icons/splash-1242x2208.png',
        '/img/icons/splash-1125x2436.png',
        '/img/icons/splash-828x1792.png',
        '/img/icons/splash-1242x2688.png',
        '/img/icons/splash-1536x2048.png',
        '/img/icons/splash-1668x2224.png',
        '/img/icons/splash-1668x2388.png',
        '/img/icons/splash-2048x2732.png'
    ];
    var staticCacheName = "pwa-v" + new Date().getTime();


(() => {
  'use strict'

  const WebPush = {
    init () {
        self.addEventListener('push', this.notificationPush.bind(this))
        self.addEventListener('notificationclick', this.notificationClick.bind(this))
        self.addEventListener('notificationclose', this.notificationClose.bind(this))
        self.addEventListener("install", event => {
            self.skipWaiting();
            event.waitUntil(
                caches.open(staticCacheName)
                    .then(cache => {
                        return cache.addAll(filesToCache);
                    })
            )
        })

        // Clear cache on activate
        self.addEventListener('activate', event => {
            event.waitUntil(
                caches.keys().then(cacheNames => {
                    return Promise.all(
                        cacheNames
                            .filter(cacheName => (cacheName.startsWith("pwa-")))
                            .filter(cacheName => (cacheName !== staticCacheName))
                            .map(cacheName => caches.delete(cacheName))
                    );
                })
            )
        })
        self.addEventListener("fetch", event => {
            event.respondWith(
                caches.match(event.request)
                    .then(response => {
                        return response || fetch(event.request);
                    })
                    .catch(() => {
                        return caches.match('offline');
                    })
            )
        })
    },


// Serve from Cache

    /**
     * Handle notification push event.
     *
     * https://developer.mozilla.org/en-US/docs/Web/Events/push
     *
     * @param {NotificationEvent} event
     */
    notificationPush (event) {
      if (!(self.Notification && self.Notification.permission === 'granted')) {
        return
      }

      // https://developer.mozilla.org/en-US/docs/Web/API/PushMessageData
      if (event.data) {
        event.waitUntil(
          this.sendNotification(event.data.json())
        )
      }
    },

    /**
     * Handle notification click event.
     *
     * https://developer.mozilla.org/en-US/docs/Web/Events/notificationclick
     *
     * @param {NotificationEvent} event
     */
    notificationClick (event) {
      // console.log(event.notification)

      if (event.action === 'some_action') {
        // Do something...
      } else {
        self.clients.openWindow('/orders')
      }
    },

    /**
     * Handle notification close event (Chrome 50+, Firefox 55+).
     *
     * https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerGlobalScope/onnotificationclose
     *
     * @param {NotificationEvent} event
     */
    notificationClose (event) {
      self.registration.pushManager.getSubscription().then(subscription => {
        if (subscription) {
          this.dismissNotification(event, subscription)
        }
      })
    },

    /**
     * Send notification to the user.
     *
     * https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/showNotification
     *
     * @param {PushMessageData|Object} data
     */
    sendNotification (data) {
      return self.registration.showNotification(data.title, data)
    },

    /**
     * Send request to server to dismiss a notification.
     *
     * @param  {NotificationEvent} event
     * @param  {String} subscription.endpoint
     * @return {Response}
     */
    dismissNotification ({ notification }, { endpoint }) {
      if (!notification.data || !notification.data.id) {
        return
      }

      const data = new FormData()
      data.append('endpoint', endpoint)

      // Send a request to the server to mark the notification as read.
      fetch(`/notifications/${notification.data.id}/dismiss`, {
        method: 'POST',
        body: data
      })
    }
  }

  WebPush.init()
})()
