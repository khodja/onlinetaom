<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function() {
    Route::get('orders', 'OrderController@indexManager');
    Route::get('/info', 'HomeController@info')->name('home');
    Route::get('orders/{id}', 'OrderController@showInnerManager');
    Route::patch('orders/{id}', 'OrderController@successOrderManager');
    Route::patch('orders/ignore/{id}', 'OrderController@ignoreOrderManager');
    Route::get('/restaurant/history', 'OrderController@historyManager');

    Route::patch('/food/{id}', 'FoodController@switchFood');
    Route::get('/restaurant/switch', 'RestaurantController@switchRestaurant');
    Route::get('/rest-foods', 'FoodController@foodsManager');

    Route::get('/user','UserController@check');

    Route::get('/favorite', 'FavoriteController@get');
    Route::get('/favorite/{id}', 'FavoriteController@getFavorite');
    Route::get('/favorite/food/{id}', 'FavoriteController@getFavoriteFood');
    Route::post('/favorite', 'FavoriteController@store');
    Route::delete('/favorite/{id}', 'FavoriteController@delete');

    Route::get('/order', 'OrderController@show');
    Route::get('/history', 'OrderController@history');
    Route::get('/basket', 'OrderController@basket');
    Route::get('/checkout/{id}', 'OrderController@inner');

    Route::post('/order-mobile', 'OrderController@storeMobile');
    Route::post('/order/{id}', 'OrderController@store');

    Route::patch('/order/{id}', 'OrderController@update');
    Route::put('/checkout/{id}', 'OrderController@checkout');

    Route::delete('/order/{id}', 'OrderController@delete');
    Route::patch('/clear/order/{id}', 'OrderController@clear');

    Route::post('/order/{id}/review', 'ReviewController@store');
    Route::patch('/order/{id}/review', 'ReviewController@update');
    Route::delete('/order/{id}/review', 'ReviewController@delete');

    Route::put('user/edit', 'UserController@change');

    Route::patch('/set-token', 'UserController@setToken');
    Route::post('/set-token', 'UserController@setToken');

    Route::get('logout', 'Api\AuthController@logout')->name('logout');

});
Route::get('/restaurant/{alias}', 'RestaurantController@show');

Route::get('/rest/{id}/{alias}', 'RestaurantController@category');
Route::get('categories/{id}', 'CategoryController@show');
Route::post('login', 'Api\AuthController@login')->name('login.api');
Route::post('auth', 'Api\AuthController@restLogin');
Route::post('check', 'Api\AuthController@check');
Route::post('register', 'Api\AuthController@register')->name('register.api');

Route::post('/location', 'LocationController@getData');
Route::patch('/search', 'LocationController@search');
Route::post('/distance', 'LocationController@distance');

Route::post('/paycom', 'PaymentController@run');


Route::get('/food/{id}', 'FoodController@show');
Route::get('/food', 'FoodController@get');

Route::get('/food/{id}/params', 'ParamController@show');

Route::get('/food/{id}/size', 'SizeController@show');



Route::get('/restaurant/{alias}/{id}', 'FoodController@showFood');

Route::get('/restaurant', 'RestaurantController@showAll');
Route::get('/restaurants/{start?}', 'RestaurantController@showAll');
Route::get('/test', 'RestaurantController@test');
Route::get('/restaurant-all', 'RestaurantController@showFinalAll');


Route::get('/promo', 'PromoController@show');
Route::get('/events', 'PromoController@get');


Route::get('/categories', 'CategoryController@indexJson');

Route::get('/{alias}', 'CategoryController@showBy');

